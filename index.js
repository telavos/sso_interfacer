const CONSTS_SYS = require('./main/libs/CONSTS_SYS')
const CONSTS = require('./main/libs/CONSTS')
let logger = require('./main/libs/logger')
const initBasicData = require('./main/processApp/initBasicData')

logger.info('=-=-=-=-= OVS_IF_v1.1.9.0821 =-=-=-=-=')


//process.env.NODE_ENV = (process.env.NODE_ENV && (process.env.NODE_ENV).trim().toLowerCase() === 'production') ? 'production' : 'development'
process.env.NODE_ENV = CONSTS.SVR_ENV.NODE_ENV
if (process.env.NODE_ENV === 'production') {
    logger.info("Production Mode")
}
else if (process.env.NODE_ENV === 'development') {
    logger.info("Development Mode")
}


//스케줄 로그관리
const schedule = require('node-schedule')
const findRemoveSync = require('find-remove')
const mock = require('mock-require')
const moment = require('moment')

schedule.scheduleJob('0 0 0 * * *', function () { //매일 00시00분00초에 함수 실행
    logger = mock.reRequire('./main/libs/logger')
    logger.info('new Date :' + moment(new Date()).format('YYYY-MM-DD'))

    try {
        findRemoveSync(CONSTS_SYS.DIR_PATH.LOG, {age: {seconds: CONSTS.LOGGER.LOG_STORAGE_AGE}, extensions: '.log'})
    }
    catch (e) {
        logger.error(e)
    }
})
