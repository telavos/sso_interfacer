FROM node:8.14.0

RUN mkdir -p /app
WORKDIR /app

COPY package.json /app
RUN cd /app; npm install

COPY . /app

EXPOSE 55000
EXPOSE 3306
EXPOSE 6379
EXPOSE 6380
EXPOSE 26379

CMD ["npm", "start"]