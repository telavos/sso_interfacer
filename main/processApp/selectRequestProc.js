const selQuery = require('../libs/db/selQuery')
const logger = require('../libs/logger')
const redis = require('../libs/redis')
const CONSTS_SYS = require('../libs/CONSTS_SYS')
const IF_ovsShop = require('../exInterface/IF_ovsShop')
const IF_FUSE = require('../exInterface/IF_FUSE')
const IF_CNTT = require('../exInterface/IF_CNTT')

/*
let doExeQuery = (type, data) => {
    selQuery.executeQueryUsingQueryGCode('OVSINIT', '', (err, res) => {
        if (err) {
            logger.error('connContents - OVSINIT - err: ' + err)
            sendErrorToContents(messageNo, err)
            return
        }

        logger.info('connContents - exQuery - OVSINIT - res: ' + res)

        doSocketWriteData(m_mapContents.get(messageNo), res)
    })
}


exports.exeQuery = (type, data) => {
    doExeQuery(type, data)
}
*/



///////////////////////////////
// sub: CONSTS_SYS.SUB_CH.REQ_QGCODE
// Query Group Code 실행...
let doExeQueryGroupCode = (data) => {
    logger.info('selectRequestProc - doExeQueryGroupCode - sub data: ' + JSON.stringify(data))
    // data = {
    //     workType: 'STAT'
    //     , option: 'query group code'
    //     , params: [{}]
    //     , csid: csid
    //     , isRes: 'Y' or 'N'
    //     , CNAME: CNAME
    // }

    // option --> query group name


    let oData = JSON.parse(data)
    let workType = oData.workType
    let option = oData.option
    let params = oData.params
    let csid = oData.csid
    let isRes = oData.isRes
    let CNAME = oData.CNAME

    if(workType === 'WT_ORDER'
        || workType === 'WT_ORDER_SETMENU'
        || workType === 'WT_ORDER_HISTORY') {
        option = 'QG_ORDER'
    }

    selQuery.executeQueryGroupCode_new(option, params, (err, rows) => {
        if (err) {
            logger.error('selectRequestProc - doExeQueryGroupCode - workType: ' + workType + ' , option: ' + option + ' , csid: ' + csid + ' - err: ' + err)
            if(isRes){
                let pubData = {data: '', workType: workType, csid: csid}
                redis.pubRedis(CONSTS_SYS.PUB_CH.RES_QGCODE + CNAME, pubData)
            }
            return
        }

        if (!rows) {
            logger.error('selectRequestProc - doExeQueryGroupCode - workType: ' + workType + ' , option: ' + option + ' , csid: ' + csid + ' - no rows data...')
            if(isRes){
                let pubData = {data: '', workType: workType, csid: csid}
                redis.pubRedis(CONSTS_SYS.PUB_CH.RES_QGCODE + CNAME, pubData)
            }
            return
        }

        if(isRes){
            let pubData = {data: rows, workType: workType, csid: csid}
            redis.pubRedis(CONSTS_SYS.PUB_CH.RES_QGCODE + CNAME, pubData)
        }
    })
}

//////////////////////////////////////////////
// 주문 실행
let doExeOrder = (data) => {
    let oData = JSON.parse(data)
    let wt = oData.workType

    switch (wt){
        case 'WT_ORDER':
            selQuery.procFinalOrderExecuteQuery_new(oData)
            break
        case 'WT_ORDER_SETMENU':
            selQuery.procFinalOrderSetMenu_new(oData)
            break
        case 'WT_ORDER_HISTORY':
            selQuery.procFinalOrderHistory_new(oData)
            break
    }

}

////////////////////////////////////////
// POS 실행
let doExePOSInterface = (data) => {
    // data: {"workType":"WT_POS_SHOP_STATUS"
    //          ,"option":"200101"
    //          ,"params":[{"mrhstSeq":81,"shopNumber":"07052013279"}]
    //          ,"csid":"1560387300.115468"
    //          , isRes: "Y"
    //          ,"CNAME":"mb4H7"}

    let oData = JSON.parse(data)
    let workType = oData.workType
    let posCode = oData.option

    logger.info('selectRequestProc - doExePOSInterface - data: ' + JSON.stringify(data))


    switch (posCode) {
        case CONSTS_SYS.POS_TYPE.FUSE_PP :
            IF_FUSE.sendDataFUSE(workType, data)
            break
        case CONSTS_SYS.POS_TYPE.CNTT_BQ :
            IF_CNTT.sendDataCNTT(workType, data)
            break
    }
}

/////////////////////////////////////////////
// POS 실행 - 페이 결제 후 POS 알림...
let doExePOSInterfaceAfterPay = (data) => {
    console.log('#### selectRequestProc - doExePOSInterfaceAfterPay - ssssss')
    console.dir(data)
    /*
    '{"workType":"WT_POS_SHOP_ORDER_AFTER_PAY_CARD"
        ,"option":""
        ,"params":[{"sleHistSeq":"23343",
                    "mrhstSeq":49}]
        ,"csid":"test"
        ,"isRes":true
        ,"CNAME":"m8oHk"}'

     */
    console.log('#### selectRequestProc - doExePOSInterfaceAfterPay - eeeeeeeeeee')

    let oData = JSON.parse(data)
    let workType = oData.workType
    let posCode = oData.option || '100001'

    logger.info('selectRequestProc - doExePOSInterfaceAfterPay - data: ' + JSON.stringify(data))


    // LT 연결...
    if (posCode === '100001') { // SSO
        let rqesterSe = '100001'
        let requstTy = '100001'
        let sleHistSeq = oData.params[0].sleHistSeq
        let mrhstSeq = oData.params[0].mrhstSeq
        console.log('selectRequestProc - doExePOSInterfaceAfterPay - LT - toShop - mrhstSeq: ' + mrhstSeq + ' - sleHistSeq: ' + sleHistSeq)
        IF_ovsShop.sendDataToOvsShop(sleHistSeq, requstTy, rqesterSe, mrhstSeq, (err, res) => {
            if (err) {
                logger.error('selectRequestProc - doExePOSInterfaceAfterPay - LT - SSO - sendDataToOvsShop -- ' + err)
                return
            }
        })
    }
    else{
        switch (posCode) {
            // case CONSTS_SYS.POS_TYPE.FUSE_PP :
            //     IF_FUSE.sendDataFUSE(workType, data)
            //     break
            case CONSTS_SYS.POS_TYPE.CNTT_BQ :
                IF_CNTT.sendDataCNTTforPay(workType, data)
                break
        }
    }
}




//////////////////////////////////////////////
// 카드 선결제를 위한 실행
let doPayCard = (data) => {
    let oData = JSON.parse(data)
    let wt = oData.workType

    switch (wt){
        case 'WT_PAY_CARD_SKMNS':
            selQuery.procPayCard_SkMns(oData)
            break
        case 'WT_PAY_CARD_SKMNS_HISTORY':
            selQuery.procPayCard_SkMns_HISTORY(oData)
            break
        case 'WT_PAY_CARD_SKMNS_SETMENU':
            selQuery.procPayCard_SkMns_SETMENU(oData)
            break


        case 'WT_ORDER_SETMENU':
            selQuery.procFinalOrderSetMenu_new(oData)
            break
        case 'WT_ORDER_HISTORY':
            selQuery.procFinalOrderHistory_new(oData)
            break
    }

}


//////////////////////////////////////////////
// 카드 선결제 리턴 후 실행
let doPayCardResponse = (data) => {
    let oData = JSON.parse(data)
    let wt = oData.workType

    switch (wt){
        case 'WT_PAY_CARD_RESPONSE_SKMNS':
            selQuery.procPayCardResponse_SkMns(oData)
            break
        case 'WT_ORDER_SETMENU':
            selQuery.procFinalOrderSetMenu_new(oData)
            break
        case 'WT_ORDER_HISTORY':
            selQuery.procFinalOrderHistory_new(oData)
            break
    }

}


//////////////////////////////////////////////
// 카드 결제 취소 리턴 후 POS 취소 실행
let doPayCardCancelResponse = (data) => {
    let oData = JSON.parse(data)
    let wt = oData.workType

    switch (wt){
        case 'WT_PAY_CARD_CANCEL_RESPONSE_SKMNS':
            selQuery.procPayCardCancelResponse_SkMns(oData)
            break
        case 'WT_ORDER_SETMENU':
            selQuery.procFinalOrderSetMenu_new(oData)
            break
        case 'WT_ORDER_HISTORY':
            selQuery.procFinalOrderHistory_new(oData)
            break
    }

}





//////////////////////////////////////////
// CONSTS_SYS.SUB_CH.REQ_QGCODE
exports.exeQueryGroupCode = (data) => {
    doExeQueryGroupCode(data)
}

//////////////////////////////////////////
// CONSTS_SYS.SUB_CH.REQ_ORDER
exports.exeOrder = (data) => {
    doExeOrder(data)
}


/////////////////////////////////////////////
// POS 연결...
exports.exePOSInterface = (data) => {
    doExePOSInterface(data)
}


/////////////////////////////////////////////
// POS 연결 - 페이 결제 후 POS 알림...
exports.exePOSInterfaceAfterPay = (data) => {
    doExePOSInterfaceAfterPay(data)
}



////////////////////////////////////////////
// 카드 선결제
exports.payCard = (data) => {
    doPayCard(data)
}

////////////////////////////////////////////
// 카드 선결제 리턴 데이터
exports.payCardResponse = (data) => {
    doPayCardResponse(data)
}


////////////////////////////////////////////
// 카드 결제 취소 후 POS 취소
exports.payCardCancelResponse = (data) => {
    doPayCardCancelResponse(data)
}


