const logger = require('../libs/logger')
const selQuery = require('../libs/db/selQuery')
//const commonMapData = require('./commonMapData')
const CONSTS_SYS = require('../libs/CONSTS_SYS')

const redis = require('../libs/redis')
const connContents = require('../IFapp/connContents')
const connPing = require('../IFapp/connPing')


// let initStoreInfo = () => {
//     selQuery.executeQueryUsingCodes('STORE_BASIC_DATA', '', (err, rows) => {
//         if (err) {
//             logger.error('initBasicData - oConnContents - err: ' + err)
//             return
//         }
//
//         if (!rows) {
//             logger.warning('initBasicData - no store data.....')
//             return
//         }
//
//         let arrData = rows.STORE_BASIC_DATA
//
//         commonMapData.cleaStoreInfo()
//
//         for (let i = 0; i < arrData.length; i++) {
//             //console.log(arrData[i].RCV_TEL_NO + ' --- ' + JSON.stringify(arrData[i]))
//             commonMapData.setStoreInfo(arrData[i].RCV_TEL_NO, arrData[i])
//         }
//
//         logger.info('initBasicData - setting basic data...')
//     })
// }
// let initStoreMent = () => {
//     selQuery.executeQueryUsingCodes('STORE_MENT', '', (err, rows) => {
//         if (err) {
//             logger.error('connContents - oConnContents - err: ' + err)
//             return
//         }
//
//         if (!rows) {
//             logger.warning('initBasicData - no ShopMent data.....')
//             return
//         }
//
//         let arrData = rows.STORE_MENT
//
//         commonMapData.clearStoreMent()
//
//         for (let i = 0; i < arrData.length; i++) {
//             //console.log(arrData[i].RCV_TEL_NO + ' --- ' + JSON.stringify(arrData[i]))
//             commonMapData.setStoreMent(arrData[i].RCV_TEL_NO, arrData[i])
//         }
//
//         logger.info('initBasicData - setting basic data...')
//     })
// }

// exports.setInitBasicData = () => {
//     logger.info('initBasicData - setInitBasicData start...')
//
//     initStoreInfo()
//     //initStoreMent()
// }




//////////////////////////
//////////////////////////
//////////////////////////
//////////////////////////
///// new ////////////////
///// new ////////////////
///// new ////////////////
///// new ////////////////

let doContentsShopBasicData = (pubData) => {
    //pubData
    /*{workType:
        , queryGroupCode:
        , params:
        , sessionId:
        , CNAME: }*/
    let pData = JSON.parse(pubData)
    let CNAME = pData.CNAME

    selQuery.executeQueryGroupCode_new('QG_SHOP_BASIC_DATA', '', (err, rows) => {
        if (err) {
            logger.error('initBasicData - doContentsShopBasicData - err: ' + err)
            return
        }

        if (!rows) {
            logger.warning('initBasicData - doContentsShopBasicData - no data')
            return
        }

        let pubData = {data: rows}
        redis.pubRedis(CONSTS_SYS.PUB_CH.RES_SHOP_BASIC_DATA + CNAME, pubData)
    })
}


exports.contentsShopBasicData = (pubData) => {
    logger.info('initBasicData - contentsShopBasicData start...')

    doContentsShopBasicData(pubData)
}
