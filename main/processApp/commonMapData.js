/*
get(key:*) : * returns the value stored for that key.
set(key:*, value:*) : HashMap stores a key-value pair
multi(key:*, value:*, key2:*, value2:*, ...) : HashMap stores several key-value pairs
copy(other:HashMap) : HashMap copies all key-value pairs from other to this instance
has(key:*) : Boolean returns whether a key is set on the hashmap
search(value:*) : * returns key under which given value is stored (null if not found)
delete(key:*) : HashMap deletes a key-value pair by key
remove(key:*) : HashMap Alias for delete(key:*) (deprecated)
type(key:*) : String returns the data type of the provided key (used internally)
keys() : Array<*> returns an array with all the registered keys
values() : Array<*> returns an array with all the values
entries() : Array<[*,*]> returns an array with [key,value] pairs
size : Number the amount of key-value pairs
count() : Number returns the amount of key-value pairs (deprecated)
clear() : HashMap deletes all the key-value pairs on the hashmap
clone() : HashMap creates a new hashmap with all the key-value pairs of the original
hash(key:*) : String returns the stringified version of a key (used internally)
forEach(function(value, key)) : HashMap iterates the pairs and calls the function for each one
 */

//const HashMap = require('hashmap')

//const logger = require('../libs/logger')


//let m_mapStoreInfo = new HashMap()
//let m_mapStoreMent = new HashMap()


// exports.setStoreInfo = (k, d) => {
//     m_mapStoreInfo.set(k, d)
//     //logger.info('commonMapData - setStoreInfo - k: ' + k + ' , d: ' + JSON.stringify(d) + ' - cnt: ' + m_mapStoreInfo.count())
// }

// exports.getStoreInfo = (k) => {
//     return m_mapStoreInfo.get(k) || ''
// }

// exports.cleaStoreInfo = () => {
//     logger.debug('commonMapData - clear m_mapStoreInfo...')
//     m_mapStoreInfo.clear()
// }


// exports.setStoreMent = (k, d) => {
//     m_mapStoreMent.set(k, d)
//     //logger.info('commonMapData - setStoreMent - k: ' + k + ' , d: ' + JSON.stringify(d) + ' - cnt: ' + m_mapStoreMent.count())
// }
//
// exports.getStoreMent = (k) => {
//     return m_mapStoreMent.get(k) || ''
// }
//
// exports.clearStoreMent = () => {
//     logger.debug('commonMapData - clear m_mapShopMent...')
//     m_mapStoreMent.clear()
// }