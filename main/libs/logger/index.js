
const CONSTS = require('../CONSTS')
const logger = require('./winston')

exports.info = function(s, mdn){
    if(mdn && (CONSTS.LOGGER.LOG_LOGGING_MDN === 'Y')){
        logger.info(s + ' , mdn: ' + mdn);
    }
    else{
        logger.info(s);
    }
};
exports.error = function(s, mdn){
    if(mdn && (CONSTS.LOGGER.LOG_LOGGING_MDN === 'Y')){
        logger.error(s + ' , mdn: ' + mdn);
    }
    else{
        logger.error(s);
    }
};
exports.debug = function(s, mdn){
    if(mdn && (CONSTS.LOGGER.LOG_LOGGING_MDN === 'Y')){
        logger.debug(s + ' , mdn: ' + mdn);
    }
    else{
        logger.debug(s);
    }
};
exports.warning = function(s, mdn){
    if(mdn && (CONSTS.LOGGER.LOG_LOGGING_MDN === 'Y')){
        logger.warn(s + ' , mdn: ' + mdn);
    }
    else{
        logger.warn(s);
    }
};
