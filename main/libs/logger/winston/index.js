/*
 emerg: 0,
 alert: 1,
 crit: 2,
 error: 3,
 warning: 4,
 notice: 5,
 info: 6,
 debug: 7
 */

const CONSTS = require('../../CONSTS')
const CONSTS_SYS = require('../../CONSTS_SYS')
const winston = require('winston')
    , path = require('path')
    , moment = require('moment');
const winstonDaily = require('winston-daily-rotate-file');


let logger = new (winston.Logger)({
    transports: [
        new (winstonDaily)({
            filename: path.join(CONSTS_SYS.DIR_PATH.LOG, function(){return'Log_IF_%DATE%.log'}()),
            datePattern: 'YYYY-MM-DD',
            maxSize: CONSTS.LOGGER.LOG_MAX_SIZE+'m',
            maxFiles:  CONSTS.LOGGER.LOG_MAX_FILE+'d',
            level: CONSTS.LOGGER.LOG_LEVEL,
            timestamp: function(){return moment(new Date()).format('YYYY-MM-DD') + ' ' + moment(new Date()).format('HH:mm:ss.SSS')}
        }),
        new winston.transports.Console({
            colorize : true
            , level : CONSTS.LOGGER.LOG_LEVEL
            , timestamp: function(){return moment(new Date()).format('YYYY-MM-DD') + ' ' + moment(new Date()).format('HH:mm:ss.SSS')}
        })
    ]
})

module.exports = logger
