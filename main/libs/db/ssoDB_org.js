const mysql = require('mysql')
const CONSTS = require('../CONSTS')
const logger = require('../logger')

const OVSDB_1 = 'OVSDB1'
const OVSDB_2 = 'OVSDB2'

let clusterConfig, poolCluster


let conf1 = {
    host: CONSTS.DB_INFO.OVS_DB_HOST_1,
    port: CONSTS.DB_INFO.OVS_DB_PORT_1,
    user: CONSTS.DB_INFO.OVS_DB_USER_1,
    password: CONSTS.DB_INFO.OVS_DB_PASSWORD_1,
    database: CONSTS.DB_INFO.OVS_DB_NAME_1,
    connectTimeout: CONSTS.DB_INFO.OVS_DB_CONNECT_TIMEOUT,
    connectionLimit: CONSTS.DB_INFO.OVS_DB_CONNECT_LIMIT
}
let conf2 = {
    host: CONSTS.DB_INFO.OVS_DB_HOST_2,
    port: CONSTS.DB_INFO.OVS_DB_PORT_2,
    user: CONSTS.DB_INFO.OVS_DB_USER_2,
    password: CONSTS.DB_INFO.OVS_DB_PASSWORD_2,
    database: CONSTS.DB_INFO.OVS_DB_NAME_2,
    connectTimeout: CONSTS.DB_INFO.OVS_DB_CONNECT_TIMEOUT,
    connectionLimit: CONSTS.DB_INFO.OVS_DB_CONNECT_LIMIT
}


let doAddDBinCluster = (clusterName) => {
    poolCluster.remove(clusterName)

    try {
        logger.info('ssoDB - ADD Cluster - ' + clusterName + '...')
        switch (clusterName) {
            case OVSDB_1:
                poolCluster.add(clusterName, conf1)
                break

            case OVSDB_2:
                poolCluster.add(clusterName, conf2)
                break
        }
    }
    catch (e) {
        logger.error('ssoDB - doAddDBinCluster - catch - e: ' + e)
    }
}


clusterConfig = {
    removeNodeErrorCount: CONSTS.DB_INFO.OVS_DB_REMOVE_NODE_ERROR_COUNT,
    defaultSelector: 'ORDER'
}

poolCluster = mysql.createPoolCluster(clusterConfig)

doAddDBinCluster(OVSDB_1)
doAddDBinCluster(OVSDB_2)

poolCluster.on('remove', function (nodeId) {
    logger.debug('ssoDB - on remove NODE : ' + nodeId)
})


let connClusterDB = function (cb) {
    poolCluster.getConnection(OVSDB_1, function (err, connection) {
        if (err) {
            connClusterSlaveDB(function (err2, connection2) {
                if (err2) {
                    cb(err2)
                    return
                }
                cb(null, connection2)
            })
            return
        }
        cb(null, connection)
    })
}
let connClusterSlaveDB = function (cb) {
    poolCluster.getConnection(OVSDB_2, function (err, connection) {
        if (err) {
            logger.error('ssoDB - connClusterSlaveDB err: ' + err)
            poolCluster = mysql.createPoolCluster(clusterConfig)
            doAddDBinCluster(OVSDB_1)
            doAddDBinCluster(OVSDB_2)
            cb(err)
            return
        }
        cb(null, connection)
        doAddDBinCluster(OVSDB_1)
    })
}

let connMariasql = function (q, p, cb) {
    // let encK = CONSTS.Q_ENC_K
    // p = Object.assign(p, encK)
    connClusterDB(function (err, connection) {
        if (err) {
            logger.error('ssoDB - connMariasql - connClusterDB() - ' + err)
            cb(err)
            return
        }
        connection.config.queryFormat = function (query, values) {
            if (!values) return query
            return query.replace(/\:(\w+)/g, function (txt, key) {
                if (values.hasOwnProperty(key)) {
                    return this.escape(values[key])
                }
                return txt
            }.bind(this))
        }
        connection.query(q, p, function (err, rows) {
            connection.release()
            if (err) {
                logger.error('ssoDB - connMariasql - query() - ' + err)
                cb(err)
                return
            }
            cb(null, rows)
        })
    })
}


let connMariasqlInsert = (q, p, cb) => {
    // let encK = CONSTS.Q_ENC_K
    // p = Object.assign(p, encK)
    connClusterDB((err, connection) => {
        if (err) {
            logger.error('ssoDB - connMariasqlInsert - connClusterDB() - err: ' + err)
            cb(err)
            return
        }
        connection.config.queryFormat = function (query, values) {
            if (!values) return query
            return query.replace(/\:(\w+)/g, function (txt, key) {
                if (values.hasOwnProperty(key)) {
                    return this.escape(values[key])
                }
                return txt
            }.bind(this))
        }

        connection.beginTransaction((err) => {
            if (err) {
                logger.error('ssoDB - connMariasqlInsert - beginTransaction - err: ' + err)
                connection.rollback(() => {
                    connection.release()
                    cb(err)
                })
                return
            }

            connection.query(q, p, function (err, rows) {
                if (err) {
                    logger.error('ssoDB - connMariasqlInsert - query - err: ' + err)
                    connection.rollback(() => {
                        connection.release()
                        cb(err)
                    })
                    return
                }

                connection.commit(function (err) {
                    if (err) {
                        logger.error('ssoDB - connMariasqlInsert - commit - err: ' + err)
                        connection.rollback(() => {
                            connection.release()
                            cb(err)
                        })
                        return
                    }

                    /*OkPacket {
                        fieldCount: 0,
                        affectedRows: 1,
                        insertId: 0,
                        serverStatus: 3,
                        warningCount: 0,
                        message: '',
                        protocol41: true,
                        changedRows: 0 }*/
                    cb(null, rows)
                })
            })
        })
    })
}


let connMariasqlUpDel = function (q, p, cb) {
    // let encK = CONSTS.Q_ENC_K
    // p = Object.assign(p, encK)
    connClusterDB((err, connection) => {
        if (err) {
            logger.error('ssoDB - connMariasqlUpDel - connClusterDB() - err: ' + err)
            cb(err)
            return
        }
        connection.config.queryFormat = function (query, values) {
            if (!values) return query
            return query.replace(/\:(\w+)/g, function (txt, key) {
                if (values.hasOwnProperty(key)) {
                    return this.escape(values[key])
                }
                return txt
            }.bind(this))
        }

        connection.beginTransaction((err) => {
            if (err) {
                logger.error('ssoDB - connMariasqlUpDel - beginTransaction - err: ' + err)
                connection.rollback(() => {
                    connection.release()
                    cb(err)
                })
                return
            }

            connection.query(q, p, function (err, rows) {
                if (err) {
                    logger.error('ssoDB - connMariasqlUpDel - query - q: ' + q + ' - p: ' + JSON.stringify(p) + ' - err: ' + err)
                    connection.rollback(() => {
                        connection.release()
                        cb(err)
                    })
                    return
                }

                connection.commit(function (err) {
                    if (err) {
                        logger.error('ssoDB - connMariasqlUpDel - commit - err: ' + err)
                        connection.rollback(() => {
                            connection.release()
                            cb(err)
                        })
                        return
                    }
                    /*OkPacket {
                        fieldCount: 0,
                        affectedRows: 1,
                        insertId: 0,
                        serverStatus: 3,
                        warningCount: 0,
                        message: '',
                        protocol41: true,
                        changedRows: 0 }*/
                    cb(null, rows)
                })
            })
        })
    })
}

let connProc = (sP, arrP, cb) => {
    connClusterDB(function (err, connection) {
        if (err) {
            logger.error('ssoDB - connProc - connClusterDB() - ' + err)
            cb(err)
            return
        }
        logger.info('db index - connProc - query: ' + sP)
        logger.info(arrP)

        connection.query(sP, arrP, (err, result) => {
            if (err) {
                logger.error('ssoDB - connProc - query() - ' + err)
                cb(err)
                return
            }

            cb(null, result)
        })
    })
}


let getQueryData = (arr) => {
    if (arr.length === undefined) {
        return []
    }
    else {
        let arrRet = arr.map(function (item) {
            return item
        })

        if (arrRet.length > 0) {
            return arrRet
        }
        else {
            return []
        }
    }
}

exports.excuteQuerySelect = (q, p, bSync, cb) => {
    connMariasql(q, p, (err, result) => {
        if (err) {
            bSync ? cb(null, err) : cb(err)
            return
        }

        bSync ? cb(null, null, JSON.parse(JSON.stringify(result))) : cb(null, JSON.parse(JSON.stringify(result)))
    })
}

exports.excuteQueryInsert = (q, p, bSync, cb) => {
    connMariasqlInsert(q, p, (err, result) => {
        if (err) {
            bSync ? cb(null, err) : cb(err)
            return
        }

        bSync ? cb(null, null, result) : cb(null, result)
    })
}

exports.excuteQueryUpDel = (q, p, bSync, cb) => {
    connMariasqlUpDel(q, p, (err, result) => {
        if (err) {
            bSync ? cb(null, err) : cb(err)
            return
        }

        bSync ? cb(null, null, result) : cb(null, result)
    })
}

exports.excuteProc = (sProc, arrParams, bSync, cb) => {
    connProc(sProc, arrParams, (err, result) => {
        if (err) {
            bSync ? cb(null, err) : cb(err)
        }
        else {
            bSync ? cb(null, null, result) : cb(null, result)
        }
    })
}
