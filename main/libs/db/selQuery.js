const sync = require('synchronize')
const logger = require('../logger')
//const ovsDB = require('../../libs/db/index_single')
const ssoDB = require('../../libs/db/ssoDB')
const CONSTS = require('../../libs/CONSTS')
const CONSTS_SYS = require('../../libs/CONSTS_SYS')
const timeUtils = require('../../libs/utils/timeUtils')
const redis = require('../../libs/redis')
const ovsShop = require('../../../main/exInterface/IF_ovsShop')
const posFUSE = require('../../../main/exInterface/IF_FUSE')
const posCNTT = require('../../../main/exInterface/IF_CNTT')


// 쿼리를 실행한다.
exports.procExecuteQuery = (queryGCode, params, cb) => {
    //logger.debug('selQuery - procExecuteQuery - queryGCode - ' + queryGCode + ' - params: ' + JSON.stringify(params))
    logger.info('selQuery - procExecuteQuery - queryGCode - ' + queryGCode)

    ////임시...
    if(queryGCode === 'QGSBQ94S' || queryGCode === 'QGSBQ94F'){
        queryGCode = 'QGSNT94'
    }

    let staticQuery = 'SELECT QUERY_CODE, QUERY_STR '
        + 'FROM twv_query_group AS QG '
        + 'LEFT JOIN twv_query_grouping AS QGP ON QGP.QUERY_GROUP_SEQ = QG.SEQ '
        + 'LEFT JOIN twv_query AS Q ON Q.SEQ = QGP.QUERY_SEQ '
        + 'WHERE QG.QUERY_GROUP_CODE = "' + queryGCode + '"'
    ssoDB.excuteQuerySelect(staticQuery, '', false, (err, resQuerys) => {
        if (err) {
            logger.error('selQuery - procExecuteQuery - err: ' + err)
            cb(err)
            return
        }
        logger.debug('selQuery - procExecuteQuery - queryGCode: ' + queryGCode + ' - resQuerys: ' + JSON.stringify(resQuerys))
        if (typeof (resQuerys) === 'string') {
            doExcuteQuery(resQuerys, params, (err, qRes) => {
                if (err) {
                    logger.error("selQuery - executeQueryUsingCodes1 - err: " + err)
                    cb(err)
                    return
                }
                let setData = {}
                let pName
                let arrPropName = []
                if (!qRes.length) {
                    logger.error('selQuery - procExecuteQuery - qRes.length: ' + qRes.length)
                    cb('err - qRes.length: ' + qRes.length)
                }
                for (let i = 0; i < qRes.length; i++) {
                    Object.getOwnPropertyNames(qRes[i]).forEach((name) => {
                        switch (name) {
                            case 'QUERY_CODE':
                                pName = qRes[i][name]
                                setData[pName] = {}
                                arrPropName.push(pName)
                                break

                            case 'QUERY_STR':
                                setData[pName] = qRes[i][name]
                                break
                        }
                    })

                    if (i === qRes.length - 1) {
                        cb(null, setData)
                    }
                }
            })

            return
        }
        /////////////////////////////////////////////////////////////
        // 여러 쿼리를 이용해서 멀티 조회
        if (!params) {
            params = []
            for (let m = 0; m < resQuerys.length; m++) {
                params.push('')
            }
        }
        if (queryGCode === 'QGSNT306') {
            logger.error('selQuery - procExecuteQuery - No QGSNT306!!!')
            cb('err - No QGSNT306')
        }
        else {
            sync.fiber(function () {
                sync.parallel(function () {
                    for (let i = 0; i < resQuerys.length; i++) {
                        try {
                            ssoDB.excuteQuerySelect(resQuerys[i].QUERY_STR, params[i], true, sync.defers())
                        }
                        catch (e) {
                            logger.error('selQuery - procExecuteQuery - sync for - catch e: ' + e)
                            logger.error('selQuery - procExecuteQuery - resQuerys[' + i + '].QUERY_STR: ' + resQuerys[i].QUERY_STR)
                            logger.error('selQuery - procExecuteQuery - params[' + i + ']: ' + JSON.stringify(params[i]))

                            cb(e)
                        }
                    }
                })
                let resData = sync.await()
                resData = [].concat.apply([], resData)
                let retLength = resData.length
                let bErr = false
                let msgErr = ''
                let arrData = []
                for (let i = 0; i < retLength; ++i) {
                    if (!(i % 2)) {
                        if (resData[i]) {
                            bErr = true
                            msgErr = JSON.stringify(resData[i])
                            logger.error('selQuery - procExecuteQuery - err: ' + msgErr)
                        }
                    }
                    else {
                        arrData.push(resData[i])
                    }
                }

                if (bErr) {
                    cb(msgErr)
                    return
                }

                let arrResults = {}
                for (let j = 0; j < resQuerys.length; j++) {
                    arrResults[resQuerys[j].QUERY_CODE] = arrData[j]
                }

                cb(null, arrResults)
            })
        }
    })
}


////////////////////////////////////////////////////
// 0704 수정....
let sortSmallMenu = (arrSmallParams, cb) => {
    let tempSame = ''
    let tempArr = []
    let lastArr = []
    let retSortedArr = []
    if (arrSmallParams.length === 1) {
        retSortedArr = arrSmallParams
        cb(null, retSortedArr)
    }
    else {
        for (let i = 0; i < arrSmallParams.length; i++) {
            let tSame = arrSmallParams[i].SAME_ORDER_GOODS_SEQ
            if (i === 0) {
                tempSame = tSame
                tempArr.push(arrSmallParams[i])
            }
            else {
                if (tempSame !== tSame) {
                    tempSame = tSame
                    tempArr.sort(doSort_MENU_GOODS_AT)
                    lastArr.push(tempArr)
                    tempArr = []
                    tempArr.push(arrSmallParams[i])
                }
                else {
                    tempArr.push(arrSmallParams[i])
                }
                if (i === arrSmallParams.length - 1) {
                    tempArr.sort(doSort_MENU_GOODS_AT)

                    lastArr.push(tempArr)

                    lastArr.forEach(v => {
                        v.forEach(m => {
                            retSortedArr.push(m)
                        })
                    })
                    cb(null, retSortedArr)
                }
            }
        }
    }
}

////////////////////////////////////////////
// insert order small query...
// table : thistory_order_small
let makeQuerySmallOrder = (insertID, data, cb) => {
    let retQuery = 'INSERT INTO THISTORY_ORDER_SMALL (SLE_HIST_SEQ' +
        ', SAME_ORDER_GOODS_SEQ' +
        ', LCLAS_CODE' +
        ', SCLAS_CODE' +
        ', MENU_GOODS_AT' +
        ', SLE_UNIT ' +
        ', LCLAS_NM ' +
        ', SCLAS_NM' +
        ', SLE_QY' +
        ', SLE_AMOUNT' +
        ', SLE_SORT' +
        ', SCLAS_SLE_UNIT' +
        ', POS_SCLAS_CODE' +
        ', POS_SCLAS_NM) VALUES '

    for (let i in data) {
        let oSP = data[i]
        let dlvrSAME_ORDER_GOODS_SEQ = oSP.SAME_ORDER_GOODS_SEQ
        let posSclasCode = oSP.POS_SCLAS_CODE ? '"' + oSP.POS_SCLAS_CODE + '"' : null
        let posSclasNm = oSP.POS_SCLAS_NM ? '"' + oSP.POS_SCLAS_NM + '"' : null

        let qValue = '(' + insertID
            + ',' + oSP.SAME_ORDER_GOODS_SEQ
            + ',' + oSP.LCLAS_CODE
            + ',' + oSP.SCLAS_CODE
            + ',"' + oSP.MENU_GOODS_AT + '"'
            + ',"' + oSP.SLE_UNIT + '"'
            + ',"' + oSP.LCLAS_NM + '"'
            + ',"' + oSP.SCLAS_NM + '"'
            + ',' + oSP.SLE_QY
            + ',' + oSP.SLE_AMOUNT
            + ',' + i
            + ',"' + oSP.SCLAS_SLE_UNIT + '"'
            + ',' + posSclasCode
            + ',' + posSclasNm
            + ')'
        if (parseInt(i) === 0) {
            retQuery += qValue
        }
        else {
            retQuery += ',' + qValue
        }

        if (parseInt(i) === (parseInt(data.length) - 1)) {
            retQuery += ';'
            cb(null, dlvrSAME_ORDER_GOODS_SEQ, retQuery)
        }
    }
}


/////////////////////////////////////////
// order menu 일 때 ADDRESS_TYPE = I
// 주소를 인서트 한다.
let insertAddress_TTINFO_CUSTOMER = (MRHST_SEQ, CSTMR_TEL_NO, DLVR_AREA, DLVR_DETAIL_AREA, MEMO, SI, GU, DONG, LI, BUNJI, ROAD_NM, BULD_NM, CRDNT_X, CRDNT_Y, BFE_X, BFE_Y, ADRES_TYPE, cb) => {
//let insertAddress_TTINFO_CUSTOMER = (MRHST_SEQ, CSTMR_TEL_NO, DLVR_AREA, DLVR_DETAIL_AREA, MEMO, SI, GU, DONG, LI, BUNJI, ROAD_NM, BULD_NM, CRDNT_X, CRDNT_Y, ADRES_TYPE, cb) => {
    let qqq = 'SELECT COUNT(CSTMR_NO) AS CNT ' +
        'FROM TINFO_CUSTOMER ' +
        'WHERE MRHST_SEQ = :MRHST_SEQ AND MOBLPHON_NO = HEX(AES_ENCRYPT(:CSTMR_TEL_NO,"$DlShQpDlTus!TjQlTmDnsDudWk&)(*&")) '
    let ppp = {MRHST_SEQ: MRHST_SEQ, CSTMR_TEL_NO: CSTMR_TEL_NO}

    let dupCnt = 0

    ssoDB.excuteQuerySelect(qqq, ppp, false, (err, cntRes) => {
        if (err) {
            logger.error('selQuery - insertAddress_TTINFO_CUSTOMER - MRHST_SEQ: ' + MRHST_SEQ + ' - err: ' + err, CSTMR_TEL_NO)
            cb(null, err)
            return
        }

        dupCnt = parseInt(cntRes[0].CNT)

        if (dupCnt > 0) {
            logger.warning('selQuery - insertAddress_TTINFO_CUSTOMER - insertCustomerAddressInfo - duplication customer address info !!! count: ' + dupCnt + ' - MRHST_SEQ: ' + MRHST_SEQ + ' - CSTMR_TEL_NO: ' + CSTMR_TEL_NO + ' , DLVR_AREA: ' + DLVR_AREA + ' , DLVR_DETAIL_AREA: ' + DLVR_DETAIL_AREA)
            //cb(null, 'duplication addr... - MRHST_SEQ: ' + MRHST_SEQ + ' - CSTMR_TEL_NO: ' + CSTMR_TEL_NO)
            //return

            //updateAddress_TTINFO_CUSTOMER(MRHST_SEQ, CSTMR_TEL_NO, DLVR_AREA, DLVR_DETAIL_AREA, MEMO, SI, GU, DONG, LI, BUNJI, ROAD_NM, BULD_NM, CRDNT_X, CRDNT_Y, BFE_X, BFE_Y, ADRES_TYPE, cb)
            cb(null, null, 'DUP')
            return

            // updateAddress_TTINFO_CUSTOMER(MRHST_SEQ, CSTMR_TEL_NO, DLVR_AREA, DLVR_DETAIL_AREA, MEMO, SI, GU, DONG, LI, BUNJI, ROAD_NM, BULD_NM, CRDNT_X, CRDNT_Y, BFE_X, BFE_Y, ADRES_TYPE, (nRet, errRet, upRet)=>{
            //     if(errRet){
            //         logger.error('selQuery - insertAddress_TTINFO_CUSTOMER - insertCustomerAddressInfo - dup addr - update)
            //     }
            // })
        }

        let YYYYMMDD = timeUtils.getDateFormat('YYYYMMDD')
        let HHmmss = timeUtils.getDateFormat('HHmmss')

        let CSTMR_TEL_NO_4_DIGIT = CSTMR_TEL_NO.substr(CSTMR_TEL_NO.length - 4, 4)

        let qInsertAddr =
            'INSERT INTO TINFO_CUSTOMER (CSTMR_NM' +
            ', MOBLPHON_NO' +
            ', MOBL_4_DIGIT' +
            ', ETC_TEL_NO' +
            ', ETC_4_DIGIT' +
            ', MRHST_SEQ' +
            ', SMS_RECPTN_AT' +
            ', RGS_DE' +
            ', RECENT_ORDER_DE' +
            ', RECENT_ORDER_HMS' +
            ', RECENT_DLVR_AREA' +
            ', RECENT_DLVR_DETAIL_AREA' +
            ', MEMO' +
            ', SI' +
            ', GU' +
            ', DONG' +
            ', LI' +
            ', BUNJI' +
            ', ROAD_NM' +
            ', BULD_NM' +
            ', CRDNT_X' +
            ', CRDNT_Y' +
            ', ADRES_TYPE) ' +
            'VALUES (HEX(AES_ENCRYPT(:CSTMR_NM, "$DlShQpDlTus!TjQlTmDnsDudWk&)(*&")) ' +
            ', HEX(AES_ENCRYPT(:CSTMR_TEL_NO, "$DlShQpDlTus!TjQlTmDnsDudWk&)(*&"))' +
            ', :CSTMR_TEL_NO_4_DIGIT' +
            ', :ETC_TEL_NO ' +
            ', :ETC_4_DIGIT ' +
            ', :MRHST_SEQ' +
            ', :SMS_RECPTN_AT ' +
            ', :RGS_DE ' +
            ', :RECENT_ORDER_DE ' +
            ', :RECENT_ORDER_HMS ' +
            ', :DLVR_AREA' +
            ', :DLVR_DETAIL_AREA' +
            ', :MEMO' +
            ', :SI' +
            ', :GU' +
            ', :DONG' +
            ', :LI' +
            ', :BUNJI' +
            ', :ROAD_NM' +
            ', :BULD_NM' +
            ', :CRDNT_X' +
            ', :CRDNT_Y' +
            ', :ADRES_TYPE);'

        let arrInsertAddrParams = {
            CSTMR_NM: '비회원'
            , CSTMR_TEL_NO: CSTMR_TEL_NO
            , CSTMR_TEL_NO_4_DIGIT: CSTMR_TEL_NO_4_DIGIT
            , ETC_TEL_NO: ''
            , ETC_4_DIGIT: ''
            , MRHST_SEQ: MRHST_SEQ
            , SMS_RECPTN_AT: 'N'
            , RGS_DE: YYYYMMDD
            , RECENT_ORDER_DE: YYYYMMDD
            , RECENT_ORDER_HMS: HHmmss
            , DLVR_AREA: DLVR_AREA
            , DLVR_DETAIL_AREA: DLVR_DETAIL_AREA
            , MEMO: MEMO
            , SI: SI
            , GU: GU
            , DONG: DONG
            , LI: LI
            , BUNJI: BUNJI
            , ROAD_NM: ROAD_NM
            , BULD_NM: BULD_NM
            , CRDNT_X: CRDNT_X
            , CRDNT_Y: CRDNT_Y
            , BFE_X: BFE_X
            , BFE_Y: BFE_Y
            , ADRES_TYPE: ADRES_TYPE
        }

        logger.debug('selQuery - insertAddress_TTINFO_CUSTOMER - params: ' + JSON.stringify(arrInsertAddrParams))

        ssoDB.excuteQueryInsert(qInsertAddr, arrInsertAddrParams, false, (err, res) => {
            if (err) {
                logger.error('selQuery - insertAddress_TTINFO_CUSTOMER - insert address - MRHST_SEQ: ' + MRHST_SEQ + ' - err: ' + err, CSTMR_TEL_NO)
                cb(null, err)
                return
            }

            logger.info('selQuery - insertAddress_TTINFO_CUSTOMER - insertCustomerAddressInfo - insertId: ' + res.insertId + ' , MRHST_SEQ: ' + MRHST_SEQ, CSTMR_TEL_NO)
            cb(null, null, res.insertId)
        })
    })
}

/////////////////////////////////////////
// order menu 일 때 ADDRESS_TYPE = U
// 주소를 업데이트 한다.
let updateAddress_TTINFO_CUSTOMER = (MRHST_SEQ, CSTMR_TEL_NO, DLVR_AREA, DLVR_DETAIL_AREA, MEMO, SI, GU, DONG, LI, BUNJI, ROAD_NM, BULD_NM, CRDNT_X, CRDNT_Y, BFE_X, BFE_Y, ADRES_TYPE, cb) => {
//let updateAddress_TTINFO_CUSTOMER = (MRHST_SEQ, CSTMR_TEL_NO, DLVR_AREA, DLVR_DETAIL_AREA, MEMO, SI, GU, DONG, LI, BUNJI, ROAD_NM, BULD_NM, CRDNT_X, CRDNT_Y, ADRES_TYPE, cb) => {
    let YYYYMMDD = timeUtils.getDateFormat('YYYYMMDD')
    let HHmmss = timeUtils.getDateFormat('HHmmss')

    let qUpdateAddr = 'UPDATE TINFO_CUSTOMER \n' +
        'SET RECENT_DLVR_AREA = :RECENT_DLVR_AREA \n' +
        ', RECENT_DLVR_DETAIL_AREA = :RECENT_DLVR_DETAIL_AREA \n' +
        ', RECENT_ORDER_DE = :RECENT_ORDER_DE \n' +
        ', RECENT_ORDER_HMS = :RECENT_ORDER_HMS \n' +
        ', MEMO = :MEMO \n' +
        ', SI = :SI \n' +
        ', GU = :GU \n' +
        ', DONG = :DONG \n' +
        ', LI = :LI \n' +
        ', BUNJI = :BUNJI \n' +
        ', ROAD_NM = :ROAD_NM \n' +
        ', BULD_NM = :BULD_NM \n' +
        ', CRDNT_X = :CRDNT_X \n' +
        ', CRDNT_Y = :CRDNT_Y \n' +
        ', ADRES_TYPE = :ADRES_TYPE \n' +
        'WHERE MRHST_SEQ = :MRHST_SEQ \n' +
        'AND MOBLPHON_NO = HEX(AES_ENCRYPT(:CSTMR_TEL_NO,"$DlShQpDlTus!TjQlTmDnsDudWk&)(*&"));'
    let arrUpdateAddrParams = {
        RECENT_DLVR_AREA: DLVR_AREA
        , RECENT_DLVR_DETAIL_AREA: DLVR_DETAIL_AREA
        , RECENT_ORDER_DE: YYYYMMDD
        , RECENT_ORDER_HMS: HHmmss
        , MEMO: MEMO
        , SI: SI
        , GU: GU
        , DONG: DONG
        , LI: LI
        , BUNJI: BUNJI
        , ROAD_NM: ROAD_NM
        , BULD_NM: BULD_NM
        , CRDNT_X: CRDNT_X
        , CRDNT_Y: CRDNT_Y
        , ADRES_TYPE: ADRES_TYPE
        , MRHST_SEQ: MRHST_SEQ
        , CSTMR_TEL_NO: CSTMR_TEL_NO
    }

    ssoDB.excuteQueryUpDel(qUpdateAddr, arrUpdateAddrParams, false, (err, res) => {
        if (err) {
            logger.error('selQuery - updateAddress_TTINFO_CUSTOMER - MRHST_SEQ: ' + MRHST_SEQ + ' - err: ' + err, CSTMR_TEL_NO)
            cb(null, err)
            return
        }

        logger.info('selQuery - updateAddress_TTINFO_CUSTOMER - MRHST_SEQ: ' + MRHST_SEQ, CSTMR_TEL_NO)
        cb(null, null, res)
    })
}


//////////////////////////////////////////////////////
// order check URL update...
let updateOrderCheckUrl = (insertID, cb) => {
    let reORDER_URL = CONSTS.CONN_SETTING.OVS_ORDER_CHK_URL + '/' + insertID
    let qUrl = "UPDATE THISTORY_ORDER_LARGE SET ORDER_URL = :ORDER_URL WHERE SLE_HIST_SEQ = :SLE_HIST_SEQ"
    let pUrl = {
        ORDER_URL: reORDER_URL,
        SLE_HIST_SEQ: insertID
    }
    logger.info('selQuery - updateOrderCheckUrl - UPDATE THISTORY_ORDER_LARGE - ORDER_URL: ' + reORDER_URL + ' , insertID: ' + insertID)
    ssoDB.excuteQueryUpDel(qUrl, pUrl, false, (err, res) => {
        if (err) {
            logger.error('selQuery - updateOrderCheckUrl - insertId: ' + insertID + ' - err: ' + err)
            cb(null, err)
            return
        }

        cb(null, null, res)
    })
}


//////////////////////////////////////////////////////
// insert delivery amount in order processing...
// 배달료를 인서트 한다.
let insertDeliveryAmount = (insertID, dlvrSAME_ORDER_GOODS_SEQ, retDlvr_ret, cb) => {
    dlvrSAME_ORDER_GOODS_SEQ = parseInt(dlvrSAME_ORDER_GOODS_SEQ)
    let q = 'INSERT INTO THISTORY_ORDER_SMALL(SLE_HIST_SEQ, SAME_ORDER_GOODS_SEQ ,LCLAS_CODE ,SCLAS_CODE, MENU_GOODS_AT ,SLE_UNIT ,LCLAS_NM ,SCLAS_NM ,SLE_QY ,SLE_AMOUNT ,SLE_SORT ,SCLAS_SLE_UNIT, POS_SCLAS_CODE, POS_SCLAS_NM) ' +
        'VALUES (:SLE_HIST_SEQ, :SAME_ORDER_GOODS_SEQ, :LCLAS_CODE, :SCLAS_CODE, :MENU_GOODS_AT, :SLE_UNIT, :LCLAS_NM, :SCLAS_NM, :SLE_QY, :SLE_AMOUNT, :SLE_SORT, :SCLAS_SLE_UNIT, :POS_SCLAS_CODE, :POS_SCLAS_NM) '
    let oDlvrRes = retDlvr_ret[0]
    let p = {
        SLE_HIST_SEQ: insertID
        , SAME_ORDER_GOODS_SEQ: ++dlvrSAME_ORDER_GOODS_SEQ
        , LCLAS_CODE: oDlvrRes.LCLAS_CODE
        , SCLAS_CODE: oDlvrRes.SCLAS_CODE
        , MENU_GOODS_AT: 'D'
        , SLE_UNIT: oDlvrRes.SLE_UNIT
        , LCLAS_NM: oDlvrRes.LCLAS_NM
        , SCLAS_NM: oDlvrRes.SCLAS_NM
        , SCLAS_SLE_UNIT: oDlvrRes.SCLAS_SLE_UNIT
        , SLE_QY: 1
        , SLE_AMOUNT: oDlvrRes.SLE_AMOUNT
        , SLE_SORT: oDlvrRes.SLE_SORT
        , POS_SCLAS_CODE: oDlvrRes.POS_SCLAS_CODE
        , POS_SCLAS_NM: oDlvrRes.POS_SCLAS_NM
    }

    ssoDB.excuteQueryInsert(q, p, false, (err, res) => {
        if (err) {
            logger.error('selQuery - insertDeliveryAmount - err: ' + err)
            cb(null, err)
            return
        }

        logger.info('selQuery - insertDeliveryAmount - SLE_HIST_SEQ: ' + insertID + ' , SLE_AMOUNT: ' + oDlvrRes.SLE_AMOUNT + ' - insertId: ' + res.insertId + ' - affectedRows: ' + res.affectedRows)
        cb(null, null, res.insertId)
    })
}


///////////////////////////////////////////////
// select order cancel time...
// 주문 시 취소 가능 시간을 가져온다.
let selectOrderCancelTime = (MRHST_SEQ, cb) => {
    let q = 'SELECT getMrhstseqAtmcOrderCanclTime(:MRHST_SEQ) AS ORDER_CANCEL_TIME'
    ssoDB.excuteQuerySelect(q, {MRHST_SEQ: MRHST_SEQ}, false, (err, res) => {
        if (err) {
            logger.error('selQuery - selectOrderCancelTime - MRHST_SEQ: ' + MRHST_SEQ + ' - err: ' + err)
            cb(null, err)
            return
        }
        let cancelTime = res[0].ORDER_CANCEL_TIME

        logger.info('selQuery - selectOrderCancelTime - MRHST_SEQ: ' + MRHST_SEQ + ' - ORDER_CANCEL_TIME: ' + cancelTime)
        cb(null, null, cancelTime)
    })
}


//////////////////////////////////////
// FINAL ORDER Query.....
exports.procFinalOrderExecuteQuery = (queryGCode, params, cb) => {
    //logger.debug('selQuery - procFinalOrderExecuteQuery - queryGCode: ' + queryGCode + ' - params: ' + JSON.stringify(params))
    logger.info('selQuery - procFinalOrderExecuteQuery - queryGCode: ' + queryGCode)

    let qOrderLarge =
        'INSERT INTO THISTORY_ORDER_LARGE (ORDER_NO' +
        ', MRHST_SEQ' +
        ', CSTMR_TEL_NO' +
        ', RECPTN_TEL_NO' +
        ', DLVR_AREA' +
        ', DLVR_DETAIL_AREA' +
        ', MEMO' +
        ', SI' +
        ', GU' +
        ', DONG' +
        ', LI' +
        ', BUNJI' +
        ', ROAD_NM' +
        ', BULD_NM' +
        ', CRDNT_X' +
        ', CRDNT_Y' +
        ', BFE_X' +
        ', BFE_Y' +
        ', ADRES_TYPE' +
        ', ORDER_DE' +
        ', ORDER_HMS' +
        ', PRE_PYMNT_SE_CODE' +
        ', PYMNT_TY' +
        ', ORDER_AMOUNT' +
        ', DLVR_AMOUNT' +
        ', ORDER_STTUS_CODE' +
        ', ORDER_STTUS_DE_HMS' +
        ', DLVR_EXPECT_TIME' +
        ', CANCL_RESN_CODE' +
        ', CANCL_OPETR_CODE' +
        ', ORDER_URL' +
        ', CSID' +
        ', MRHST_BSN_DE' +
        ', RESVE_GNRL_SE' +
        ', ORDER_SE' +
        ', DLVR_SE) \n' +

        'VALUES ((select IFNULL(max(SLE_HIST_SEQ), 0)+1000000001 from (select sle_hist_seq from thistory_order_large) tmp)' +
        ', :MRHST_SEQ' +
        ', HEX(AES_ENCRYPT(:CSTMR_TEL_NO,"$DlShQpDlTus&)(*&^%$#&WnAnsDlFur"))' +
        ', HEX(AES_ENCRYPT(:RECPTN_TEL_NO,"$DlShQpDlTus&)(*&^%$#&WnAnsDlFur"))' +
        ', :DLVR_AREA' +
        ', :DLVR_DETAIL_AREA' +
        ', :MEMO' +
        ', :SI' +
        ', :GU' +
        ', :DONG' +
        ', :LI' +
        ', :BUNJI' +
        ', :ROAD_NM' +
        ', :BULD_NM' +
        ', :CRDNT_X' +
        ', :CRDNT_Y' +
        ', :BFE_X' +
        ', :BFE_Y' +
        ', :ADRES_TYPE' +
        ', REPLACE(substring(curdate(),1,10),"-","")' +
        ', REPLACE(curtime(),":","")' +
        ', :PRE_PYMNT_SE_CODE' +
        ', :PYMNT_TY' +
        ', :ORDER_AMOUNT' +
        ', :DLVR_AMOUNT' +
        ', :ORDER_STTUS_CODE' +
        ', concat(REPLACE(substring(curdate(),1,10),"-",""),REPLACE(curtime(),":",""))' +
        ', :DLVR_EXPECT_TIME' +
        ', :CANCL_RESN_CODE' +
        ', :CANCL_OPETR_CODE' +
        ', :ORDER_URL' +
        ', :CSID' +
        ', get_branch_bsn_de(:MRHST_SEQ)' +
        ', :RESVE_GNRL_SE' +
        ', :ORDER_SE' +
        ', :DLVR_SE) '


    let arrLargeParams = params[0].ORDER_LARGE
    let arrSmallParams = params[0].ORDER_SMALL

    let MRHST_SEQ = arrLargeParams.MRHST_SEQ

    sync.fiber(function () {

        /////////////////////////////////////////
        // 배달료 상품 정보 조회...
        let dlvrQuery = 'SELECT dl.LCLAS_CODE, dl.LCLAS_NM, ds.SCLAS_CODE, ds.SCLAS_NM, ds.SCLAS_TYPE, dsp.SLE_UNIT '
            + ' , dsp.SLE_AMOUNT, 901 + ifnull(dsp.SLE_UNIT_DC_SORT,0) AS SLE_SORT, BSS.DLVR_AMOUNT_INCLS_AT '
            + ' , IFNULL(dsp.SCLAS_SLE_UNIT, "기본배달료") AS SCLAS_SLE_UNIT '
            + ' , dsp.POS_SCLAS_CODE, dsp.POS_SCLAS_NM '
            + ' FROM tbranch_delivery_large dl '
            + ' JOIN tbranch_delivery_small ds ON dl.MRHST_SEQ = ds.MRHST_SEQ AND dl.LCLAS_CODE = ds.LCLAS_CODE '
            + ' JOIN tbranch_delivery_small_price dsp ON ds.MRHST_SEQ = dsp.MRHST_SEQ AND ds.SCLAS_CODE = dsp.SCLAS_CODE '
            + ' LEFT JOIN tinfo_branch_store_state AS BSS ON BSS.MRHST_SEQ = dl.MRHST_SEQ '
            + ' WHERE ds.MRHST_SEQ = :MRHST_SEQ '
            + ' AND ds.SCLAS_CODE = 9000000001 '
            + ' AND ds.SCLAS_TYPE = "100004" '
            + ' AND dsp.USE_AT = "Y" '

        sync.parallel(function () {
            ssoDB.excuteQuerySelect(dlvrQuery, {MRHST_SEQ: MRHST_SEQ}, true, sync.defers())
        })
        let retDlvr = sync.await()
        let retDlvr_err = [].concat.apply([], retDlvr)[0]
        let retDlvr_ret = [].concat.apply([], retDlvr)[1]
        if (retDlvr_err) {
            logger.error('selQuery - procFinalOrderExecuteQuery - dlvrQuery - err: ' + retDlvr_err)
            cb(retDlvr_err)
            return
        }

        let ADDR_TYPE = arrLargeParams.ADDR_TYPE

        let sleAmount = 0

        arrLargeParams.DLVR_AMOUNT = 0

        if (retDlvr_ret && retDlvr_ret.length > 0) {
            let sInclsAt = retDlvr_ret[0].DLVR_AMOUNT_INCLS_AT || ''
            if (sInclsAt === 'A' && ADDR_TYPE !== 'P') {
                let oDlvrRes = retDlvr_ret[0]
                let iSleAmount = oDlvrRes.SLE_AMOUNT || 0
                sleAmount = parseInt(iSleAmount)
                arrLargeParams.DLVR_AMOUNT = sleAmount
            }
        }


        let CSTMR_TEL_NO = arrLargeParams.CSTMR_TEL_NO
        let DLVR_AREA = arrLargeParams.DLVR_AREA
        let DLVR_DETAIL_AREA = arrLargeParams.DLVR_DETAIL_AREA
        let MEMO = arrLargeParams.MEMO
        let SI = arrLargeParams.SI
        let GU = arrLargeParams.GU
        let DONG = arrLargeParams.DONG
        let LI = arrLargeParams.LI
        let BUNJI = arrLargeParams.BUNJI
        let ROAD_NM = arrLargeParams.ROAD_NM
        let BULD_NM = arrLargeParams.BULD_NM
        let CRDNT_X = arrLargeParams.CRDNT_X
        let CRDNT_Y = arrLargeParams.CRDNT_Y
        let BFE_X = arrLargeParams.BFE_X
        let BFE_Y = arrLargeParams.BFE_Y
        let ADRES_TYPE = arrLargeParams.ADRES_TYPE


        ////////////////////////////////////////////
        // insert order large data...
        sync.parallel(function () {
            ssoDB.excuteQueryInsert(qOrderLarge, arrLargeParams, true, sync.defers())
        })
        let retLarge = sync.await()
        let retLarge_err = [].concat.apply([], retLarge)[0]
        let retLarge_ret = [].concat.apply([], retLarge)[1]
        if (retLarge_err) {
            logger.error('selQuery - procFinalOrderExecuteQuery - insertLargeData - err: ' + retLarge_err)
            cb(retLarge_err)
            return
        }

        let insertID = retLarge_ret.insertId
        logger.info('selQuery - procFinalOrderExecuteQuery - insertOrderLarge - insertID :   ' + insertID + ' - aff: ' + retLarge_ret)


        //////////////////////////////////////////////////
        // insert order small data....

        // sorting small menu
        arrSmallParams = arrSmallParams.sort(doSort_SAME_ORDER_GOODS_SEQ)
        sync.parallel(function () {
            sortSmallMenu(arrSmallParams, sync.defers())
        })
        let sortedArr = sync.await()
        sortedArr = [].concat.apply([], sortedArr)[0]

        // make small menu
        sync.parallel(function () {
            makeQuerySmallOrder(insertID, sortedArr, sync.defers())
        })
        let smallOrderInsertQuery = sync.await()
        let dlvrSAME_ORDER_GOODS_SEQ = [].concat.apply([], smallOrderInsertQuery)[0]
        let orderSmallInsertQuery = [].concat.apply([], smallOrderInsertQuery)[1]
        logger.info('selQuery - procFinalOrderExecuteQuery - orderSmall - query: ' + orderSmallInsertQuery)

        // insert small menu...
        sync.parallel(function () {
            ssoDB.excuteQueryInsert(orderSmallInsertQuery, '', true, sync.defers())
        })
        let retSmallOrder = sync.await()
        let retSmallOrder_err = [].concat.apply([], retSmallOrder)[0]
        let retSmallOrder_ret = [].concat.apply([], retSmallOrder)[1]
        if (retSmallOrder_err) {
            logger.error('selQuery - procFinalOrderExecuteQuery - insert order small - insertID: ' + insertID + ' - err: ' + retSmallOrder_err)
            cb(retSmallOrder_err)
            return
        }
        logger.info('selQuery - procFinalOrderExecuteQuery - insert order small -  orderLarge insertID: ' + insertID + ' - retSmallOrder_ret: ' + retSmallOrder_ret.affectedRows)


        //////////////////////////////////////////////////
        // update order large URL param...
        sync.parallel(function () {
            updateOrderCheckUrl(insertID, sync.defers())
        })
        let retUpUrl = sync.await()
        let retUpUrl_err = [].concat.apply([], retUpUrl)[0]
        //let retUpUrl_ret = [].concat.apply([], retUpUrl)[1]
        if (retUpUrl_err) {
            logger.error('selQuery - procFinalOrderExecuteQuery - update order URL insertID - insertID: ' + insertID + ' - err: ' + retUpUrl_err)
            cb(retUpUrl_err)
            return
        }


        ////////////////////////////////////
        // check address...
        logger.debug('selQuery - procFinalOrderExecuteQuery - ADDR_TYPE: ' + ADDR_TYPE + ' - MRHST_SEQ: ' + MRHST_SEQ + ' , CSTMR_TEL_NO: ' + CSTMR_TEL_NO + ' , DLVR_AREA: ' + DLVR_AREA + ' , DLVR_DETAIL_AREA: ' + DLVR_DETAIL_AREA)

        if (ADDR_TYPE === 'I')  // insert address info...
        {
            sync.parallel(function () {
                insertAddress_TTINFO_CUSTOMER(MRHST_SEQ, CSTMR_TEL_NO, DLVR_AREA, DLVR_DETAIL_AREA, MEMO, SI, GU, DONG, LI, BUNJI, ROAD_NM, BULD_NM, CRDNT_X, CRDNT_Y, BFE_X, BFE_Y, ADRES_TYPE, sync.defers())
                //insertAddress_TTINFO_CUSTOMER(MRHST_SEQ, CSTMR_TEL_NO, DLVR_AREA, DLVR_DETAIL_AREA, MEMO, SI, GU, DONG, LI, BUNJI, ROAD_NM, BULD_NM, CRDNT_X, CRDNT_Y, ADRES_TYPE, sync.defers())
            })
            let retAddrIn = sync.await()
            let retAddrIn_err = [].concat.apply([], retAddrIn)[0]
            let retAddrIn_ret = [].concat.apply([], retAddrIn)[1]
            if (retAddrIn_err) {
                logger.error('selQuery - procFinalOrderExecuteQuery - insertCustomerAddressInfo - MRHST_SEQ: ' + MRHST_SEQ + ' - err: ' + retAddr_err, CSTMR_TEL_NO)
            }
            else {
                if(retAddrIn_ret === 'DUP'){
                    sync.parallel(function () {
                        updateAddress_TTINFO_CUSTOMER(MRHST_SEQ, CSTMR_TEL_NO, DLVR_AREA, DLVR_DETAIL_AREA, MEMO, SI, GU, DONG, LI, BUNJI, ROAD_NM, BULD_NM, CRDNT_X, CRDNT_Y, BFE_X, BFE_Y, ADRES_TYPE, sync.defers())
                        //updateAddress_TTINFO_CUSTOMER(MRHST_SEQ, CSTMR_TEL_NO, DLVR_AREA, DLVR_DETAIL_AREA, MEMO, SI, GU, DONG, LI, BUNJI, ROAD_NM, BULD_NM, CRDNT_X, CRDNT_Y, ADRES_TYPE, sync.defers())
                    })
                    let retAddrInUp = sync.await()
                    let retAddrInUp_err = [].concat.apply([], retAddrInUp)[0]
                    //let retAddrInUp_ret = [].concat.apply([], retAddrInUp)[1]
                    if (retAddrInUp_err) {
                        logger.error('selQuery - procFinalOrderExecuteQuery - insertCustomerAddressInfo - DUP - updateTINFO - MRHST_SEQ: ' + MRHST_SEQ + ' - err: ' + retAddrInUp_err, CSTMR_TEL_NO)
                    }
                    else {
                        logger.info('selQuery - procFinalOrderExecuteQuery - insertCustomerAddressInfo - DUP - updateTINFO - MRHST_SEQ: ' + MRHST_SEQ, CSTMR_TEL_NO)
                    }
                }
                else {
                    logger.info('selQuery - procFinalOrderExecuteQuery - insertCustomerAddressInfo - MRHST_SEQ: ' + MRHST_SEQ, CSTMR_TEL_NO)
                }
            }
        }  // end of if (ADDR_TYPE === 'I')

        else if (ADDR_TYPE === 'U')  // update address info...
        {
            sync.parallel(function () {
                updateAddress_TTINFO_CUSTOMER(MRHST_SEQ, CSTMR_TEL_NO, DLVR_AREA, DLVR_DETAIL_AREA, MEMO, SI, GU, DONG, LI, BUNJI, ROAD_NM, BULD_NM, CRDNT_X, CRDNT_Y, BFE_X, BFE_Y, ADRES_TYPE, sync.defers())
                //updateAddress_TTINFO_CUSTOMER(MRHST_SEQ, CSTMR_TEL_NO, DLVR_AREA, DLVR_DETAIL_AREA, MEMO, SI, GU, DONG, LI, BUNJI, ROAD_NM, BULD_NM, CRDNT_X, CRDNT_Y, ADRES_TYPE, sync.defers())
            })
            let retAddrUp = sync.await()
            let retAddrUp_err = [].concat.apply([], retAddrUp)[0]
            //let retAddrUp_ret = [].concat.apply([], retAddrUp)[1]
            if (retAddrUp_err) {
                logger.error('selQuery - procFinalOrderExecuteQuery - updateCustomerAddressInfo - MRHST_SEQ: ' + MRHST_SEQ + ' - err: ' + retAddrUp_err, CSTMR_TEL_NO)
            }
            else {
                logger.info('selQuery - procFinalOrderExecuteQuery - updateCustomerAddressInfo - MRHST_SEQ: ' + MRHST_SEQ, CSTMR_TEL_NO)
            }
        }  // end of (ADDR_TYPE === 'U')

        else if (ADDR_TYPE === 'P')  // update address info... shop pick up...
        {
            logger.info('selQuery - procFinalOrderExecuteQuery - ADDR_TYPE: P - No action customer address info...')
        }  // end of (ADDR_TYPE === 'P')

        else if (ADDR_TYPE === 'N') {  // No action...
            logger.info('selQuery - procFinalOrderExecuteQuery - ADDR_TYPE: N - No action customer address info...')
        }


        /////////////////////////////////////////
        // select order cancel time...
        sync.parallel(function () {
            selectOrderCancelTime(MRHST_SEQ, sync.defers())
        })
        let retCncl = sync.await()
        let retCncl_err = [].concat.apply([], retCncl)[0]
        let retCncl_ret = [].concat.apply([], retCncl)[1]
        let retCancelTime = ''
        if (retCncl_err) {
            logger.error('selQuery - procFinalOrderExecuteQuery - order cancel time - err: ' + retCncl_err)
        }
        else {
            retCancelTime = retCncl_ret
        }
        logger.info('selQuery - procFinalOrderExecuteQuery - end order - MRHST_SEQ: ' + MRHST_SEQ + ' , insertID: ' + insertID + ' , retCancelTime: ' + retCancelTime, CSTMR_TEL_NO)


        ////////////////////////////
        // 배달료 insert...
        if (retDlvr_ret && retDlvr_ret.length > 0) {
            if (retDlvr_ret[0].DLVR_AMOUNT_INCLS_AT === 'A' && arrLargeParams.ADDR_TYPE !== 'P') {
                sync.parallel(function () {
                    insertDeliveryAmount(insertID, dlvrSAME_ORDER_GOODS_SEQ, retDlvr_ret, sync.defers())
                })
                let retDlvrAmnt = sync.await()
                let retDlvrAmnt_err = [].concat.apply([], retDlvrAmnt)[0]
                //let retDlvrAmnt_ret = [].concat.apply([], retDlvrAmnt)[1]
                if (retDlvrAmnt_err) {
                    logger.error('selQuery - procFinalOrderExecuteQuery - insert delevery amount - err: ' + retDlvrAmnt_err)
                }
                cb(null, {
                    resultCode: '200',
                    seq: insertID,
                    ORDER_CANCEL_TIME: retCancelTime
                })

            }
            else {
                cb(null, {
                    resultCode: '200',
                    seq: insertID,
                    ORDER_CANCEL_TIME: retCancelTime
                })

            }
        }
        else {
            cb(null, {
                resultCode: '200',
                seq: insertID,
                ORDER_CANCEL_TIME: retCancelTime
            })
        }



        // /////////////////////////////////////////
        // // select order cancel time...
        // sync.parallel(function () {
        //     selectOrderCancelTime(MRHST_SEQ, sync.defers())
        // })
        // let retCncl = sync.await()
        // let retCncl_err = [].concat.apply([], retCncl)[0]
        // let retCncl_ret = [].concat.apply([], retCncl)[1]
        // let retCancelTime = ''
        // if (retCncl_err) {
        //     logger.error('selQuery - procFinalOrderExecuteQuery - order cancel time - err: ' + retCncl_err)
        // }
        // else {
        //     retCancelTime = retCncl_ret
        // }
        //
        // logger.info('selQuery - procFinalOrderExecuteQuery - end order - MRHST_SEQ: ' + MRHST_SEQ + ' , insertID: ' + insertID + ' , retCancelTime: ' + retCancelTime, CSTMR_TEL_NO)
        //
        // cb(null, {
        //     resultCode: '200',
        //     seq: insertID,
        //     ORDER_CANCEL_TIME: retCancelTime
        // })

    })  // end of sync.fiber(
}  // end of procFinalOrderExecuteQuery

// 0704 수정 end......................


//////////////////////////////////////
// FINAL ORDER Query..... PUB
exports.procFinalOrderExecuteQuery_new = (oData) => {
    // { workType: 'WT_ORDER',
    //     , option: 'SNT306'
    //     , params: [ { ORDER_LARGE: [Object], ORDER_SMALL: [Array] }
    //                  , { MRHST_SEQ: 5, POS_CODE: '', SHOP_NUMBER: '' } ]
    //     , csid: 'test'
    //     , isRes: true
    //     , CNAME: 'WtMYN' }

    logger.info('selQuery - procFinalOrderExecuteQuery_new')


    let arrLargeParams = oData.params[0].ORDER_LARGE
    let arrSmallParams = oData.params[0].ORDER_SMALL
    let mrhstSeq = oData.params[1].MRHST_SEQ
    let posCode = oData.params[1].POS_CODE
    let shopNumber = oData.params[1].SHOP_NUMBER
    let CNAME = oData.CNAME
    let workType = oData.workType
    let csid = oData.csid
    let scenCode = oData.option

    let oBFE_X = arrLargeParams.BFE_X
    let oBFE_Y = arrLargeParams.BFE_Y
    if(!oBFE_X){
        arrLargeParams.BFE_X = null
    }
    if(!oBFE_Y){
        arrLargeParams.BFE_Y = null
    }


    let qOrderLarge =
        'INSERT INTO THISTORY_ORDER_LARGE (ORDER_NO' +
        ', MRHST_SEQ' +
        ', CSTMR_TEL_NO' +
        ', RECPTN_TEL_NO' +
        ', DLVR_AREA' +
        ', DLVR_DETAIL_AREA' +
        ', MEMO' +
        ', SI' +
        ', GU' +
        ', DONG' +
        ', LI' +
        ', BUNJI' +
        ', ROAD_NM' +
        ', BULD_NM' +
        ', CRDNT_X' +
        ', CRDNT_Y' +
        ', BFE_X' +
        ', BFE_Y' +
        ', ADRES_TYPE' +
        ', ORDER_DE' +
        ', ORDER_HMS' +
        ', PRE_PYMNT_SE_CODE' +
        ', PYMNT_TY' +
        ', ORDER_AMOUNT' +
        ', DLVR_AMOUNT' +
        ', ORDER_STTUS_CODE' +
        ', ORDER_STTUS_DE_HMS' +
        ', DLVR_EXPECT_TIME' +
        ', CANCL_RESN_CODE' +
        ', CANCL_OPETR_CODE' +
        ', ORDER_URL' +
        ', CSID' +
        ', MRHST_BSN_DE' +
        ', RESVE_GNRL_SE' +
        ', ORDER_SE' +
        ', DLVR_SE) \n' +

        'VALUES ((select IFNULL(max(SLE_HIST_SEQ), 0)+1000000001 from (select sle_hist_seq from thistory_order_large) tmp)' +
        ', :MRHST_SEQ' +
        ', HEX(AES_ENCRYPT(:CSTMR_TEL_NO,"$DlShQpDlTus&)(*&^%$#&WnAnsDlFur"))' +
        ', HEX(AES_ENCRYPT(:RECPTN_TEL_NO,"$DlShQpDlTus&)(*&^%$#&WnAnsDlFur"))' +
        ', :DLVR_AREA' +
        ', :DLVR_DETAIL_AREA' +
        ', :MEMO' +
        ', :SI' +
        ', :GU' +
        ', :DONG' +
        ', :LI' +
        ', :BUNJI' +
        ', :ROAD_NM' +
        ', :BULD_NM' +
        ', :CRDNT_X' +
        ', :CRDNT_Y' +
        ', :BFE_X' +
        ', :BFE_Y' +
        ', :ADRES_TYPE' +
        ', REPLACE(substring(curdate(),1,10),"-","")' +
        ', REPLACE(curtime(),":","")' +
        ', :PRE_PYMNT_SE_CODE' +
        ', :PYMNT_TY' +
        ', :ORDER_AMOUNT' +
        ', :DLVR_AMOUNT' +
        ', :ORDER_STTUS_CODE' +
        ', concat(REPLACE(substring(curdate(),1,10),"-",""),REPLACE(curtime(),":",""))' +
        ', :DLVR_EXPECT_TIME' +
        ', :CANCL_RESN_CODE' +
        ', :CANCL_OPETR_CODE' +
        ', :ORDER_URL' +
        ', :CSID' +
        ', get_branch_bsn_de(:MRHST_SEQ)' +
        ', :RESVE_GNRL_SE' +
        ', :ORDER_SE' +
        ', :DLVR_SE) '


    sync.fiber(function () {

        /////////////////////////////////////////
        // 배달료 상품 정보 조회...
        let dlvrQuery = 'SELECT dl.LCLAS_CODE, dl.LCLAS_NM, ds.SCLAS_CODE, ds.SCLAS_NM, ds.SCLAS_TYPE, dsp.SLE_UNIT '
            + ' , dsp.SLE_AMOUNT, 901 + ifnull(dsp.SLE_UNIT_DC_SORT,0) AS SLE_SORT, BSS.DLVR_AMOUNT_INCLS_AT '
            + ' , IFNULL(dsp.SCLAS_SLE_UNIT, "기본배달료") AS SCLAS_SLE_UNIT '
            + ' , dsp.POS_SCLAS_CODE, dsp.POS_SCLAS_NM '
            + ' FROM tbranch_delivery_large dl '
            + ' JOIN tbranch_delivery_small ds ON dl.MRHST_SEQ = ds.MRHST_SEQ AND dl.LCLAS_CODE = ds.LCLAS_CODE '
            + ' JOIN tbranch_delivery_small_price dsp ON ds.MRHST_SEQ = dsp.MRHST_SEQ AND ds.SCLAS_CODE = dsp.SCLAS_CODE '
            + ' LEFT JOIN tinfo_branch_store_state AS BSS ON BSS.MRHST_SEQ = dl.MRHST_SEQ '
            + ' WHERE ds.MRHST_SEQ = :MRHST_SEQ '
            + ' AND ds.SCLAS_CODE = 9000000001 '
            + ' AND ds.SCLAS_TYPE = "100004" '
            + ' AND dsp.USE_AT = "Y" '

        sync.parallel(function () {
            ssoDB.excuteQuerySelect(dlvrQuery, {MRHST_SEQ: mrhstSeq}, true, sync.defers())
        })
        let retDlvr = sync.await()
        let retDlvr_err = [].concat.apply([], retDlvr)[0]
        let retDlvr_ret = [].concat.apply([], retDlvr)[1]
        if (retDlvr_err) {
            logger.error('selQuery - procFinalOrderExecuteQuery_new - dlvrQuery - csid: ' + csid + ' - err: ' + retDlvr_err)

            let sendData = {SCEN_CODE: scenCode, RESULT: 'ERROR'}
            let pubData = {data: sendData, workType: workType, csid: csid}
            redis.pubRedis(CONSTS_SYS.PUB_CH.RES_ORDER + CNAME, pubData)
            return
        }

        let ADDR_TYPE = arrLargeParams.ADDR_TYPE

        let sleAmount = 0

        arrLargeParams.DLVR_AMOUNT = 0

        if (retDlvr_ret && retDlvr_ret.length > 0) {
            let sInclsAt = retDlvr_ret[0].DLVR_AMOUNT_INCLS_AT || ''
            if (sInclsAt === 'A' && ADDR_TYPE !== 'P') {
                let oDlvrRes = retDlvr_ret[0]
                let iSleAmount = oDlvrRes.SLE_AMOUNT || 0
                sleAmount = parseInt(iSleAmount)
                arrLargeParams.DLVR_AMOUNT = sleAmount
            }
        }

        let CSTMR_TEL_NO = arrLargeParams.CSTMR_TEL_NO
        let DLVR_AREA = arrLargeParams.DLVR_AREA
        let DLVR_DETAIL_AREA = arrLargeParams.DLVR_DETAIL_AREA
        let MEMO = arrLargeParams.MEMO
        let SI = arrLargeParams.SI
        let GU = arrLargeParams.GU
        let DONG = arrLargeParams.DONG
        let LI = arrLargeParams.LI
        let BUNJI = arrLargeParams.BUNJI
        let ROAD_NM = arrLargeParams.ROAD_NM
        let BULD_NM = arrLargeParams.BULD_NM
        let CRDNT_X = arrLargeParams.CRDNT_X
        let CRDNT_Y = arrLargeParams.CRDNT_Y
        let BFE_X = arrLargeParams.BFE_X
        let BFE_Y = arrLargeParams.BFE_Y
        let ADRES_TYPE = arrLargeParams.ADRES_TYPE


        ////////////////////////////////////////////
        // insert order large data...
        sync.parallel(function () {
            ssoDB.excuteQueryInsert(qOrderLarge, arrLargeParams, true, sync.defers())
        })
        let retLarge = sync.await()
        let retLarge_err = [].concat.apply([], retLarge)[0]
        let retLarge_ret = [].concat.apply([], retLarge)[1]
        if (retLarge_err) {
            logger.error('selQuery - procFinalOrderExecuteQuery_new - insertLargeData - csid: ' + csid + ' - err: ' + retLarge_err)

            let sendData = {SCEN_CODE: scenCode, RESULT: 'ERROR'}
            let pubData = {data: sendData, workType: workType, csid: csid}
            redis.pubRedis(CONSTS_SYS.PUB_CH.RES_ORDER + CNAME, pubData)
            return
        }

        let insertID = retLarge_ret.insertId
        logger.info('selQuery - procFinalOrderExecuteQuery_new - insertOrderLarge - insertID :   ' + insertID + ' - aff: ' + JSON.stringify(retLarge_ret))


        //////////////////////////////////////////////////
        // insert order small data....

        // sorting small menu
        arrSmallParams = arrSmallParams.sort(doSort_SAME_ORDER_GOODS_SEQ)
        sync.parallel(function () {
            sortSmallMenu(arrSmallParams, sync.defers())
        })
        let sortedArr = sync.await()
        sortedArr = [].concat.apply([], sortedArr)[0]

        // make small menu
        sync.parallel(function () {
            makeQuerySmallOrder(insertID, sortedArr, sync.defers())
        })
        let smallOrderInsertQuery = sync.await()
        let dlvrSAME_ORDER_GOODS_SEQ = [].concat.apply([], smallOrderInsertQuery)[0]
        let orderSmallInsertQuery = [].concat.apply([], smallOrderInsertQuery)[1]
        logger.info('selQuery - procFinalOrderExecuteQuery_new - orderSmall - query: ' + orderSmallInsertQuery)

        // insert small menu...
        sync.parallel(function () {
            ssoDB.excuteQueryInsert(orderSmallInsertQuery, '', true, sync.defers())
        })
        let retSmallOrder = sync.await()
        let retSmallOrder_err = [].concat.apply([], retSmallOrder)[0]
        let retSmallOrder_ret = [].concat.apply([], retSmallOrder)[1]
        if (retSmallOrder_err) {
            logger.error('selQuery - procFinalOrderExecuteQuery_new - insert order small - csid: ' + csid + ' - insertID: ' + insertID + ' - err: ' + retSmallOrder_err)

            let sendData = {SCEN_CODE: scenCode, RESULT: 'ERROR'}
            let pubData = {data: sendData, workType: workType, csid: csid}
            redis.pubRedis(CONSTS_SYS.PUB_CH.RES_ORDER + CNAME, pubData)
            return
        }

        logger.info('selQuery - procFinalOrderExecuteQuery_new - insert order small -  orderLarge insertID: ' + insertID + ' - retSmallOrder_ret: ' + retSmallOrder_ret.affectedRows)


        //////////////////////////////////////////////////
        // update order large URL param...
        sync.parallel(function () {
            updateOrderCheckUrl(insertID, sync.defers())
        })
        let retUpUrl = sync.await()
        let retUpUrl_err = [].concat.apply([], retUpUrl)[0]
        //let retUpUrl_ret = [].concat.apply([], retUpUrl)[1]
        if (retUpUrl_err) {
            logger.error('selQuery - procFinalOrderExecuteQuery_new - update order URL insertID - csid: ' + csid + ' - insertID: ' + insertID + ' - err: ' + retUpUrl_err)

            let sendData = {SCEN_CODE: scenCode, RESULT: 'ERROR'}
            let pubData = {data: sendData, workType: workType, csid: csid}
            redis.pubRedis(CONSTS_SYS.PUB_CH.RES_ORDER + CNAME, pubData)
            return
        }


        ////////////////////////////////////
        // check address...
        logger.debug('selQuery - procFinalOrderExecuteQuery_new - ADDR_TYPE: ' + ADDR_TYPE + ' - MRHST_SEQ: ' + mrhstSeq + ' , CSTMR_TEL_NO: ' + CSTMR_TEL_NO + ' , DLVR_AREA: ' + DLVR_AREA + ' , DLVR_DETAIL_AREA: ' + DLVR_DETAIL_AREA)

        if (ADDR_TYPE === 'I')  // insert address info...
        {
            sync.parallel(function () {
                insertAddress_TTINFO_CUSTOMER(mrhstSeq, CSTMR_TEL_NO, DLVR_AREA, DLVR_DETAIL_AREA, MEMO, SI, GU, DONG, LI, BUNJI, ROAD_NM, BULD_NM, CRDNT_X, CRDNT_Y, BFE_X, BFE_Y, ADRES_TYPE, sync.defers())
                //insertAddress_TTINFO_CUSTOMER(mrhstSeq, CSTMR_TEL_NO, DLVR_AREA, DLVR_DETAIL_AREA, MEMO, SI, GU, DONG, LI, BUNJI, ROAD_NM, BULD_NM, CRDNT_X, CRDNT_Y, ADRES_TYPE, sync.defers())
            })
            let retAddrIn = sync.await()
            let retAddrIn_err = [].concat.apply([], retAddrIn)[0]
            let retAddrIn_ret = [].concat.apply([], retAddrIn)[1]
            if (retAddrIn_err) {
                logger.error('selQuery - procFinalOrderExecuteQuery_new - insertCustomerAddressInfo - MRHST_SEQ: ' + mrhstSeq + ' - err: ' + retAddrIn_err, CSTMR_TEL_NO)
            }
            // else {
            //     logger.info('selQuery - procFinalOrderExecuteQuery_new - insertCustomerAddressInfo - MRHST_SEQ: ' + mrhstSeq, CSTMR_TEL_NO)
            // }
            else {
                if(retAddrIn_ret === 'DUP'){
                    sync.parallel(function () {
                        updateAddress_TTINFO_CUSTOMER(mrhstSeq, CSTMR_TEL_NO, DLVR_AREA, DLVR_DETAIL_AREA, MEMO, SI, GU, DONG, LI, BUNJI, ROAD_NM, BULD_NM, CRDNT_X, CRDNT_Y, BFE_X, BFE_Y, ADRES_TYPE, sync.defers())
                        //updateAddress_TTINFO_CUSTOMER(MRHST_SEQ, CSTMR_TEL_NO, DLVR_AREA, DLVR_DETAIL_AREA, MEMO, SI, GU, DONG, LI, BUNJI, ROAD_NM, BULD_NM, CRDNT_X, CRDNT_Y, ADRES_TYPE, sync.defers())
                    })
                    let retAddrInUp = sync.await()
                    let retAddrInUp_err = [].concat.apply([], retAddrInUp)[0]
                    //let retAddrInUp_ret = [].concat.apply([], retAddrInUp)[1]
                    if (retAddrInUp_err) {
                        logger.error('selQuery - procFinalOrderExecuteQuery_new - insertCustomerAddressInfo - DUP - updateTINFO - MRHST_SEQ: ' + mrhstSeq + ' - err: ' + retAddrInUp_err, CSTMR_TEL_NO)
                    }
                    else {
                        logger.info('selQuery - procFinalOrderExecuteQuery_new - insertCustomerAddressInfo - DUP - updateTINFO - MRHST_SEQ: ' + mrhstSeq, CSTMR_TEL_NO)
                    }
                }
                else {
                    logger.info('selQuery - procFinalOrderExecuteQuery_new - insertCustomerAddressInfo - MRHST_SEQ: ' + mrhstSeq, CSTMR_TEL_NO)
                }
            }
        }  // end of if (ADDR_TYPE === 'I')

        else if (ADDR_TYPE === 'U')  // update address info...
        {
            sync.parallel(function () {
                updateAddress_TTINFO_CUSTOMER(mrhstSeq, CSTMR_TEL_NO, DLVR_AREA, DLVR_DETAIL_AREA, MEMO, SI, GU, DONG, LI, BUNJI, ROAD_NM, BULD_NM, CRDNT_X, CRDNT_Y, BFE_X, BFE_Y, ADRES_TYPE, sync.defers())
                //updateAddress_TTINFO_CUSTOMER(mrhstSeq, CSTMR_TEL_NO, DLVR_AREA, DLVR_DETAIL_AREA, MEMO, SI, GU, DONG, LI, BUNJI, ROAD_NM, BULD_NM, CRDNT_X, CRDNT_Y, ADRES_TYPE, sync.defers())
            })
            let retAddrUp = sync.await()
            let retAddrUp_err = [].concat.apply([], retAddrUp)[0]
            //let retAddrUp_ret = [].concat.apply([], retAddrUp)[1]
            if (retAddrUp_err) {
                logger.error('selQuery - procFinalOrderExecuteQuery_new - updateCustomerAddressInfo - MRHST_SEQ: ' + mrhstSeq + ' - err: ' + retAddrUp_err, CSTMR_TEL_NO)
            }
            else {
                logger.info('selQuery - procFinalOrderExecuteQuery_new - updateCustomerAddressInfo - MRHST_SEQ: ' + mrhstSeq, CSTMR_TEL_NO)
            }
        }  // end of (ADDR_TYPE === 'U')

        else if (ADDR_TYPE === 'P')  // update address info... shop pick up...
        {
            logger.info('selQuery - procFinalOrderExecuteQuery_new - ADDR_TYPE: P - No action customer address info...')
        }  // end of (ADDR_TYPE === 'P')

        else if (ADDR_TYPE === 'N') {  // No action...
            logger.info('selQuery - procFinalOrderExecuteQuery_new - ADDR_TYPE: N - No action customer address info...')
        }


        /////////////////////////////////////////
        // select order cancel time...
        sync.parallel(function () {
            selectOrderCancelTime(mrhstSeq, sync.defers())
        })
        let retCncl = sync.await()
        let retCncl_err = [].concat.apply([], retCncl)[0]
        let retCncl_ret = [].concat.apply([], retCncl)[1]
        let retCancelTime = ''
        if (retCncl_err) {
            logger.error('selQuery - procFinalOrderExecuteQuery_new - order cancel time - err: ' + retCncl_err)
        }
        else {
            retCancelTime = retCncl_ret
        }
        logger.info('selQuery - procFinalOrderExecuteQuery_new - end order - MRHST_SEQ: ' + mrhstSeq + ' , insertID: ' + insertID + ' , retCancelTime: ' + retCancelTime, CSTMR_TEL_NO)


        ////////////////////////////
        // 배달료 insert...
        if (retDlvr_ret && retDlvr_ret.length > 0) {
            if (retDlvr_ret[0].DLVR_AMOUNT_INCLS_AT === 'A' && arrLargeParams.ADDR_TYPE !== 'P') {
                sync.parallel(function () {
                    insertDeliveryAmount(insertID, dlvrSAME_ORDER_GOODS_SEQ, retDlvr_ret, sync.defers())
                })
                let retDlvrAmnt = sync.await()
                let retDlvrAmnt_err = [].concat.apply([], retDlvrAmnt)[0]
                //let retDlvrAmnt_ret = [].concat.apply([], retDlvrAmnt)[1]
                if (retDlvrAmnt_err) {
                    logger.error('selQuery - procFinalOrderExecuteQuery_new - insert delevery amount - err: ' + retDlvrAmnt_err)
                }
                doFinalOrderToShop(mrhstSeq, posCode, shopNumber, insertID, csid, workType, retCancelTime, CNAME)
            }
            else {
                doFinalOrderToShop(mrhstSeq, posCode, shopNumber, insertID, csid, workType, retCancelTime, CNAME)
            }
        }
        else {
            doFinalOrderToShop(mrhstSeq, posCode, shopNumber, insertID, csid, workType, retCancelTime, CNAME)
        }
    })  // end of sync.fiber(
}  // end of procFinalOrderExecuteQuery_new
//*/////////////////////////////////////////////////////////////



////////////////////////////////////////////////////
// 카드 선결제 - insert order data... SkMns
exports.procPayCard_SkMns = (oData) => {
    console.log('procPayCard_SkMns_ssssssssssss')
    console.dir(oData)
    console.log('procPayCard_SkMns_eeeeeeeeeeeeeeeeee')
    /*
    { workType: 'WT_PAY_CARD_SKMNS'
       , option: ''
       , params: [ { ORDER_LARGE: [Object] , ORDER_SMALL: [Array] }
                      , { MRHST_SEQ: 80, POS_CODE: '200201', SHOP_NUMBER: '07052013160' } ]
       , csid: '1566475292.170002'
       , isRes: true
       , CNAME: 'lWXam' }
    */

    logger.debug('selQuery - procPayCard_SkMns - data: ' + JSON.stringify(oData))


    let arrLargeParams = oData.params[0].ORDER_LARGE
    let arrSmallParams = oData.params[0].ORDER_SMALL
    let mrhstSeq = oData.params[1].MRHST_SEQ
    let posCode = oData.params[1].POS_CODE
    let shopNumber = oData.params[1].SHOP_NUMBER
    let CNAME = oData.CNAME
    let workType = oData.workType
    let sessionId = oData.csid
    let scenCode = oData.option

    let oBFE_X = arrLargeParams.BFE_X
    let oBFE_Y = arrLargeParams.BFE_Y
    if(!oBFE_X){
        arrLargeParams.BFE_X = null
    }
    if(!oBFE_Y){
        arrLargeParams.BFE_Y = null
    }


    let qOrderLarge =
        'INSERT INTO THISTORY_ORDER_LARGE (ORDER_NO' +
        ', MRHST_SEQ' +
        ', CSTMR_TEL_NO' +
        ', RECPTN_TEL_NO' +
        ', DLVR_AREA' +
        ', DLVR_DETAIL_AREA' +
        ', MEMO' +
        ', SI' +
        ', GU' +
        ', DONG' +
        ', LI' +
        ', BUNJI' +
        ', ROAD_NM' +
        ', BULD_NM' +
        ', CRDNT_X' +
        ', CRDNT_Y' +
        ', BFE_X' +
        ', BFE_Y' +
        ', ADRES_TYPE' +
        ', ORDER_DE' +
        ', ORDER_HMS' +
        ', PRE_PYMNT_SE_CODE' +
        ', PYMNT_TY' +
        ', ORDER_AMOUNT' +
        ', DLVR_AMOUNT' +
        ', ORDER_STTUS_CODE' +
        ', ORDER_STTUS_DE_HMS' +
        ', DLVR_EXPECT_TIME' +
        ', CANCL_RESN_CODE' +
        ', CANCL_OPETR_CODE' +
        ', ORDER_URL' +
        ', CSID' +
        ', MRHST_BSN_DE' +
        ', RESVE_GNRL_SE' +
        ', ORDER_SE' +
        ', DLVR_SE) \n' +

        'VALUES ((select IFNULL(max(SLE_HIST_SEQ), 0)+1000000001 from (select sle_hist_seq from thistory_order_large) tmp)' +
        ', :MRHST_SEQ' +
        ', HEX(AES_ENCRYPT(:CSTMR_TEL_NO,"$DlShQpDlTus&)(*&^%$#&WnAnsDlFur"))' +
        ', HEX(AES_ENCRYPT(:RECPTN_TEL_NO,"$DlShQpDlTus&)(*&^%$#&WnAnsDlFur"))' +
        ', :DLVR_AREA' +
        ', :DLVR_DETAIL_AREA' +
        ', :MEMO' +
        ', :SI' +
        ', :GU' +
        ', :DONG' +
        ', :LI' +
        ', :BUNJI' +
        ', :ROAD_NM' +
        ', :BULD_NM' +
        ', :CRDNT_X' +
        ', :CRDNT_Y' +
        ', :BFE_X' +
        ', :BFE_Y' +
        ', :ADRES_TYPE' +
        ', REPLACE(substring(curdate(),1,10),"-","")' +
        ', REPLACE(curtime(),":","")' +
        ', :PRE_PYMNT_SE_CODE' +
        ', :PYMNT_TY' +
        ', :ORDER_AMOUNT' +
        ', :DLVR_AMOUNT' +
        ', :ORDER_STTUS_CODE' +
        ', concat(REPLACE(substring(curdate(),1,10),"-",""),REPLACE(curtime(),":",""))' +
        ', :DLVR_EXPECT_TIME' +
        ', :CANCL_RESN_CODE' +
        ', :CANCL_OPETR_CODE' +
        ', :ORDER_URL' +
        ', :CSID' +
        ', get_branch_bsn_de(:MRHST_SEQ)' +
        ', :RESVE_GNRL_SE' +
        ', :ORDER_SE' +
        ', :DLVR_SE) '


    sync.fiber(function () {

        /////////////////////////////////////////
        // 배달료 상품 정보 조회...
        let dlvrQuery = 'SELECT dl.LCLAS_CODE, dl.LCLAS_NM, ds.SCLAS_CODE, ds.SCLAS_NM, ds.SCLAS_TYPE, dsp.SLE_UNIT '
            + ' , dsp.SLE_AMOUNT, 901 + ifnull(dsp.SLE_UNIT_DC_SORT,0) AS SLE_SORT, BSS.DLVR_AMOUNT_INCLS_AT '
            + ' , IFNULL(dsp.SCLAS_SLE_UNIT, "기본배달료") AS SCLAS_SLE_UNIT '
            + ' , dsp.POS_SCLAS_CODE, dsp.POS_SCLAS_NM '
            + ' FROM tbranch_delivery_large dl '
            + ' JOIN tbranch_delivery_small ds ON dl.MRHST_SEQ = ds.MRHST_SEQ AND dl.LCLAS_CODE = ds.LCLAS_CODE '
            + ' JOIN tbranch_delivery_small_price dsp ON ds.MRHST_SEQ = dsp.MRHST_SEQ AND ds.SCLAS_CODE = dsp.SCLAS_CODE '
            + ' LEFT JOIN tinfo_branch_store_state AS BSS ON BSS.MRHST_SEQ = dl.MRHST_SEQ '
            + ' WHERE ds.MRHST_SEQ = :MRHST_SEQ '
            + ' AND ds.SCLAS_CODE = 9000000001 '
            + ' AND ds.SCLAS_TYPE = "100004" '
            + ' AND dsp.USE_AT = "Y" '

        sync.parallel(function () {
            ssoDB.excuteQuerySelect(dlvrQuery, {MRHST_SEQ: mrhstSeq}, true, sync.defers())
        })
        let retDlvr = sync.await()
        let retDlvr_err = [].concat.apply([], retDlvr)[0]
        let retDlvr_ret = [].concat.apply([], retDlvr)[1]
        if (retDlvr_err) {
            logger.error('selQuery - procPayCard_SkMns - dlvrQuery - sessionId: ' + sessionId + ' - err: ' + retDlvr_err)
            doPayCardRes('E', '', sessionId, CNAME, oData)
            return
        }

        let ADDR_TYPE = arrLargeParams.ADDR_TYPE

        let sleAmount = 0

        arrLargeParams.DLVR_AMOUNT = 0

        if (retDlvr_ret && retDlvr_ret.length > 0) {
            let sInclsAt = retDlvr_ret[0].DLVR_AMOUNT_INCLS_AT || ''
            if (sInclsAt === 'A' && ADDR_TYPE !== 'P') {
                let oDlvrRes = retDlvr_ret[0]
                let iSleAmount = oDlvrRes.SLE_AMOUNT || 0
                sleAmount = parseInt(iSleAmount)
                arrLargeParams.DLVR_AMOUNT = sleAmount
            }
        }

        let CSTMR_TEL_NO = arrLargeParams.CSTMR_TEL_NO
        let DLVR_AREA = arrLargeParams.DLVR_AREA
        let DLVR_DETAIL_AREA = arrLargeParams.DLVR_DETAIL_AREA
        let MEMO = arrLargeParams.MEMO
        let SI = arrLargeParams.SI
        let GU = arrLargeParams.GU
        let DONG = arrLargeParams.DONG
        let LI = arrLargeParams.LI
        let BUNJI = arrLargeParams.BUNJI
        let ROAD_NM = arrLargeParams.ROAD_NM
        let BULD_NM = arrLargeParams.BULD_NM
        let CRDNT_X = arrLargeParams.CRDNT_X
        let CRDNT_Y = arrLargeParams.CRDNT_Y
        let BFE_X = arrLargeParams.BFE_X
        let BFE_Y = arrLargeParams.BFE_Y
        let ADRES_TYPE = arrLargeParams.ADRES_TYPE


        ////////////////////////////////////////////
        // insert order large data...
        sync.parallel(function () {
            ssoDB.excuteQueryInsert(qOrderLarge, arrLargeParams, true, sync.defers())
        })
        let retLarge = sync.await()
        let retLarge_err = [].concat.apply([], retLarge)[0]
        let retLarge_ret = [].concat.apply([], retLarge)[1]
        if (retLarge_err) {
            logger.error('selQuery - procPayCard_SkMns - insertLargeData - sessionId: ' + sessionId + ' - err: ' + retLarge_err)
            doPayCardRes('E', '', sessionId, CNAME, oData)
            return
        }

        let insertID = retLarge_ret.insertId
        logger.info('selQuery - procPayCard_SkMns - insertOrderLarge - insertID :   ' + insertID + ' - aff: ' + JSON.stringify(retLarge_ret))


        //////////////////////////////////////////////////
        // insert order small data....

        // sorting small menu
        arrSmallParams = arrSmallParams.sort(doSort_SAME_ORDER_GOODS_SEQ)
        sync.parallel(function () {
            sortSmallMenu(arrSmallParams, sync.defers())
        })
        let sortedArr = sync.await()
        sortedArr = [].concat.apply([], sortedArr)[0]

        // make small menu
        sync.parallel(function () {
            makeQuerySmallOrder(insertID, sortedArr, sync.defers())
        })
        let smallOrderInsertQuery = sync.await()
        let dlvrSAME_ORDER_GOODS_SEQ = [].concat.apply([], smallOrderInsertQuery)[0]
        let orderSmallInsertQuery = [].concat.apply([], smallOrderInsertQuery)[1]
        logger.info('selQuery - procPayCard_SkMns - orderSmall - query: ' + orderSmallInsertQuery)

        // insert small menu...
        sync.parallel(function () {
            ssoDB.excuteQueryInsert(orderSmallInsertQuery, '', true, sync.defers())
        })
        let retSmallOrder = sync.await()
        let retSmallOrder_err = [].concat.apply([], retSmallOrder)[0]
        let retSmallOrder_ret = [].concat.apply([], retSmallOrder)[1]
        if (retSmallOrder_err) {
            logger.error('selQuery - procPayCard_SkMns - insert order small - sessionId: ' + sessionId + ' - insertID: ' + insertID + ' - err: ' + retSmallOrder_err)
            doPayCardRes('E', '', sessionId, CNAME, oData)
            return
        }

        logger.info('selQuery - procPayCard_SkMns - insert order small -  orderLarge insertID: ' + insertID + ' - retSmallOrder_ret: ' + retSmallOrder_ret.affectedRows)


        //////////////////////////////////////////////////
        // update order large URL param...
        sync.parallel(function () {
            updateOrderCheckUrl(insertID, sync.defers())
        })
        let retUpUrl = sync.await()
        let retUpUrl_err = [].concat.apply([], retUpUrl)[0]
        //let retUpUrl_ret = [].concat.apply([], retUpUrl)[1]
        if (retUpUrl_err) {
            logger.error('selQuery - procPayCard_SkMns - update order URL insertID - sessionId: ' + sessionId + ' - insertID: ' + insertID + ' - err: ' + retUpUrl_err)
            doPayCardRes('E', '', sessionId, CNAME, oData)
            return
        }


        ////////////////////////////////////
        // check address...
        logger.debug('selQuery - procPayCard_SkMns - ADDR_TYPE: ' + ADDR_TYPE + ' - MRHST_SEQ: ' + mrhstSeq + ' , CSTMR_TEL_NO: ' + CSTMR_TEL_NO + ' , DLVR_AREA: ' + DLVR_AREA + ' , DLVR_DETAIL_AREA: ' + DLVR_DETAIL_AREA)

        if (ADDR_TYPE === 'I')  // insert address info...
        {
            sync.parallel(function () {
                insertAddress_TTINFO_CUSTOMER(mrhstSeq, CSTMR_TEL_NO, DLVR_AREA, DLVR_DETAIL_AREA, MEMO, SI, GU, DONG, LI, BUNJI, ROAD_NM, BULD_NM, CRDNT_X, CRDNT_Y, BFE_X, BFE_Y, ADRES_TYPE, sync.defers())
                //insertAddress_TTINFO_CUSTOMER(mrhstSeq, CSTMR_TEL_NO, DLVR_AREA, DLVR_DETAIL_AREA, MEMO, SI, GU, DONG, LI, BUNJI, ROAD_NM, BULD_NM, CRDNT_X, CRDNT_Y, ADRES_TYPE, sync.defers())
            })
            let retAddrIn = sync.await()
            let retAddrIn_err = [].concat.apply([], retAddrIn)[0]
            let retAddrIn_ret = [].concat.apply([], retAddrIn)[1]
            if (retAddrIn_err) {
                logger.error('selQuery - procPayCard_SkMns - insertCustomerAddressInfo - MRHST_SEQ: ' + mrhstSeq + ' - err: ' + retAddrIn_err, CSTMR_TEL_NO)
            }
            // else {
            //     logger.info('selQuery - procPayCard_SkMns - insertCustomerAddressInfo - MRHST_SEQ: ' + mrhstSeq, CSTMR_TEL_NO)
            // }
            else {
                if(retAddrIn_ret === 'DUP'){
                    sync.parallel(function () {
                        updateAddress_TTINFO_CUSTOMER(mrhstSeq, CSTMR_TEL_NO, DLVR_AREA, DLVR_DETAIL_AREA, MEMO, SI, GU, DONG, LI, BUNJI, ROAD_NM, BULD_NM, CRDNT_X, CRDNT_Y, BFE_X, BFE_Y, ADRES_TYPE, sync.defers())
                        //updateAddress_TTINFO_CUSTOMER(MRHST_SEQ, CSTMR_TEL_NO, DLVR_AREA, DLVR_DETAIL_AREA, MEMO, SI, GU, DONG, LI, BUNJI, ROAD_NM, BULD_NM, CRDNT_X, CRDNT_Y, ADRES_TYPE, sync.defers())
                    })
                    let retAddrInUp = sync.await()
                    let retAddrInUp_err = [].concat.apply([], retAddrInUp)[0]
                    //let retAddrInUp_ret = [].concat.apply([], retAddrInUp)[1]
                    if (retAddrInUp_err) {
                        logger.error('selQuery - procPayCard_SkMns - insertCustomerAddressInfo - DUP - updateTINFO - MRHST_SEQ: ' + mrhstSeq + ' - err: ' + retAddrInUp_err, CSTMR_TEL_NO)
                    }
                    else {
                        logger.info('selQuery - procPayCard_SkMns - insertCustomerAddressInfo - DUP - updateTINFO - MRHST_SEQ: ' + mrhstSeq, CSTMR_TEL_NO)
                    }
                }
                else {
                    logger.info('selQuery - procPayCard_SkMns - insertCustomerAddressInfo - MRHST_SEQ: ' + mrhstSeq, CSTMR_TEL_NO)
                }
            }
        }  // end of if (ADDR_TYPE === 'I')

        else if (ADDR_TYPE === 'U')  // update address info...
        {
            sync.parallel(function () {
                updateAddress_TTINFO_CUSTOMER(mrhstSeq, CSTMR_TEL_NO, DLVR_AREA, DLVR_DETAIL_AREA, MEMO, SI, GU, DONG, LI, BUNJI, ROAD_NM, BULD_NM, CRDNT_X, CRDNT_Y, BFE_X, BFE_Y, ADRES_TYPE, sync.defers())
                //updateAddress_TTINFO_CUSTOMER(mrhstSeq, CSTMR_TEL_NO, DLVR_AREA, DLVR_DETAIL_AREA, MEMO, SI, GU, DONG, LI, BUNJI, ROAD_NM, BULD_NM, CRDNT_X, CRDNT_Y, ADRES_TYPE, sync.defers())
            })
            let retAddrUp = sync.await()
            let retAddrUp_err = [].concat.apply([], retAddrUp)[0]
            //let retAddrUp_ret = [].concat.apply([], retAddrUp)[1]
            if (retAddrUp_err) {
                logger.error('selQuery - procPayCard_SkMns - updateCustomerAddressInfo - MRHST_SEQ: ' + mrhstSeq + ' - err: ' + retAddrUp_err, CSTMR_TEL_NO)
            }
            else {
                logger.info('selQuery - procPayCard_SkMns - updateCustomerAddressInfo - MRHST_SEQ: ' + mrhstSeq, CSTMR_TEL_NO)
            }
        }  // end of (ADDR_TYPE === 'U')

        else if (ADDR_TYPE === 'P')  // update address info... shop pick up...
        {
            logger.info('selQuery - procPayCard_SkMns - ADDR_TYPE: P - No action customer address info...')
        }  // end of (ADDR_TYPE === 'P')

        else if (ADDR_TYPE === 'N') {  // No action...
            logger.info('selQuery - procPayCard_SkMns - ADDR_TYPE: N - No action customer address info...')
        }


        /////////////////////////////////////////
        // select order cancel time...
        sync.parallel(function () {
            selectOrderCancelTime(mrhstSeq, sync.defers())
        })
        let retCncl = sync.await()
        let retCncl_err = [].concat.apply([], retCncl)[0]
        let retCncl_ret = [].concat.apply([], retCncl)[1]
        let retCancelTime = ''
        if (retCncl_err) {
            logger.error('selQuery - procPayCard_SkMns - order cancel time - err: ' + retCncl_err)
            doPayCardRes('E', '', sessionId, CNAME, oData)
            return
        }
        retCancelTime = retCncl_ret
        logger.info('selQuery - procPayCard_SkMns - end order - MRHST_SEQ: ' + mrhstSeq + ' , insertID: ' + insertID + ' , retCancelTime: ' + retCancelTime, CSTMR_TEL_NO)


        ////////////////////////////
        // 배달료 insert...
        if (retDlvr_ret && retDlvr_ret.length > 0) {
            if (retDlvr_ret[0].DLVR_AMOUNT_INCLS_AT === 'A' && arrLargeParams.ADDR_TYPE !== 'P') {
                sync.parallel(function () {
                    insertDeliveryAmount(insertID, dlvrSAME_ORDER_GOODS_SEQ, retDlvr_ret, sync.defers())
                })
                let retDlvrAmnt = sync.await()
                let retDlvrAmnt_err = [].concat.apply([], retDlvrAmnt)[0]
                //let retDlvrAmnt_ret = [].concat.apply([], retDlvrAmnt)[1]
                if (retDlvrAmnt_err) {
                    logger.error('selQuery - procPayCard_SkMns - insert delevery amount - err: ' + retDlvrAmnt_err)
                }
                doPayCardRes('S', insertID, sessionId, CNAME, oData, retCancelTime, sleAmount)
            }
            else {
                doPayCardRes('S', insertID, sessionId, CNAME, oData, retCancelTime, sleAmount)
            }
        }
        else {
            doPayCardRes('S', insertID, sessionId, CNAME, oData, retCancelTime, sleAmount)
        }
    })  // end of sync.fiber(
}  // end of procPayCard_SkMns
//*/////////////////////////////////////////////////////////////
function doPayCardRes(resultCode, insertID, sessionId, CNAME, oData, retCancelTime, dlvrAmount) {
    console.log("selQuery - retCancelTime-----:" + retCancelTime)
    console.log("selQuery - retCancelTime-----:" + retCancelTime)
    console.log("selQuery - retCancelTime-----:" + retCancelTime)
    console.log("selQuery - retCancelTime-----:" + retCancelTime)
    console.log("selQuery - retCancelTime-----:" + retCancelTime)
    redis.hSetRedis(sessionId, 'ORDER_CANCEL_TIME', retCancelTime, (err, res) => {
        if(err){
            logger.error('selQuery - doPayCardRes - set Redis - sessionId:' + sessionId + ' - retCancelTime: ' + retCancelTime + ' - err: ' + err)
        }

        let sendData = {RESULT: resultCode, INSERT_ID: insertID, orderData: oData, dlvrAmount}
        let pubData = {data: sendData, workType: 'WT_PAY_CARD_SKMNS', sessionId: sessionId, csid: sessionId, retCancelTime: retCancelTime}
        logger.debug('selQuery - doPayCardRes - PUB: ' + 'RES_PC_' + CNAME)
        redis.pubRedis('RES_PC_' + CNAME, pubData)
    })
}


//0825
//////////////////////////////////////
// HISTORY 메뉴 인서트... PAY CARD
// HISTORY - FINAL ORDER Query.....
exports.procPayCard_SkMns_HISTORY = (oData) => {
    // { workType: 'WT_ORDER_HISTORY',
    //   option: 'SNT306_HISTORY',
    //   params:
    //           [ { ORDER_LARGE: [Object] },
    //             { ORDER_SEL_HISTORY: [Object] },
    //             { MRHST_SEQ: 5, POS_CODE: '100001', SHOP_NUMBER: '07052013279' , USER_MDN: userMdn} ],
    //   csid: 'test',
    //   isRes: true,
    //    CNAME: '296BF' }

    logger.info('selQuery - procPayCard_SkMns_HISTORY - oData: ' + JSON.stringify(oData))


    let CNAME = oData.CNAME
    let csid = oData.csid
    let workType = oData.workType
    let oBase = oData.params[2]

    let MRHST_SEQ = oBase.MRHST_SEQ
    let posCode = oBase.POS_CODE
    let shopNumber = oBase.SHOP_NUMBER
    let userMdn = oBase.USER_MDN

    let arrSmallParams = oData.params[1].ORDER_SEL_HISTORY
    // { SLE_HIST_SEQ: '22356',
    //     SCLAS_SLE_UNIT: '간바치 외 3건',
    //     ORDER_AMOUNT: '139,000' }

    let SLE_HIST_SEQ = arrSmallParams.SLE_HIST_SEQ

    let arrLargeParams = oData.params[0].ORDER_LARGE
    let oBFE_X = arrLargeParams.BFE_X
    let oBFE_Y = arrLargeParams.BFE_Y
    if(!oBFE_X){
        arrLargeParams.BFE_X = null
    }
    if(!oBFE_Y){
        arrLargeParams.BFE_Y = null
    }

    let ADDR_TYPE = arrLargeParams.ADDR_TYPE


    let qOrderLarge =
        'INSERT INTO THISTORY_ORDER_LARGE (ORDER_NO' +
        ', MRHST_SEQ' +
        ', CSTMR_TEL_NO' +
        ', RECPTN_TEL_NO' +
        ', DLVR_AREA' +
        ', DLVR_DETAIL_AREA' +
        ', MEMO' +
        ', SI' +
        ', GU' +
        ', DONG' +
        ', LI' +
        ', BUNJI' +
        ', ROAD_NM' +
        ', BULD_NM' +
        ', CRDNT_X' +
        ', CRDNT_Y' +
        ', BFE_X' +
        ', BFE_Y' +
        ', ADRES_TYPE' +
        ', ORDER_DE' +
        ', ORDER_HMS' +
        ', PRE_PYMNT_SE_CODE' +
        ', PYMNT_TY' +
        ', ORDER_AMOUNT' +
        ', DLVR_AMOUNT' +
        ', ORDER_STTUS_CODE' +
        ', ORDER_STTUS_DE_HMS' +
        ', DLVR_EXPECT_TIME' +
        ', CANCL_RESN_CODE' +
        ', CANCL_OPETR_CODE' +
        ', ORDER_URL' +
        ', CSID' +
        ', MRHST_BSN_DE' +
        ', RESVE_GNRL_SE' +
        ', ORDER_SE' +
        ', DLVR_SE) \n' +

        'VALUES ((select IFNULL(max(SLE_HIST_SEQ), 0)+1000000001 from (select sle_hist_seq from thistory_order_large) tmp)' +
        ', :MRHST_SEQ' +
        ', HEX(AES_ENCRYPT(:CSTMR_TEL_NO,"$DlShQpDlTus&)(*&^%$#&WnAnsDlFur"))' +
        ', HEX(AES_ENCRYPT(:RECPTN_TEL_NO,"$DlShQpDlTus&)(*&^%$#&WnAnsDlFur"))' +
        ', :DLVR_AREA' +
        ', :DLVR_DETAIL_AREA' +
        ', :MEMO' +
        ', :SI' +
        ', :GU' +
        ', :DONG' +
        ', :LI' +
        ', :BUNJI' +
        ', :ROAD_NM' +
        ', :BULD_NM' +
        ', :CRDNT_X' +
        ', :CRDNT_Y' +
        ', :BFE_X' +
        ', :BFE_Y' +
        ', :ADRES_TYPE' +
        ', REPLACE(substring(curdate(),1,10),"-","")' +
        ', REPLACE(curtime(),":","")' +
        ', :PRE_PYMNT_SE_CODE' +
        ', :PYMNT_TY' +
        ', :ORDER_AMOUNT' +
        ', :DLVR_AMOUNT' +
        ', :ORDER_STTUS_CODE' +
        ', concat(REPLACE(substring(curdate(),1,10),"-",""),REPLACE(curtime(),":",""))' +
        ', :DLVR_EXPECT_TIME' +
        ', :CANCL_RESN_CODE' +
        ', :CANCL_OPETR_CODE' +
        ', :ORDER_URL' +
        ', :CSID' +
        ', get_branch_bsn_de(:MRHST_SEQ)' +
        ', :RESVE_GNRL_SE' +
        ', :ORDER_SE' +
        ', :DLVR_SE) '


    sync.fiber(function () {
        /////////////////////////////////////////
        // 배달료 상품 정보 조회...
        let dlvrQuery = 'SELECT dl.LCLAS_CODE, dl.LCLAS_NM, ds.SCLAS_CODE, ds.SCLAS_NM, ds.SCLAS_TYPE, dsp.SLE_UNIT '
            + ' , dsp.SLE_AMOUNT, 901 + ifnull(dsp.SLE_UNIT_DC_SORT,0) AS SLE_SORT, BSS.DLVR_AMOUNT_INCLS_AT '
            + ' , IFNULL(dsp.SCLAS_SLE_UNIT, "기본배달료") AS SCLAS_SLE_UNIT '
            + ' , dsp.POS_SCLAS_CODE, dsp.POS_SCLAS_NM '
            + ' FROM tbranch_delivery_large dl '
            + ' JOIN tbranch_delivery_small ds ON dl.MRHST_SEQ = ds.MRHST_SEQ AND dl.LCLAS_CODE = ds.LCLAS_CODE '
            + ' JOIN tbranch_delivery_small_price dsp ON ds.MRHST_SEQ = dsp.MRHST_SEQ AND ds.SCLAS_CODE = dsp.SCLAS_CODE '
            + ' LEFT JOIN tinfo_branch_store_state AS BSS ON BSS.MRHST_SEQ = dl.MRHST_SEQ '
            + ' WHERE ds.MRHST_SEQ = :MRHST_SEQ '
            + ' AND ds.SCLAS_CODE = 9000000001 '
            + ' AND ds.SCLAS_TYPE = "100004" '
            + ' AND dsp.USE_AT = "Y" '

        sync.parallel(function () {
            ssoDB.excuteQuerySelect(dlvrQuery, {MRHST_SEQ: MRHST_SEQ}, true, sync.defers())
        })
        let retDlvr = sync.await()
        let retDlvr_err = [].concat.apply([], retDlvr)[0]
        let retDlvr_ret = [].concat.apply([], retDlvr)[1]
        if (retDlvr_err) {
            logger.error('selQuery - procPayCard_SkMns_HISTORY - dlvrQuery - csid: ' + csid + ' - err: ' + retDlvr_err)
            doPayCardRes_HISTORY('E', '', csid, CNAME, oData)
            return
        }


        let sleAmount = 0

        arrLargeParams.DLVR_AMOUNT = 0

        if (retDlvr_ret && retDlvr_ret.length > 0) {
            let sInclsAt = retDlvr_ret[0].DLVR_AMOUNT_INCLS_AT || ''
            if (sInclsAt === 'A' && ADDR_TYPE !== 'P') {
                let oDlvrRes = retDlvr_ret[0]
                let iSleAmount = oDlvrRes.SLE_AMOUNT || 0
                sleAmount = parseInt(iSleAmount)
                arrLargeParams.DLVR_AMOUNT = sleAmount
            }
        }


        ////////////////////////////////////////////
        // insert order large data...
        sync.parallel(function () {
            ssoDB.excuteQueryInsert(qOrderLarge, arrLargeParams, true, sync.defers())
        })
        let retLarge = sync.await()
        let retLarge_err = [].concat.apply([], retLarge)[0]
        let retLarge_ret = [].concat.apply([], retLarge)[1]
        if (retLarge_err) {
            logger.error('selQuery - procPayCard_SkMns_HISTORY - insertLargeData - csid: ' + csid + ' - err: ' + retLarge_err)
            doPayCardRes_HISTORY('E', '', csid, CNAME, oData)
            return
        }

        let insertID = retLarge_ret.insertId
        logger.info('selQuery - procPayCard_SkMns_HISTORY - insertOrderLarge - insertID :   ' + insertID + ' - aff: ' + JSON.stringify(retLarge_ret))


        let CSTMR_TEL_NO = arrLargeParams.CSTMR_TEL_NO
        let DLVR_AREA = arrLargeParams.DLVR_AREA
        let DLVR_DETAIL_AREA = arrLargeParams.DLVR_DETAIL_AREA
        let MEMO = arrLargeParams.MEMO
        let SI = arrLargeParams.SI
        let GU = arrLargeParams.GU
        let DONG = arrLargeParams.DONG
        let LI = arrLargeParams.LI
        let BUNJI = arrLargeParams.BUNJI
        let ROAD_NM = arrLargeParams.ROAD_NM
        let BULD_NM = arrLargeParams.BULD_NM
        let CRDNT_X = arrLargeParams.CRDNT_X
        let CRDNT_Y = arrLargeParams.CRDNT_Y
        let BFE_X = arrLargeParams.BFE_X
        let BFE_Y = arrLargeParams.BFE_Y
        let ADRES_TYPE = arrLargeParams.ADRES_TYPE

        let qSelSmall = 'SELECT OL.SLE_HIST_SEQ\n' +
            '\t, OSH.SAME_ORDER_GOODS_SEQ \n' +
            '\t, OL.ORDER_NO\n' +
            '\t, OL.MRHST_SEQ\n' +
            '\t, OSH.LCLAS_CODE\n' +
            '\t, get_lclas_name(OL.MRHST_SEQ, OSH.LCLAS_CODE) LCLAS_NM\n' +
            '\t, OSH.SCLAS_CODE \n' +
            '\t, ifnull(ms.SCLAS_NM, cs.SCLAS_NM) SCLAS_NM\n' +
            '\t, ifnull(MSP.SCLAS_SLE_UNIT , csp.SCLAS_SLE_UNIT) AS SCLAS_SLE_UNIT\n' +
            '\t, OL.ORDER_AMOUNT\n' +
            '\t, OSH.SLE_QY\n' +
            '\t, ifnull(MSP.SLE_UNIT, CSP.SLE_UNIT) AS SLE_UNIT \n' +
            '\t, OSH.SLE_QY * ifnull(MSP.SLE_AMOUNT, CSP.SLE_AMOUNT) AS SLE_AMOUNT \n' +
            '\t, OSH.MENU_GOODS_AT \n' +
            '\t, OSH.SLE_SORT \n' +
            '\t, MS.POS_MRHST_SEQ , MSP.POS_SCLAS_CODE, MSP.POS_SCLAS_NM \n' +
            'FROM (\n' +
            '\tSELECT ol.ORDER_STTUS_CODE\n' +
            '\t\t, ol.MRHST_SEQ\n' +
            '\t\t, ol.SLE_HIST_SEQ\n' +
            '\t\t, ol.ORDER_NO\n' +
            '\t\t, SUM( os.SLE_QY * ifnull(ms.SLE_AMOUNT, cs.SLE_AMOUNT)) ORDER_AMOUNT\n' +
            '\t\t, os.SAME_ORDER_GOODS_SEQ \n' +
            '\tFROM tanalysis_order_large_01 AS ol\n' +
            '\t\tJOIN tanalysis_order_small_history AS os ON os.SLE_HIST_SEQ = ol.SLE_HIST_SEQ \n' +
            '\t\tLEFT JOIN TBRANCH_MENU_SMALL_PRICE AS ms ON ms.MRHST_SEQ = os.MRHST_SEQ AND ms.SCLAS_CODE = os.SCLAS_CODE AND ms.SLE_UNIT = os.SLE_UNIT\n' +
            '\t\tLEFT JOIN TBRANCH_CHOISE_SMALL_PRICE AS cs ON cs.MRHST_SEQ = os.MRHST_SEQ AND cs.SCLAS_CODE = os.SCLAS_CODE AND cs.SLE_UNIT = os.SLE_UNIT\n' +
            '\tWHERE ol.MRHST_SEQ = :MRHST_SEQ\n' +
            '\t\tAND  \tNOT EXISTS(SELECT SLE_HIST_SEQ FROM tanalysis_sle_hist_seq_state sh \n' +
            '\tWHERE ol.MRHST_SEQ = :MRHST_SEQ \n' +
            '\t\tAND ol.SLE_HIST_SEQ = sh.SLE_HIST_SEQ)\n' +
            '\t\tAND   ol.CSTMR_TEL_NO = HEX(AES_ENCRYPT(:USER_MDN, "$DlShQpDlTus&)(*&^%$#&WnAnsDlFur") )\n' +
            '\t\tAND   ol.MRHST_BSN_DE > date_format(DATE_SUB(NOW(), INTERVAL 6 MONTH), "%Y%m%d") \n' +
            '\t\tAND   ol.ORDER_STTUS_CODE <> "100006" \n' +
            '\tGROUP by ol.MRHST_SEQ, ol.SLE_HIST_SEQ, ol.ORDER_NO\n' +
            '\tORDER BY ol.SLE_HIST_SEQ DESC\n' +
            '\tLIMIT 3    \n' +
            ') OL\n' +
            'JOIN tanalysis_order_small_history AS OSH ON OSH.SLE_HIST_SEQ = OL.SLE_HIST_SEQ\n' +
            'LEFT JOIN TBRANCH_MENU_SMALL AS MS ON  MS.MRHST_SEQ = OSH.MRHST_SEQ AND MS.SCLAS_CODE = OSH.SCLAS_CODE  \t\t\t\n' +
            'LEFT JOIN TBRANCH_CHOISE_SMALL AS CS ON CS.MRHST_SEQ = OSH.MRHST_SEQ AND CS.SCLAS_CODE = OSH.SCLAS_CODE\n' +
            'LEFT JOIN TBRANCH_MENU_SMALL_PRICE AS MSP ON  MSP.MRHST_SEQ = OSH.MRHST_SEQ AND MSP.SCLAS_CODE = OSH.SCLAS_CODE AND MSP.SLE_UNIT = OSH.SLE_UNIT\n' +
            'LEFT JOIN TBRANCH_CHOISE_SMALL_PRICE AS CSP ON CSP.MRHST_SEQ = OSH.MRHST_SEQ AND CSP.SCLAS_CODE = OSH.SCLAS_CODE AND CSP.SLE_UNIT = OSH.SLE_UNIT\n' +
            'WHERE OSH.MENU_GOODS_AT <> "D" AND OSH.SLE_HIST_SEQ = :SLE_HIST_SEQ -- AND MSP.USE_AT = "Y" \n' +
            'ORDER BY ol.SLE_HIST_SEQ DESC, OSH.SLE_SORT;'

        let pSelSmall = {MRHST_SEQ: MRHST_SEQ, USER_MDN: userMdn, SLE_HIST_SEQ: SLE_HIST_SEQ}

        sync.parallel(function () {
            ssoDB.excuteQuerySelect(qSelSmall, pSelSmall, true, sync.defers())
        })
        let retSmall = sync.await()
        let retSmall_err = [].concat.apply([], retSmall)[0]
        let retSmall_ret = [].concat.apply([], retSmall)[1]
        if (retSmall_err) {
            logger.error('selQuery - procPayCard_SkMns_HISTORY - insertLargeData - csid: ' + csid + ' - err: ' + retSmall)
            doPayCardRes_HISTORY('E', '', csid, CNAME, oData)
            return
        }

        //////////////////////////////////////////////////
        // insert order small data....
        // sorting small menu
        retSmall_ret = retSmall_ret.sort(doSort_SAME_ORDER_GOODS_SEQ)

        sync.parallel(function () {
            sortSmallMenu(retSmall_ret, sync.defers())
        })
        let sortedArr = sync.await()
        sortedArr = [].concat.apply([], sortedArr)[0]

        ///////////////////////////////////////////////////////////
        // SkMns - Pay data를 위해 ORDER_SMALL을 넣는다...
        oData.params[0].ORDER_SMALL = sortedArr

        // make small menu
        sync.parallel(function () {
            makeQuerySmallOrder(insertID, sortedArr, sync.defers())
        })
        let smallOrderInsertQuery = sync.await()
        let dlvrSAME_ORDER_GOODS_SEQ = [].concat.apply([], smallOrderInsertQuery)[0]
        let orderSmallInsertQuery = [].concat.apply([], smallOrderInsertQuery)[1]
        logger.info('selQuery - procPayCard_SkMns_HISTORY - orderSmall - query: ' + orderSmallInsertQuery)

        // insert small menu...
        sync.parallel(function () {
            ssoDB.excuteQueryInsert(orderSmallInsertQuery, '', true, sync.defers())
        })
        let retSmallOrder = sync.await()
        let retSmallOrder_err = [].concat.apply([], retSmallOrder)[0]
        let retSmallOrder_ret = [].concat.apply([], retSmallOrder)[1]
        if (retSmallOrder_err) {
            logger.error('selQuery - procPayCard_SkMns_HISTORY - insert order small - insertID: ' + insertID + ' - err: ' + retSmallOrder_err)
            doPayCardRes_HISTORY('E', '', csid, CNAME, oData)
            return
        }

        logger.info('selQuery - procPayCard_SkMns_HISTORY - insert order small -  orderLarge insertID: ' + insertID + ' - retSmallOrder_ret: ' + JSON.stringify(retSmallOrder_ret))


        //////////////////////////////////////////////////
        // update order large URL param...
        let reORDER_URL = CONSTS.CONN_SETTING.OVS_ORDER_CHK_URL + '/' + insertID
        let qUrl = "UPDATE THISTORY_ORDER_LARGE SET ORDER_URL = :ORDER_URL WHERE SLE_HIST_SEQ = :SLE_HIST_SEQ"
        let pUrl = {
            ORDER_URL: reORDER_URL,
            SLE_HIST_SEQ: insertID
        }
        logger.info('selQuery - procPayCard_SkMns_HISTORY - UPDATE THISTORY_ORDER_LARGE - ORDER_URL: ' + reORDER_URL + ' , insertID: ' + insertID)
        sync.parallel(function () {
            ssoDB.excuteQueryUpDel(qUrl, pUrl, true, sync.defers())
        })
        let retUpUrl = sync.await()
        let retUpUrl_err = [].concat.apply([], retUpUrl)[0]
        //let retUpUrl_ret = [].concat.apply([], retUpUrl)[1]
        if (retUpUrl_err) {
            logger.error('selQuery - procPayCard_SkMns_HISTORY - update order URL insertID - insertID: ' + insertID + ' - err: ' + retUpUrl_err)
            doPayCardRes_HISTORY('E', '', csid, CNAME, oData)
            return
        }


        ////////////////////////////////////
        // check address...
        logger.debug('selQuery - procPayCard_SkMns_HISTORY - ADDR_TYPE: ' + ADDR_TYPE + ' - MRHST_SEQ: ' + MRHST_SEQ + ' , CSTMR_TEL_NO: ' + CSTMR_TEL_NO + ' , DLVR_AREA: ' + DLVR_AREA + ' , DLVR_DETAIL_AREA: ' + DLVR_DETAIL_AREA)

        if (ADDR_TYPE === 'I')  // insert address info...
        {
            sync.parallel(function () {
                insertAddress_TTINFO_CUSTOMER(MRHST_SEQ, CSTMR_TEL_NO, DLVR_AREA, DLVR_DETAIL_AREA, MEMO, SI, GU, DONG, LI, BUNJI, ROAD_NM, BULD_NM, CRDNT_X, CRDNT_Y, BFE_X, BFE_Y, ADRES_TYPE, sync.defers())
                //insertAddress_TTINFO_CUSTOMER(MRHST_SEQ, CSTMR_TEL_NO, DLVR_AREA, DLVR_DETAIL_AREA, MEMO, SI, GU, DONG, LI, BUNJI, ROAD_NM, BULD_NM, CRDNT_X, CRDNT_Y, ADRES_TYPE, sync.defers())
            })
            let retAddrIn = sync.await()
            let retAddrIn_err = [].concat.apply([], retAddrIn)[0]
            let retAddrIn_ret = [].concat.apply([], retAddrIn)[1]
            if (retAddrIn_err) {
                logger.error('selQuery - procPayCard_SkMns_HISTORY - insertCustomerAddressInfo - MRHST_SEQ: ' + MRHST_SEQ + ' - err: ' + retAddrIn_err, CSTMR_TEL_NO)
            }
            else {
                if(retAddrIn_ret === 'DUP'){
                    sync.parallel(function () {
                        updateAddress_TTINFO_CUSTOMER(MRHST_SEQ, CSTMR_TEL_NO, DLVR_AREA, DLVR_DETAIL_AREA, MEMO, SI, GU, DONG, LI, BUNJI, ROAD_NM, BULD_NM, CRDNT_X, CRDNT_Y, BFE_X, BFE_Y, ADRES_TYPE, sync.defers())
                        //updateAddress_TTINFO_CUSTOMER(MRHST_SEQ, CSTMR_TEL_NO, DLVR_AREA, DLVR_DETAIL_AREA, MEMO, SI, GU, DONG, LI, BUNJI, ROAD_NM, BULD_NM, CRDNT_X, CRDNT_Y, ADRES_TYPE, sync.defers())
                    })
                    let retAddrInUp = sync.await()
                    let retAddrInUp_err = [].concat.apply([], retAddrInUp)[0]
                    //let retAddrInUp_ret = [].concat.apply([], retAddrInUp)[1]
                    if (retAddrInUp_err) {
                        logger.error('selQuery - procPayCard_SkMns_HISTORY - insertCustomerAddressInfo - DUP - updateTINFO - MRHST_SEQ: ' + MRHST_SEQ + ' - err: ' + retAddrInUp_err, CSTMR_TEL_NO)
                    }
                    else {
                        logger.info('selQuery - procPayCard_SkMns_HISTORY - insertCustomerAddressInfo - DUP - updateTINFO - MRHST_SEQ: ' + MRHST_SEQ, CSTMR_TEL_NO)
                    }
                }
                else {
                    logger.info('selQuery - procPayCard_SkMns_HISTORY - insertCustomerAddressInfo - MRHST_SEQ: ' + MRHST_SEQ, CSTMR_TEL_NO)
                }
            }
        }
        else if (ADDR_TYPE === 'U')  // update address info...
        {
            sync.parallel(function () {
                updateAddress_TTINFO_CUSTOMER(MRHST_SEQ, CSTMR_TEL_NO, DLVR_AREA, DLVR_DETAIL_AREA, MEMO, SI, GU, DONG, LI, BUNJI, ROAD_NM, BULD_NM, CRDNT_X, CRDNT_Y, BFE_X, BFE_Y, ADRES_TYPE, sync.defers())
                //updateAddress_TTINFO_CUSTOMER(MRHST_SEQ, CSTMR_TEL_NO, DLVR_AREA, DLVR_DETAIL_AREA, MEMO, SI, GU, DONG, LI, BUNJI, ROAD_NM, BULD_NM, CRDNT_X, CRDNT_Y, ADRES_TYPE, sync.defers())
            })
            let retAddrUp = sync.await()
            let retAddrUp_err = [].concat.apply([], retAddrUp)[0]
            //let retAddrUp_ret = [].concat.apply([], retAddrUp)[1]
            if (retAddrUp_err) {
                logger.error('selQuery - procPayCard_SkMns_HISTORY - updateCustomerAddressInfo - MRHST_SEQ: ' + MRHST_SEQ + ' - err: ' + retAddrUp_err, CSTMR_TEL_NO)
            }
            else {
                logger.info('selQuery - procPayCard_SkMns_HISTORY - updateCustomerAddressInfo - MRHST_SEQ: ' + MRHST_SEQ, CSTMR_TEL_NO)
            }
        }
        else if (ADDR_TYPE === 'P')  // update address info... shop pick up...
        {
            logger.info('selQuery - procPayCard_SkMns_HISTORY - ADDR_TYPE: P - No action customer address info...')
        }  // end of (ADDR_TYPE === 'P')

        else if (ADDR_TYPE === 'N') {  // No action...
            logger.info('selQuery - procPayCard_SkMns_HISTORY - ADDR_TYPE: N - No action customer address info...')
        }


        /////////////////////////////////////////
        // select order cancel time...
        sync.parallel(function () {
            selectOrderCancelTime(MRHST_SEQ, sync.defers())
        })
        let retCncl = sync.await()
        let retCncl_err = [].concat.apply([], retCncl)[0]
        let retCncl_ret = [].concat.apply([], retCncl)[1]
        let retCancelTime = ''
        if (retCncl_err) {
            logger.error('selQuery - procPayCard_SkMns_HISTORY - order cancel time - err: ' + retCncl_err)
        }
        else {
            retCancelTime = retCncl_ret
        }
        logger.info('selQuery - procPayCard_SkMns_HISTORY - end order - MRHST_SEQ: ' + MRHST_SEQ + ' , insertID: ' + insertID + ' , retCancelTime: ' + retCancelTime, CSTMR_TEL_NO)


        ////////////////////////////
        // 배달료 insert...
        if (retDlvr_ret && retDlvr_ret.length > 0) {
            if (retDlvr_ret[0].DLVR_AMOUNT_INCLS_AT === 'A' && arrLargeParams.ADDR_TYPE !== 'P') {
                sync.parallel(function () {
                    insertDeliveryAmount(insertID, dlvrSAME_ORDER_GOODS_SEQ, retDlvr_ret, sync.defers())
                })
                let retDlvrAmnt = sync.await()
                let retDlvrAmnt_err = [].concat.apply([], retDlvrAmnt)[0]
                //let retDlvrAmnt_ret = [].concat.apply([], retDlvrAmnt)[1]
                if (retDlvrAmnt_err) {
                    logger.error('selQuery - procPayCard_SkMns_HISTORY - insert delevery amount - insertID: ' + insertID + ' - err: ' + retDlvrAmnt_err)
                }
                doPayCardRes_HISTORY('S', insertID, csid, CNAME, oData, retCancelTime, sleAmount)
            }
            else {
                doPayCardRes_HISTORY('S', insertID, csid, CNAME, oData, retCancelTime, sleAmount)
            }
        }
        else{
            doPayCardRes_HISTORY('S', insertID, csid, CNAME, oData, retCancelTime, sleAmount)
        }
    })  // end of sync.fiber()
}  // end of procFinalOrderHistory()
function doPayCardRes_HISTORY(resultCode, insertID, sessionId, CNAME, oData, retCancelTime, dlvrAmount) {
    redis.hSetRedis(sessionId, 'ORDER_CANCEL_TIME', retCancelTime, (err, res) => {
        if(err){
            logger.error('selQuery - doPayCardRes_HISTORY - set Redis - sessionId:' + sessionId + ' - retCancelTime: ' + retCancelTime + ' - err: ' + err)
        }

        let sendData = {RESULT: resultCode, INSERT_ID: insertID, orderData: oData, dlvrAmount: dlvrAmount}
        let pubData = {data: sendData, workType: 'WT_PAY_CARD_SKMNS_HISTORY', sessionId: sessionId, csid: sessionId, retCancelTime: retCancelTime}
        logger.debug('selQuery - doPayCardRes_HISTORY - PUB: ' + 'RES_PC_' + CNAME)
        redis.pubRedis('RES_PC_' + CNAME, pubData)
    })
}

//0825
//////////////////////////////////////
// SETMENU 메뉴 인서트... PAY CARD
// SETMENU - FINAL ORDER Query.....
exports.procPayCard_SkMns_SETMENU = (oData) => {
    /*
     { workType: 'WT_ORDER_SETMENU',
       option: 'SNT306_SETMENU',
       params:
        [ { ORDER_LARGE: [Object] }
          , {SEL_MENU_DATA: setMenuData}
          { MRHST_SEQ: 5, POS_CODE: '100001', SHOP_NUMBER: '07052013279' } ],
       csid: 'test',
       isRes: true,
       CNAME: 'y1mvm' }
      */
    logger.info('selQuery - procPayCard_SkMns_SETMENU - oData: ' + JSON.stringify(oData))

    let CNAME = oData.CNAME
    let csid = oData.csid
    let workType = oData.workType
    let oBase = oData.params[2]

    let MRHST_SEQ = oBase.MRHST_SEQ
    let posCode = oBase.POS_CODE
    let shopNumber = oBase.SHOP_NUMBER

    let selMenuData = oData.params[1].SEL_MENU_DATA

    let arrLargeParams = oData.params[0].ORDER_LARGE
    let oBFE_X = arrLargeParams.BFE_X
    let oBFE_Y = arrLargeParams.BFE_Y
    if(!oBFE_X){
        arrLargeParams.BFE_X = null
    }
    if(!oBFE_Y){
        arrLargeParams.BFE_Y = null
    }

    let ADDR_TYPE = arrLargeParams.ADDR_TYPE


    let CSTMR_TEL_NO = arrLargeParams.CSTMR_TEL_NO
    let DLVR_AREA = arrLargeParams.DLVR_AREA
    let DLVR_DETAIL_AREA = arrLargeParams.DLVR_DETAIL_AREA
    let MEMO = arrLargeParams.MEMO
    let SI = arrLargeParams.SI
    let GU = arrLargeParams.GU
    let DONG = arrLargeParams.DONG
    let LI = arrLargeParams.LI
    let BUNJI = arrLargeParams.BUNJI
    let ROAD_NM = arrLargeParams.ROAD_NM
    let BULD_NM = arrLargeParams.BULD_NM
    let CRDNT_X = arrLargeParams.CRDNT_X
    let CRDNT_Y = arrLargeParams.CRDNT_Y
    let BFE_X = arrLargeParams.BFE_X
    let BFE_Y = arrLargeParams.BFE_Y
    let ADRES_TYPE = arrLargeParams.ADRES_TYPE

    let PCKAGE_SLE_AMOUNT = selMenuData.PCKAGE_SLE_AMOUNT
    let PCKAGE_SCLAS_CODE_NM = selMenuData.PCKAGE_SCLAS_CODE_NM
    let PCKAGE_SCLAS_CODE = selMenuData.PCKAGE_SCLAS_CODE
    let MRHST_RANK = selMenuData.MRHST_RANK
    let ORDER_SAME_ORDER_COUNT = selMenuData.ORDER_SAME_ORDER_COUNT
    let PCKAGE_SLE_UNIT = selMenuData.PCKAGE_SLE_UNIT


    //let arrSmallParams = oMenus.ORDER_SEL_SETMENU

    let qOrderLarge =
        'INSERT INTO THISTORY_ORDER_LARGE (ORDER_NO' +
        ', MRHST_SEQ' +
        ', CSTMR_TEL_NO' +
        ', RECPTN_TEL_NO' +
        ', DLVR_AREA' +
        ', DLVR_DETAIL_AREA' +
        ', MEMO' +
        ', SI' +
        ', GU' +
        ', DONG' +
        ', LI' +
        ', BUNJI' +
        ', ROAD_NM' +
        ', BULD_NM' +
        ', CRDNT_X' +
        ', CRDNT_Y' +
        ', BFE_X' +
        ', BFE_Y' +
        ', ADRES_TYPE' +
        ', ORDER_DE' +
        ', ORDER_HMS' +
        ', PRE_PYMNT_SE_CODE' +
        ', PYMNT_TY' +
        ', ORDER_AMOUNT' +
        ', DLVR_AMOUNT' +
        ', ORDER_STTUS_CODE' +
        ', ORDER_STTUS_DE_HMS' +
        ', DLVR_EXPECT_TIME' +
        ', CANCL_RESN_CODE' +
        ', CANCL_OPETR_CODE' +
        ', ORDER_URL' +
        ', CSID' +
        ', MRHST_BSN_DE' +
        ', RESVE_GNRL_SE' +
        ', ORDER_SE' +
        ', DLVR_SE) \n' +

        'VALUES ((select IFNULL(max(SLE_HIST_SEQ), 0)+1000000001 from (select sle_hist_seq from thistory_order_large) tmp)' +
        ', :MRHST_SEQ' +
        ', HEX(AES_ENCRYPT(:CSTMR_TEL_NO,"$DlShQpDlTus&)(*&^%$#&WnAnsDlFur"))' +
        ', HEX(AES_ENCRYPT(:RECPTN_TEL_NO,"$DlShQpDlTus&)(*&^%$#&WnAnsDlFur"))' +
        ', :DLVR_AREA' +
        ', :DLVR_DETAIL_AREA' +
        ', :MEMO' +
        ', :SI' +
        ', :GU' +
        ', :DONG' +
        ', :LI' +
        ', :BUNJI' +
        ', :ROAD_NM' +
        ', :BULD_NM' +
        ', :CRDNT_X' +
        ', :CRDNT_Y' +
        ', :BFE_X' +
        ', :BFE_Y' +
        ', :ADRES_TYPE' +
        ', REPLACE(substring(curdate(),1,10),"-","")' +
        ', REPLACE(curtime(),":","")' +
        ', :PRE_PYMNT_SE_CODE' +
        ', :PYMNT_TY' +
        ', :ORDER_AMOUNT' +
        ', :DLVR_AMOUNT' +
        ', :ORDER_STTUS_CODE' +
        ', concat(REPLACE(substring(curdate(),1,10),"-",""),REPLACE(curtime(),":",""))' +
        ', :DLVR_EXPECT_TIME' +
        ', :CANCL_RESN_CODE' +
        ', :CANCL_OPETR_CODE' +
        ', :ORDER_URL' +
        ', :CSID' +
        ', get_branch_bsn_de(:MRHST_SEQ)' +
        ', :RESVE_GNRL_SE' +
        ', :ORDER_SE' +
        ', :DLVR_SE) '


    sync.fiber(function () {
        /////////////////////////////////////////
        // 배달료 상품 정보 조회...
        let dlvrQuery = 'SELECT dl.LCLAS_CODE, dl.LCLAS_NM, ds.SCLAS_CODE, ds.SCLAS_NM, ds.SCLAS_TYPE, dsp.SLE_UNIT '
            + ' , dsp.SLE_AMOUNT, 901 + ifnull(dsp.SLE_UNIT_DC_SORT,0) AS SLE_SORT, BSS.DLVR_AMOUNT_INCLS_AT '
            + ' , IFNULL(dsp.SCLAS_SLE_UNIT, "기본배달료") AS SCLAS_SLE_UNIT '
            + ' , dsp.POS_SCLAS_CODE, dsp.POS_SCLAS_NM '
            + ' FROM tbranch_delivery_large dl '
            + ' JOIN tbranch_delivery_small ds ON dl.MRHST_SEQ = ds.MRHST_SEQ AND dl.LCLAS_CODE = ds.LCLAS_CODE '
            + ' JOIN tbranch_delivery_small_price dsp ON ds.MRHST_SEQ = dsp.MRHST_SEQ AND ds.SCLAS_CODE = dsp.SCLAS_CODE '
            + ' LEFT JOIN tinfo_branch_store_state AS BSS ON BSS.MRHST_SEQ = dl.MRHST_SEQ '
            + ' WHERE ds.MRHST_SEQ = :MRHST_SEQ '
            + ' AND ds.SCLAS_CODE = 9000000001 '
            + ' AND ds.SCLAS_TYPE = "100004" '
            + ' AND dsp.USE_AT = "Y" '

        sync.parallel(function () {
            ssoDB.excuteQuerySelect(dlvrQuery, {MRHST_SEQ: MRHST_SEQ}, true, sync.defers())
        })
        let retDlvr = sync.await()
        let retDlvr_err = [].concat.apply([], retDlvr)[0]
        let retDlvr_ret = [].concat.apply([], retDlvr)[1]
        if (retDlvr_err) {
            logger.error('selQuery - procPayCard_SkMns_SETMENU - dlvrQuery - csid: ' + csid + ' - err: ' + retDlvr_err)
            doPayCardRes_SETMENU('E', '', csid, CNAME, oData)
            return
        }


        arrLargeParams.DLVR_AMOUNT = 0

        let sleAmount = 0

        if (retDlvr_ret && retDlvr_ret.length > 0) {
            let sInclsAt = retDlvr_ret[0].DLVR_AMOUNT_INCLS_AT || ''
            if (sInclsAt === 'A' && ADDR_TYPE !== 'P') {
                let oDlvrRes = retDlvr_ret[0]
                let iSleAmount = oDlvrRes.SLE_AMOUNT || 0
                sleAmount = parseInt(iSleAmount)
                arrLargeParams.DLVR_AMOUNT = sleAmount
            }
        }


        ////////////////////////////////////////////
        // insert order large data...
        sync.parallel(function () {
            ssoDB.excuteQueryInsert(qOrderLarge, arrLargeParams, true, sync.defers())
        })
        let retLarge = sync.await()
        let retLarge_err = [].concat.apply([], retLarge)[0]
        let retLarge_ret = [].concat.apply([], retLarge)[1]
        if (retLarge_err) {
            logger.error('selQuery - procPayCard_SkMns_SETMENU - insertLargeData - csid: ' + csid + ' - err: ' + retLarge_err)
            doPayCardRes_SETMENU('E', '', csid, CNAME, oData)
            return
        }

        let insertID = retLarge_ret.insertId
        logger.info('selQuery - procPayCard_SkMns_SETMENU - insertOrderLarge - insertID :   ' + insertID + ' - aff: ' + JSON.stringify(retLarge_ret))

        let qSelSmall = 'select  \n' +
            '\tt1.MRHST_SEQ\n' +
            '\t, t1.PCKAGE_SCLAS_CODE \n' +
            '\t, t2.LCLAS_CODE \n' +
            '\t, get_lclas_name(t1.MRHST_SEQ, t2.LCLAS_CODE) AS LCLAS_NM \n' +
            '\t, t2.SCLAS_CODE \n' +
            '\t, ifnull(ms.SCLAS_NM, cs.SCLAS_NM) AS SCLAS_NM \n' +
            '\t, ifnull(msp.SCLAS_SLE_UNIT, csp.SCLAS_SLE_UNIT) AS SCLAS_SLE_UNIT \n' +
            '\t, t2.SCLAS_TYPE \n' +
            '\t, t2.MENU_GOODS_AT \n' +
            '\t, t2.SLE_UNIT \n' +
            '\t, ifnull(msp.SLE_AMOUNT, csp.SLE_AMOUNT) AS SLE_AMOUNT \n' +
            '\t, t2.SLE_SORT \n' +
            '\t, 1 AS SLE_QY \n' +
            '\t, 1 AS SAME_ORDER_GOODS_SEQ \n' +
            'from tchg_analysis_order_package_rank_01 AS t1 \n' +
            'join tchg_analysis_order_package_goodscode_01 AS t2 on t1.MRHST_SEQ = t2.MRHST_SEQ  and  t1.PCKAGE_SCLAS_CODE = t2.PCKAGE_SCLAS_CODE and t1.PCKAGE_SLE_UNIT = t2.PCKAGE_SLE_UNIT\n' +
            'left join  tbranch_menu_small AS ms on t2.MRHST_SEQ = ms.MRHST_SEQ  and t2.SCLAS_CODE = ms.SCLAS_CODE\n' +
            'left join  tbranch_menu_small_price AS msp on msp.MRHST_SEQ = t2.MRHST_SEQ and msp.SCLAS_CODE = t2.SCLAS_CODE and msp.SLE_UNIT = t2.SLE_UNIT\n' +
            'left join  tbranch_choise_small AS cs on t2.MRHST_SEQ = cs.MRHST_SEQ and t2.SCLAS_CODE = cs.SCLAS_CODE\n' +
            'left join  tbranch_choise_small_price AS csp on csp.MRHST_SEQ = t2.MRHST_SEQ and csp.SCLAS_CODE = t2.SCLAS_CODE and csp.SLE_UNIT = t2.SLE_UNIT    \n' +
            'where t1.SCLAS_TYPE = "100001" \n' +
            'and t1.USE_AT = "Y" \n' +
            'and t1.MRHST_SEQ = :MRHST_SEQ \n' +
            'and t1.PCKAGE_SCLAS_CODE = :PCKAGE_SCLAS_CODE \n' +
            'and t1.PCKAGE_SLE_UNIT = :PCKAGE_SLE_UNIT \n' +
            'order by t2.SLE_SORT;'

        let pSelSmall = {MRHST_SEQ: MRHST_SEQ, PCKAGE_SCLAS_CODE: PCKAGE_SCLAS_CODE, PCKAGE_SLE_UNIT: PCKAGE_SLE_UNIT}

        sync.parallel(function () {
            ssoDB.excuteQuerySelect(qSelSmall, pSelSmall, true, sync.defers())
        })
        let retSmall = sync.await()
        let retSmall_err = [].concat.apply([], retSmall)[0]
        let retSmall_ret = [].concat.apply([], retSmall)[1]
        if (retSmall_err) {
            logger.error('selQuery - procPayCard_SkMns_SETMENU - insertLargeData - csid: ' + csid + ' - err: ' + retSmall)
            doPayCardRes_SETMENU('E', '', csid, CNAME, oData)
            return
        }


        //////////////////////////////////////////////////
        // insert order small data....

        // sorting small menu
        retSmall_ret = retSmall_ret.sort(doSort_SAME_ORDER_GOODS_SEQ)
        sync.parallel(function () {
            sortSmallMenu(retSmall_ret, sync.defers())
        })
        let sortedArr = sync.await()
        sortedArr = [].concat.apply([], sortedArr)[0]

        ///////////////////////////////////////////////////////////
        // SkMns - Pay data를 위해 ORDER_SMALL을 넣는다...
        oData.params[0].ORDER_SMALL = sortedArr

        // make small menu
        sync.parallel(function () {
            makeQuerySmallOrder(insertID, sortedArr, sync.defers())
        })
        let smallOrderInsertQuery = sync.await()
        let dlvrSAME_ORDER_GOODS_SEQ = [].concat.apply([], smallOrderInsertQuery)[0]
        let orderSmallInsertQuery = [].concat.apply([], smallOrderInsertQuery)[1]
        logger.info('selQuery - procPayCard_SkMns_SETMENU - orderSmall - query: ' + orderSmallInsertQuery)

        // insert small menu...
        sync.parallel(function () {
            ssoDB.excuteQueryInsert(orderSmallInsertQuery, '', true, sync.defers())
        })
        let retSmallOrder = sync.await()
        let retSmallOrder_err = [].concat.apply([], retSmallOrder)[0]
        let retSmallOrder_ret = [].concat.apply([], retSmallOrder)[1]
        if (retSmallOrder_err) {
            logger.error('selQuery - procPayCard_SkMns_SETMENU - insert order small - insertID: ' + insertID + ' - err: ' + retSmallOrder_err)
            doPayCardRes_SETMENU('E', '', csid, CNAME, oData)
            return
        }

        logger.info('selQuery - procPayCard_SkMns_SETMENU - insert order small -  orderLarge insertID: ' + insertID + ' - retSmallOrder_ret: ' + JSON.stringify(retSmallOrder_ret))


        //////////////////////////////////////////////////
        // update order large URL param...
        let reORDER_URL = CONSTS.CONN_SETTING.OVS_ORDER_CHK_URL + '/' + insertID
        let qUrl = "UPDATE THISTORY_ORDER_LARGE SET ORDER_URL = :ORDER_URL WHERE SLE_HIST_SEQ = :SLE_HIST_SEQ"
        let pUrl = {
            ORDER_URL: reORDER_URL,
            SLE_HIST_SEQ: insertID
        }
        logger.info('selQuery - procPayCard_SkMns_SETMENU - UPDATE THISTORY_ORDER_LARGE - ORDER_URL: ' + reORDER_URL + ' , insertID: ' + insertID)
        sync.parallel(function () {
            ssoDB.excuteQueryUpDel(qUrl, pUrl, true, sync.defers())
        })
        let retUpUrl = sync.await()
        let retUpUrl_err = [].concat.apply([], retUpUrl)[0]
        //let retUpUrl_ret = [].concat.apply([], retUpUrl)[1]
        if (retUpUrl_err) {
            logger.error('selQuery - procPayCard_SkMns_SETMENU - update order URL insertID - insertID: ' + insertID + ' - err: ' + retUpUrl_err)
            doPayCardRes_SETMENU('E', '', csid, CNAME, oData)
            return
        }


        ////////////////////////////////////
        // check address...
        logger.debug('selQuery - procPayCard_SkMns_SETMENU - ADDR_TYPE: ' + ADDR_TYPE + ' - MRHST_SEQ: ' + MRHST_SEQ + ' , CSTMR_TEL_NO: ' + CSTMR_TEL_NO + ' , DLVR_AREA: ' + DLVR_AREA + ' , DLVR_DETAIL_AREA: ' + DLVR_DETAIL_AREA)

        if (ADDR_TYPE === 'I')  // insert address info...
        {
            sync.parallel(function () {
                insertAddress_TTINFO_CUSTOMER(MRHST_SEQ, CSTMR_TEL_NO, DLVR_AREA, DLVR_DETAIL_AREA, MEMO, SI, GU, DONG, LI, BUNJI, ROAD_NM, BULD_NM, CRDNT_X, CRDNT_Y, BFE_X, BFE_Y, ADRES_TYPE, sync.defers())
                //insertAddress_TTINFO_CUSTOMER(MRHST_SEQ, CSTMR_TEL_NO, DLVR_AREA, DLVR_DETAIL_AREA, MEMO, SI, GU, DONG, LI, BUNJI, ROAD_NM, BULD_NM, CRDNT_X, CRDNT_Y, ADRES_TYPE, sync.defers())
            })
            let retAddrIn = sync.await()
            let retAddrIn_err = [].concat.apply([], retAddrIn)[0]
            let retAddrIn_ret = [].concat.apply([], retAddrIn)[1]
            if (retAddrIn_err) {
                logger.error('selQuery - procPayCard_SkMns_SETMENU - insertCustomerAddressInfo - MRHST_SEQ: ' + MRHST_SEQ + ' - err: ' + retAddrIn_err, CSTMR_TEL_NO)
            }
            // else {
            //     logger.info('selQuery - procPayCard_SkMns_SETMENU - insertCustomerAddressInfo - MRHST_SEQ: ' + MRHST_SEQ, CSTMR_TEL_NO)
            // }
            else {
                if(retAddrIn_ret === 'DUP'){
                    sync.parallel(function () {
                        updateAddress_TTINFO_CUSTOMER(MRHST_SEQ, CSTMR_TEL_NO, DLVR_AREA, DLVR_DETAIL_AREA, MEMO, SI, GU, DONG, LI, BUNJI, ROAD_NM, BULD_NM, CRDNT_X, CRDNT_Y, BFE_X, BFE_Y, ADRES_TYPE, sync.defers())
                        //updateAddress_TTINFO_CUSTOMER(MRHST_SEQ, CSTMR_TEL_NO, DLVR_AREA, DLVR_DETAIL_AREA, MEMO, SI, GU, DONG, LI, BUNJI, ROAD_NM, BULD_NM, CRDNT_X, CRDNT_Y, ADRES_TYPE, sync.defers())
                    })
                    let retAddrInUp = sync.await()
                    let retAddrInUp_err = [].concat.apply([], retAddrInUp)[0]
                    //let retAddrInUp_ret = [].concat.apply([], retAddrInUp)[1]
                    if (retAddrInUp_err) {
                        logger.error('selQuery - procPayCard_SkMns_SETMENU - insertCustomerAddressInfo - DUP - updateTINFO - MRHST_SEQ: ' + MRHST_SEQ + ' - err: ' + retAddrInUp_err, CSTMR_TEL_NO)
                    }
                    else {
                        logger.info('selQuery - procPayCard_SkMns_SETMENU - insertCustomerAddressInfo - DUP - updateTINFO - MRHST_SEQ: ' + MRHST_SEQ, CSTMR_TEL_NO)
                    }
                }
                else {
                    logger.info('selQuery - procPayCard_SkMns_SETMENU - insertCustomerAddressInfo - MRHST_SEQ: ' + MRHST_SEQ, CSTMR_TEL_NO)
                }
            }
        }
        else if (ADDR_TYPE === 'U')  // update address info...
        {
            sync.parallel(function () {
                updateAddress_TTINFO_CUSTOMER(MRHST_SEQ, CSTMR_TEL_NO, DLVR_AREA, DLVR_DETAIL_AREA, MEMO, SI, GU, DONG, LI, BUNJI, ROAD_NM, BULD_NM, CRDNT_X, CRDNT_Y, BFE_X, BFE_Y, ADRES_TYPE, sync.defers())
                //updateAddress_TTINFO_CUSTOMER(MRHST_SEQ, CSTMR_TEL_NO, DLVR_AREA, DLVR_DETAIL_AREA, MEMO, SI, GU, DONG, LI, BUNJI, ROAD_NM, BULD_NM, CRDNT_X, CRDNT_Y, ADRES_TYPE, sync.defers())
            })
            let retAddrUp = sync.await()
            let retAddrUp_err = [].concat.apply([], retAddrUp)[0]
            //let retAddrUp_ret = [].concat.apply([], retAddrUp)[1]
            if (retAddrUp_err) {
                logger.error('selQuery - procPayCard_SkMns_SETMENU - updateCustomerAddressInfo - MRHST_SEQ: ' + MRHST_SEQ + ' - err: ' + retAddrUp_err, CSTMR_TEL_NO)
            }
            else {
                logger.info('selQuery - procPayCard_SkMns_SETMENU - updateCustomerAddressInfo - MRHST_SEQ: ' + MRHST_SEQ, CSTMR_TEL_NO)
            }
        }
        else if (ADDR_TYPE === 'P')  // update address info... shop pick up...
        {
            logger.info('selQuery - procPayCard_SkMns_SETMENU - ADDR_TYPE: P - No action customer address info...')
        }  // end of (ADDR_TYPE === 'P')

        else if (ADDR_TYPE === 'N') {  // No action...
            logger.info('selQuery - procPayCard_SkMns_SETMENU - ADDR_TYPE: N - No action customer address info...')
        }


        /////////////////////////////////////////
        // select order cancel time...
        sync.parallel(function () {
            selectOrderCancelTime(MRHST_SEQ, sync.defers())
        })
        let retCncl = sync.await()
        let retCncl_err = [].concat.apply([], retCncl)[0]
        let retCncl_ret = [].concat.apply([], retCncl)[1]
        let retCancelTime = ''
        if (retCncl_err) {
            logger.error('selQuery - procPayCard_SkMns_SETMENU - order cancel time - err: ' + retCncl_err)
        }
        else {
            retCancelTime = retCncl_ret
        }

        logger.info('selQuery - procPayCard_SkMns_SETMENU - end order - MRHST_SEQ: ' + MRHST_SEQ + ' , insertID: ' + insertID + ' , retCancelTime: ' + retCancelTime, CSTMR_TEL_NO)


        ////////////////////////////
        // 배달료 insert...
        if (retDlvr_ret && retDlvr_ret.length > 0) {
            if (retDlvr_ret[0].DLVR_AMOUNT_INCLS_AT === 'A' && arrLargeParams.ADDR_TYPE !== 'P') {
                sync.parallel(function () {
                    insertDeliveryAmount(insertID, dlvrSAME_ORDER_GOODS_SEQ, retDlvr_ret, sync.defers())
                })
                let retDlvrAmnt = sync.await()
                let retDlvrAmnt_err = [].concat.apply([], retDlvrAmnt)[0]
                //let retDlvrAmnt_ret = [].concat.apply([], retDlvrAmnt)[1]
                if (retDlvrAmnt_err) {
                    logger.error('selQuery - procPayCard_SkMns_SETMENU - insert delevery amount - insertID: ' + insertID + ' - err: ' + retDlvrAmnt_err)
                }

                doPayCardRes_SETMENU('S', insertID, csid, CNAME, oData, retCancelTime, sleAmount)
            }
            else {
                doPayCardRes_SETMENU('S', insertID, csid, CNAME, oData, retCancelTime, sleAmount)
            }
        }
        else {
            doPayCardRes_SETMENU('S', insertID, csid, CNAME, oData, retCancelTime, sleAmount)
        }
    })  // end of sync.fiber()
}
function doPayCardRes_SETMENU(resultCode, insertID, sessionId, CNAME, oData, retCancelTime, dlvrAmount) {
    redis.hSetRedis(sessionId, 'ORDER_CANCEL_TIME', retCancelTime, (err, res) => {
        if(err){
            logger.error('selQuery - doPayCardRes_SETMENU - set Redis - sessionId:' + sessionId + ' - retCancelTime: ' + retCancelTime + ' - err: ' + err)
        }

        let sendData = {RESULT: resultCode, INSERT_ID: insertID, orderData: oData, dlvrAmount: dlvrAmount}
        let pubData = {data: sendData, workType: 'WT_PAY_CARD_SKMNS_SETMENU', sessionId: sessionId, csid: sessionId, retCancelTime: retCancelTime}
        logger.debug('selQuery - doPayCardRes_SETMENU - PUB: ' + 'RES_PC_' + CNAME)
        redis.pubRedis('RES_PC_' + CNAME, pubData)
    })
}









////////////////////////////////////////////////////
// 카드 선결제 - insert order data... SkMns
exports.procPayCardResponse_SkMns = (oData) => {
    console.log('procPayCardResponse_SkMns---------')
    console.dir(oData)
    console.log('procPayCardResponse_SkMns------------------------')
    /*
    { workType: 'WT_PAY_CARD_RESPONSE_SKMNS',
      option: '',
      params:
       { tradeId: 'resTradeId123'
         orderNumber: 'resOrderNumber123',
         totalAmount: 'resTotalAmount123',
         inicisPaymentNumber: 'resInicisPaymentNumber123',
         payMessage: 'resMessage123' },
      csid: 'sessionId123',
      isRes: true,
      CNAME: 'AXhr7' }
     */
    /*
    {"workType":"WT_PAY_CARD_RESPONSE_SKMNS"
        ,"option":""
        ,"params":{"tradeId":"1000023349TRDID"
                    ,"orderNumber":"1000023349"
                    ,"totalAmount":"12900"
                    ,"inicisPaymentNumber":"INIMX_ISP_INIpayTest20190827192335555661"
                    ,"payMessage":"결제 성공"}
        ,"csid":"1566901333.173069"
        ,"isRes":true
        ,"CNAME":"saYqK"}
     */

    logger.debug('selQuery - procPayCardResponse_SkMns - data: ' + JSON.stringify(oData))

    // {orderNumber: resOrderNumber
    //   , totalAmount: resTotalAmount
    //   , inicisPaymentNumber: resInicisPaymentNumber
    //   , payMessage: resMessage}
    let oParams = oData.params
    let sessionId = oData.csid
    let CNAME = oData.CNAME
    let tradeId = oParams.tradeId
    let skmnsOrderNo = oParams.orderNumber
    let inicisPaymentNumber = oParams.inicisPaymentNumber
    let payMessage = oParams.payMessage

    redis.hGetallRedis(tradeId, (err, retData) => {
        if(err){
            logger.error('selQuery - procPayCardResponse_SkMns - get Redis - sessionId: ' + sessionId + ', skmnsOrderNo: ' + skmnsOrderNo + ' - err: ' + err)
            return
        }

        // retData = {'MRHST_SEQ', 'POS_CODE', 'SHOP_NUMBER', 'ORDER_LARGE', 'ORDER_SMALL'}
        logger.debug('selQuery - procPayCardResponse_SkMns - get redis tradeId: ' + tradeId + ' - retData: ' + JSON.stringify(retData))

        let mrhstSeq = retData.MRHST_SEQ
        let shopNumber = retData.SHOP_NUMBER

        let rqesterSe = '100001'
        let requstTy = '100001'
        let insertID = tradeId.split('T')[0]
        let sleHistSeq = parseInt(insertID) - 1000000000

        let sendData = {
            sleHistSeq: sleHistSeq
            , requstTy: requstTy
            , rqesterSe: rqesterSe
            , mrhstSeq: mrhstSeq
            , shopNumber: shopNumber
        }

        console.log("@@@@ selQuery - 카드결제 후 POS - retData.POS_CODE: " + retData.POS_CODE)
        console.log("@@@@ selQuery - 카드결제 후 POS - retData.POS_CODE: " + retData.POS_CODE)
        console.log("@@@@ selQuery - 카드결제 후 POS - retData.POS_CODE: " + retData.POS_CODE)

        if(retData.POS_CODE === CONSTS_SYS.POS_TYPE.CNTT_BQ){
            posCNTT.sendDataCNTT('ORDER_SEND', sendData, (err, res) => {
                if (err) {
                    logger.error('selQuery - procPayCardResponse_SkMns - CNTT - sendDataCNTT - ORDER_SEND -- ' + err)

                    let retPubData = {RESULT: 'E'}
                    let pubData = {data: retPubData, workType: 'WT_PAY_CARD_RESPONSE_SKMNS', sessionId: sessionId, csid: sessionId}
                    redis.pubRedis('RES_PC_PAY_RES_' + CNAME, pubData)
                    return
                }

                logger.info('selQuery - procPayCardResponse_SkMns - sendDataCNTT - res: ' + res)

                let sendData = {RESULT: 'S'}
                let pubData = {data: sendData, workType: 'WT_PAY_CARD_RESPONSE_SKMNS', sessionId: sessionId, csid: sessionId}
                logger.debug('selQuery - doPayCardRes - PUB: ' + 'RES_PC_' + CNAME)
                redis.pubRedis('RES_PC_PAY_RES_' + CNAME, pubData)
            })
        }
        else{
            ovsShop.sendDataToOvsShop(sleHistSeq, requstTy, rqesterSe, mrhstSeq, (err, res) => {
                if (err) {
                    logger.error('selQuery - procPayCardResponse_SkMns - SSO - sendDataToOvsShop -- ' + err)

                    let retPubData = {RESULT: 'E'}
                    let pubData = {data: retPubData, workType: 'WT_PAY_CARD_RESPONSE_SKMNS', sessionId: sessionId, csid: sessionId}
                    redis.pubRedis('RES_PC_PAY_RES_' + CNAME, pubData)
                    return
                }

                logger.info('selQuery - procPayCardResponse_SkMns - sendDataToOvsShop - res: ' + res)

                let sendData = {RESULT: 'S'}
                let pubData = {data: sendData, workType: 'WT_PAY_CARD_RESPONSE_SKMNS', sessionId: sessionId, csid: sessionId}
                logger.debug('selQuery - doPayCardRes - PUB: ' + 'RES_PC_' + CNAME)
                redis.pubRedis('RES_PC_PAY_RES_' + CNAME, pubData)
            })
        }
    })
}  // end of procPayCardResponse_SkMns



////////////////////////////////////////////////////
// 카드 결제 취소 POS 취소 전달
exports.procPayCardCancelResponse_SkMns = (oData) => {
    console.log('procPayCardCancelResponse_SkMns---------')
    console.dir(oData)
    console.log('procPayCardCancelResponse_SkMns------------------------')
    /*
    { workType: 'WT_PAY_CARD_CANCEL_RESPONSE_SKMNS',
      option: '',
      params:
       { tradeId: 'resTradeId123'
         orderNumber: '1000012345',
         totalAmount: 'resTotalAmount123',
         inicisPaymentNumber: 'resInicisPaymentNumber123',
         sessionId: 'ssid123'
         mrhstSeq: 111
         posCode: 10001},
      csid: 'sessionId123',
      isRes: true,
      CNAME: 'AXhr7' }
     */

    logger.debug('selQuery - procPayCardCancelResponse_SkMns - data: ' + JSON.stringify(oData))

    let mrhstSeq = oData.params.mrhstSeq
    let orderNumber = parseInt(oData.params.orderNumber) - 1000000000
    let sessionId = oData.params.sessionId
    let csid = oData.csid
    let CNAME = oData.CNAME
    let tradeId = oData.params.tradeId

    let cData = {MRHST_SEQ: mrhstSeq, SLE_HIST_SEQ: orderNumber}

    redis.hGetallRedis(tradeId, (err, retData) => {
        if (err) {
            logger.error('selQuery - procPayCardCancelResponse_SkMns - get Redis - sessionId: ' + sessionId + ', orderNumber: ' + orderNumber + ' - err: ' + err)
            return
        }

        logger.debug('selQuery - procPayCardCancelResponse_SkMns - get redis tradeId: ' + tradeId + ' - retData: ' + JSON.stringify(retData))


        console.log("@@@@ selQuery - 카드결제 취소 POS - retData.POS_CODE: " + retData.POS_CODE)
        console.log("@@@@ selQuery - 카드결제 취소 POS - retData.POS_CODE: " + retData.POS_CODE)
        console.log("@@@@ selQuery - 카드결제 취소 POS - retData.POS_CODE: " + retData.POS_CODE)

        if(retData.POS_CODE === CONSTS_SYS.POS_TYPE.CNTT_BQ){
            posCNTT.sendDataCNTT('ORDER_CANCEL', cData, (err, cRes) => {
                if(err){
                    logger.error('selQuery - procPayCardCancelResponse_SkMns - MRHST_SEQ: ' + mrhstSeq + ' - SLE_HIST_SEQ: ' + orderNumber)
                    let retPubData = {RESULT: 'E'}
                    let pubData = {data: retPubData, workType: 'WT_PAY_CARD_CANCEL_RESPONSE_SKMNS', sessionId: sessionId, csid: sessionId}
                    redis.pubRedis('RES_PC_PAY_RES_' + CNAME, pubData)
                    return
                }

                logger.info('selQuery - procPayCardCancelResponse_SkMns - sendDataCNTT - res: ' + JSON.stringify(cRes))

                let sendData = {RESULT: 'S'}
                let pubData = {data: sendData, workType: 'WT_PAY_CARD_CANCEL_RESPONSE_SKMNS', sessionId: sessionId, csid: sessionId}
                logger.debug('selQuery - procPayCardCancelResponse_SkMns - PUB: ' + 'RES_PC_PAY_CANCEL_RES_' + CNAME)
                redis.pubRedis('RES_PC_PAY_CANCEL_RES_' + CNAME, pubData)
            })
        }
        else{
            let rqesterSe = '100001'
            let requstTy = '100006'
            let insertID = tradeId.split('T')[0]
            let sleHistSeq = parseInt(insertID) - 1000000000

            console.log('### selQuery - procPayCardCancelResponse_SkMns - sleHistSeq:' + sleHistSeq + ' - insertID: ' + insertID)

            ovsShop.sendDataToOvsShop(sleHistSeq, requstTy, rqesterSe, mrhstSeq, (err, cRes) => {
                if(err){
                    logger.error('selQuery - procPayCardCancelResponse_SkMns - MRHST_SEQ: ' + mrhstSeq + ' - SLE_HIST_SEQ: ' + orderNumber)
                    let retPubData = {RESULT: 'E'}
                    let pubData = {data: retPubData, workType: 'WT_PAY_CARD_CANCEL_RESPONSE_SKMNS', sessionId: sessionId, csid: sessionId}
                    redis.pubRedis('RES_PC_PAY_RES_' + CNAME, pubData)
                    return
                }

                logger.info('selQuery - procPayCardCancelResponse_SkMns - sendDataCNTT - res: ' + JSON.stringify(cRes))

                let sendData = {RESULT: 'S'}
                let pubData = {data: sendData, workType: 'WT_PAY_CARD_CANCEL_RESPONSE_SKMNS', sessionId: sessionId, csid: sessionId}
                logger.debug('selQuery - procPayCardCancelResponse_SkMns - PUB: ' + 'RES_PC_PAY_CANCEL_RES_' + CNAME)
                redis.pubRedis('RES_PC_PAY_CANCEL_RES_' + CNAME, pubData)
            })
        }
    })

}  // end of procPayCardCancelResponse_SkMns







function doFinalOrderToShop(mrhstSeq, posCode, shopNumber, insertID, csid, workType, retCancelTime, CNAME) {
    let rqesterSe = '100001'
    let requstTy = '100001'
    let sleHistSeq = parseInt(insertID)


    mrhstSeq = mrhstSeq.toString()
    posCode = posCode
    shopNumber = shopNumber.toString()


    logger.debug('selQuery - doFinalOrderToShop - mrhstSeq: ' + mrhstSeq + ' , posCode: ' + posCode
        + ' , shopNumber: ' + shopNumber + ' , insertID: ' + insertID + ' , csid: ' + csid
        + ' , workType: ' + workType + ' , retCancelTime: ' + retCancelTime + ' , CNAME: ' + CNAME)


    if (posCode === '100001' || '') { // SSO
        ovsShop.sendDataToOvsShop(sleHistSeq, requstTy, rqesterSe, mrhstSeq, (err, res) => {
            if (err) {
                logger.error('selQuery - doFinalOrderToShop - SSO - sendDataToOvsShop -- ' + err)

                let retPubData = {RESULT: 'ERROR'}
                let pubData = {data: retPubData, workType: workType, csid: csid}
                redis.pubRedis(CONSTS_SYS.PUB_CH.RES_ORDER + CNAME, pubData)
                return
            }

            logger.info('selQuery - doFinalOrderToShop - sendDataToOvsShop - res: ' + res)

            let retPubData = {RESULT: 'OK', INSERT_ID: insertID, ORDER_CANCEL_TIME: retCancelTime}
            let pubData = {data: retPubData, workType: workType, csid: csid}
            redis.pubRedis(CONSTS_SYS.PUB_CH.RES_ORDER + CNAME, pubData)
        })
    }
    else if (posCode === '200101') { // FUSE_PP:
        let sendData = {
            sleHistSeq: sleHistSeq
            , requstTy: requstTy
            , rqesterSe: rqesterSe
            , mrhstSeq: mrhstSeq
            , shopNumber: shopNumber
        }

        posFUSE.sendDataFUSE('ORDER_SEND', sendData, (err, res) => {
            if (err) {
                logger.error('selQuery - doFinalOrderToShop - FUSE - sendDataFUSE - ORDER_SEND -- ' + err)

                let retPubData = {RESULT: 'ERROR'}
                let pubData = {data: retPubData, workType: workType, csid: csid}
                redis.pubRedis(CONSTS_SYS.PUB_CH.RES_ORDER + CNAME, pubData)
                return
            }

            logger.info('selQuery - doFinalOrderToShop - sendDataFUSE - res: ' + res)

            let retPubData = {RESULT: 'OK', INSERT_ID: insertID, ORDER_CANCEL_TIME: retCancelTime}
            let pubData = {data: retPubData, workType: workType, csid: csid}
            redis.pubRedis(CONSTS_SYS.PUB_CH.RES_ORDER + CNAME, pubData)
        })
    }
    else if (posCode === '200201') { // CNTT_BQ
        let sendData = {
            sleHistSeq: sleHistSeq
            , requstTy: requstTy
            , rqesterSe: rqesterSe
            , mrhstSeq: mrhstSeq
            , shopNumber: shopNumber
        }

        posCNTT.sendDataCNTT('ORDER_SEND', sendData, (err, res) => {
            if (err) {
                logger.error('selQuery - doFinalOrderToShop - CNTT - sendDataCNTT - ORDER_SEND -- ' + err)

                let retPubData = {RESULT: 'ERROR'}
                let pubData = {data: retPubData, workType: workType, csid: csid}
                redis.pubRedis(CONSTS_SYS.PUB_CH.RES_ORDER + CNAME, pubData)
                return
            }

            logger.info('selQuery - doFinalOrderToShop - sendDataCNTT - res: ' + res)

            let retPubData = {RESULT: 'OK', INSERT_ID: insertID, ORDER_CANCEL_TIME: retCancelTime}
            let pubData = {data: retPubData, workType: workType, csid: csid}
            redis.pubRedis(CONSTS_SYS.PUB_CH.RES_ORDER + CNAME, pubData)
        })
    }
}


//0705
//////////////////////////////////////
// SET MENU - FINAL ORDER Query.....
exports.procFinalOrderSetMenu_new = (oData) => {
    /*
    { workType: 'WT_ORDER_SETMENU',
      option: 'SNT306_SETMENU',
      params:
       [ { ORDER_LARGE: [Object] }
         , {SEL_MENU_DATA: setMenuData}
         { MRHST_SEQ: 5, POS_CODE: '100001', SHOP_NUMBER: '07052013279' } ],
      csid: 'test',
      isRes: true,
      CNAME: 'y1mvm' }
     */
    logger.info('selQuery - procFinalOrderSetMenu_new - oData: ' + JSON.stringify(oData))

    let CNAME = oData.CNAME
    let csid = oData.csid
    let workType = oData.workType
    let oBase = oData.params[2]

    let MRHST_SEQ = oBase.MRHST_SEQ
    let posCode = oBase.POS_CODE
    let shopNumber = oBase.SHOP_NUMBER

    let selMenuData = oData.params[1].SEL_MENU_DATA

    let arrLargeParams = oData.params[0].ORDER_LARGE
    let oBFE_X = arrLargeParams.BFE_X
    let oBFE_Y = arrLargeParams.BFE_Y
    if(!oBFE_X){
        arrLargeParams.BFE_X = null
    }
    if(!oBFE_Y){
        arrLargeParams.BFE_Y = null
    }

    /* arrLargeParams =
    { MRHST_SEQ: 5,
      CSTMR_TEL_NO: '01089501450',
      RECPTN_TEL_NO: '',
      DLVR_AREA: '대구 달성군 화원읍 류목정길 9 (설화리)',
      DLVR_DETAIL_AREA: 'fd',
      MEMO: '',
      SI: '',
      GU: '',
      DONG: '',
      LI: '',
      BUNJI: '871',
      ROAD_NM: '',
      BULD_NM: '',
      CRDNT_X: 290573.9628461039,
      CRDNT_Y: -2541973.2521950137,
      ADRES_TYPE: '',
      PRE_PYMNT_SE_CODE: '100002',
      PYMNT_TY: '100003',
      ORDER_AMOUNT: 16000,
      ORDER_STTUS_CODE: '100001',
      DLVR_EXPECT_TIME: 0,
      CANCL_RESN_CODE: '',
      CANCL_OPETR_CODE: '',
      ORDER_URL: '',
      CSID: 'test',
      ADDR_TYPE: 'I',
      RESVE_GNRL_SE: '100001',
      ORDER_SE: '100002',
      DLVR_SE: '100001',
      POS_CODE: '100001' }
     */

    let ADDR_TYPE = arrLargeParams.ADDR_TYPE


    let CSTMR_TEL_NO = arrLargeParams.CSTMR_TEL_NO
    let DLVR_AREA = arrLargeParams.DLVR_AREA
    let DLVR_DETAIL_AREA = arrLargeParams.DLVR_DETAIL_AREA
    let MEMO = arrLargeParams.MEMO
    let SI = arrLargeParams.SI
    let GU = arrLargeParams.GU
    let DONG = arrLargeParams.DONG
    let LI = arrLargeParams.LI
    let BUNJI = arrLargeParams.BUNJI
    let ROAD_NM = arrLargeParams.ROAD_NM
    let BULD_NM = arrLargeParams.BULD_NM
    let CRDNT_X = arrLargeParams.CRDNT_X
    let CRDNT_Y = arrLargeParams.CRDNT_Y
    let BFE_X = arrLargeParams.BFE_X
    let BFE_Y = arrLargeParams.BFE_Y
    let ADRES_TYPE = arrLargeParams.ADRES_TYPE


    // setMenuData =
    //             {"PCKAGE_SLE_AMOUNT":"60000"
    //                ,"PCKAGE_SCLAS_CODE_NM":"순살 3종세트"
    //                ,"PCKAGE_SCLAS_CODE":"91"
    //                ,"MRHST_RANK":"1"
    //                ,"ORDER_SAME_ORDER_COUNT":"13"
    //                ,"PCKAGE_SLE_UNIT":""

    let PCKAGE_SLE_AMOUNT = selMenuData.PCKAGE_SLE_AMOUNT
    let PCKAGE_SCLAS_CODE_NM = selMenuData.PCKAGE_SCLAS_CODE_NM
    let PCKAGE_SCLAS_CODE = selMenuData.PCKAGE_SCLAS_CODE
    let MRHST_RANK = selMenuData.MRHST_RANK
    let ORDER_SAME_ORDER_COUNT = selMenuData.ORDER_SAME_ORDER_COUNT
    let PCKAGE_SLE_UNIT = selMenuData.PCKAGE_SLE_UNIT


    //let arrSmallParams = oMenus.ORDER_SEL_SETMENU

    let qOrderLarge =
        'INSERT INTO THISTORY_ORDER_LARGE (ORDER_NO' +
        ', MRHST_SEQ' +
        ', CSTMR_TEL_NO' +
        ', RECPTN_TEL_NO' +
        ', DLVR_AREA' +
        ', DLVR_DETAIL_AREA' +
        ', MEMO' +
        ', SI' +
        ', GU' +
        ', DONG' +
        ', LI' +
        ', BUNJI' +
        ', ROAD_NM' +
        ', BULD_NM' +
        ', CRDNT_X' +
        ', CRDNT_Y' +
        ', BFE_X' +
        ', BFE_Y' +
        ', ADRES_TYPE' +
        ', ORDER_DE' +
        ', ORDER_HMS' +
        ', PRE_PYMNT_SE_CODE' +
        ', PYMNT_TY' +
        ', ORDER_AMOUNT' +
        ', DLVR_AMOUNT' +
        ', ORDER_STTUS_CODE' +
        ', ORDER_STTUS_DE_HMS' +
        ', DLVR_EXPECT_TIME' +
        ', CANCL_RESN_CODE' +
        ', CANCL_OPETR_CODE' +
        ', ORDER_URL' +
        ', CSID' +
        ', MRHST_BSN_DE' +
        ', RESVE_GNRL_SE' +
        ', ORDER_SE' +
        ', DLVR_SE) \n' +

        'VALUES ((select IFNULL(max(SLE_HIST_SEQ), 0)+1000000001 from (select sle_hist_seq from thistory_order_large) tmp)' +
        ', :MRHST_SEQ' +
        ', HEX(AES_ENCRYPT(:CSTMR_TEL_NO,"$DlShQpDlTus&)(*&^%$#&WnAnsDlFur"))' +
        ', HEX(AES_ENCRYPT(:RECPTN_TEL_NO,"$DlShQpDlTus&)(*&^%$#&WnAnsDlFur"))' +
        ', :DLVR_AREA' +
        ', :DLVR_DETAIL_AREA' +
        ', :MEMO' +
        ', :SI' +
        ', :GU' +
        ', :DONG' +
        ', :LI' +
        ', :BUNJI' +
        ', :ROAD_NM' +
        ', :BULD_NM' +
        ', :CRDNT_X' +
        ', :CRDNT_Y' +
        ', :BFE_X' +
        ', :BFE_Y' +
        ', :ADRES_TYPE' +
        ', REPLACE(substring(curdate(),1,10),"-","")' +
        ', REPLACE(curtime(),":","")' +
        ', :PRE_PYMNT_SE_CODE' +
        ', :PYMNT_TY' +
        ', :ORDER_AMOUNT' +
        ', :DLVR_AMOUNT' +
        ', :ORDER_STTUS_CODE' +
        ', concat(REPLACE(substring(curdate(),1,10),"-",""),REPLACE(curtime(),":",""))' +
        ', :DLVR_EXPECT_TIME' +
        ', :CANCL_RESN_CODE' +
        ', :CANCL_OPETR_CODE' +
        ', :ORDER_URL' +
        ', :CSID' +
        ', get_branch_bsn_de(:MRHST_SEQ)' +
        ', :RESVE_GNRL_SE' +
        ', :ORDER_SE' +
        ', :DLVR_SE) '


    sync.fiber(function () {
        /////////////////////////////////////////
        // 배달료 상품 정보 조회...
        let dlvrQuery = 'SELECT dl.LCLAS_CODE, dl.LCLAS_NM, ds.SCLAS_CODE, ds.SCLAS_NM, ds.SCLAS_TYPE, dsp.SLE_UNIT '
            + ' , dsp.SLE_AMOUNT, 901 + ifnull(dsp.SLE_UNIT_DC_SORT,0) AS SLE_SORT, BSS.DLVR_AMOUNT_INCLS_AT '
            + ' , IFNULL(dsp.SCLAS_SLE_UNIT, "기본배달료") AS SCLAS_SLE_UNIT '
            + ' , dsp.POS_SCLAS_CODE, dsp.POS_SCLAS_NM '
            + ' FROM tbranch_delivery_large dl '
            + ' JOIN tbranch_delivery_small ds ON dl.MRHST_SEQ = ds.MRHST_SEQ AND dl.LCLAS_CODE = ds.LCLAS_CODE '
            + ' JOIN tbranch_delivery_small_price dsp ON ds.MRHST_SEQ = dsp.MRHST_SEQ AND ds.SCLAS_CODE = dsp.SCLAS_CODE '
            + ' LEFT JOIN tinfo_branch_store_state AS BSS ON BSS.MRHST_SEQ = dl.MRHST_SEQ '
            + ' WHERE ds.MRHST_SEQ = :MRHST_SEQ '
            + ' AND ds.SCLAS_CODE = 9000000001 '
            + ' AND ds.SCLAS_TYPE = "100004" '
            + ' AND dsp.USE_AT = "Y" '

        sync.parallel(function () {
            ssoDB.excuteQuerySelect(dlvrQuery, {MRHST_SEQ: MRHST_SEQ}, true, sync.defers())
        })
        let retDlvr = sync.await()
        let retDlvr_err = [].concat.apply([], retDlvr)[0]
        let retDlvr_ret = [].concat.apply([], retDlvr)[1]
        if (retDlvr_err) {
            logger.error('selQuery - procFinalOrderExecuteQuery_new - dlvrQuery - csid: ' + csid + ' - err: ' + retDlvr_err)

            let sendData = {RESULT: 'ERROR'}
            let pubData = {data: sendData, workType: workType, csid: csid}
            redis.pubRedis(CONSTS_SYS.PUB_CH.RES_ORDER + CNAME, pubData)
            return
        }


        arrLargeParams.DLVR_AMOUNT = 0

        let sleAmount = -1

        if (retDlvr_ret && retDlvr_ret.length > 0) {
            let sInclsAt = retDlvr_ret[0].DLVR_AMOUNT_INCLS_AT || ''
            if (sInclsAt === 'A' && ADDR_TYPE !== 'P') {
                let oDlvrRes = retDlvr_ret[0]
                let iSleAmount = oDlvrRes.SLE_AMOUNT || 0
                sleAmount = parseInt(iSleAmount)
                arrLargeParams.DLVR_AMOUNT = sleAmount
            }
        }


        ////////////////////////////////////////////
        // insert order large data...
        sync.parallel(function () {
            ssoDB.excuteQueryInsert(qOrderLarge, arrLargeParams, true, sync.defers())
        })
        let retLarge = sync.await()
        let retLarge_err = [].concat.apply([], retLarge)[0]
        let retLarge_ret = [].concat.apply([], retLarge)[1]
        if (retLarge_err) {
            logger.error('selQuery - procFinalOrderSetMenu_new - insertLargeData - csid: ' + csid + ' - err: ' + retLarge_err)

            let sendData = {RESULT: 'ERROR'}
            let pubData = {data: sendData, workType: workType, csid: csid}
            redis.pubRedis(CONSTS_SYS.PUB_CH.RES_ORDER + CNAME, pubData)
            return
        }

        let insertID = retLarge_ret.insertId
        logger.info('selQuery - procFinalOrderSetMenu_new - insertOrderLarge - insertID :   ' + insertID + ' - aff: ' + JSON.stringify(retLarge_ret))


        /////////////////////////////////////////////////////
        // select small data...
        // let qSelSmall = 'SELECT t1.MRHST_SEQ \n' +
        //     ', t1.PCKAGE_SCLAS_CODE \n' +
        //     ', t2.LCLAS_CODE \n' +
        //     ', get_lclas_name(t1.MRHST_SEQ, t2.LCLAS_CODE) AS LCLAS_NM \n' +
        //     ', t2.SCLAS_CODE \n' +
        //     ', (case when isnull(ms.SCLAS_NM) = 1 THEN cs.SCLAS_NM ELSE ms.SCLAS_NM END) AS SCLAS_NM \n' +
        //     ', (case when isnull(msp.SCLAS_SLE_UNIT) = 1 THEN csp.SCLAS_SLE_UNIT ELSE msp.SCLAS_SLE_UNIT END) AS SCLAS_SLE_UNIT \n' +
        //     ', t2.SCLAS_TYPE \n' +
        //     ', t2.MENU_GOODS_AT \n' +
        //     ', t2.SLE_UNIT \n' +
        //     ', (case when isnull(msp.SLE_AMOUNT) = 1 THEN csp.SLE_AMOUNT ELSE msp.SLE_AMOUNT END) SLE_AMOUNT \n' +
        //     ', t2.SLE_SORT \n' +
        //     ', 1 AS SLE_QY \n' +
        //     ', 1 AS SAME_ORDER_GOODS_SEQ \n' +
        //     ', (case when isnull(msp.POS_SCLAS_CODE) = 1 THEN csp.POS_SCLAS_CODE ELSE msp.POS_SCLAS_CODE END) AS POS_SCLAS_CODE \n' +
        //     ', (case when isnull(msp.POS_SCLAS_NM) = 1 THEN csp.POS_SCLAS_NM ELSE msp.POS_SCLAS_NM END) AS POS_SCLAS_NM \n' +
        //     'FROM tchg_analysis_order_package_rank_01 AS t1 \n' +
        //     'JOIN tchg_analysis_order_package_goodscode_01 AS t2 ON t1.MRHST_SEQ = t2.MRHST_SEQ \n' +
        //     'AND t1.PCKAGE_SCLAS_CODE = t2.PCKAGE_SCLAS_CODE \n' +
        //     'AND t1.PCKAGE_SLE_UNIT = t2.PCKAGE_SLE_UNIT \n' +
        //     'LEFT JOIN tbranch_menu_small AS ms ON t2.MRHST_SEQ = ms.MRHST_SEQ \n' +
        //     'AND t2.SCLAS_CODE = ms.SCLAS_CODE \n' +
        //     'LEFT JOIN tbranch_menu_small_price AS msp ON msp.MRHST_SEQ = t2.MRHST_SEQ \n' +
        //     'AND msp.SCLAS_CODE = t2.SCLAS_CODE \n' +
        //     'AND msp.SLE_UNIT = t2.SLE_UNIT \n' +
        //     'LEFT JOIN tbranch_choise_small AS cs ON t2.MRHST_SEQ = cs.MRHST_SEQ \n' +
        //     'AND t2.SCLAS_CODE = cs.SCLAS_CODE \n' +
        //     'LEFT JOIN tbranch_choise_small_price AS csp ON csp.MRHST_SEQ = t2.MRHST_SEQ \n' +
        //     'AND csp.SCLAS_CODE = t2.SCLAS_CODE \n' +
        //     'AND csp.SLE_UNIT = t2.SLE_UNIT \n' +
        //     'WHERE t1.SCLAS_TYPE = "100001" \n' +
        //     'AND t1.USE_AT = \'Y\' \n' +
        //     'AND t1.MRHST_SEQ = :MRHST_SEQ \n' +
        //     'AND t1.PCKAGE_SCLAS_CODE = :PCKAGE_SCLAS_CODE \n' +
        //     'AND t1.PCKAGE_SLE_UNIT = :PCKAGE_SLE_UNIT \n' +
        //     'ORDER BY t2.SLE_SORT;'

        let qSelSmall = 'select  \n' +
            '\tt1.MRHST_SEQ\n' +
            '\t, t1.PCKAGE_SCLAS_CODE \n' +
            '\t, t2.LCLAS_CODE \n' +
            '\t, get_lclas_name(t1.MRHST_SEQ, t2.LCLAS_CODE) AS LCLAS_NM \n' +
            '\t, t2.SCLAS_CODE \n' +
            '\t, ifnull(ms.SCLAS_NM, cs.SCLAS_NM) AS SCLAS_NM \n' +
            '\t, ifnull(msp.SCLAS_SLE_UNIT, csp.SCLAS_SLE_UNIT) AS SCLAS_SLE_UNIT \n' +
            '\t, t2.SCLAS_TYPE \n' +
            '\t, t2.MENU_GOODS_AT \n' +
            '\t, t2.SLE_UNIT \n' +
            '\t, ifnull(msp.SLE_AMOUNT, csp.SLE_AMOUNT) AS SLE_AMOUNT \n' +
            '\t, t2.SLE_SORT \n' +
            '\t, 1 AS SLE_QY \n' +
            '\t, 1 AS SAME_ORDER_GOODS_SEQ \n' +
            'from tchg_analysis_order_package_rank_01 AS t1 \n' +
            'join tchg_analysis_order_package_goodscode_01 AS t2 on t1.MRHST_SEQ = t2.MRHST_SEQ  and  t1.PCKAGE_SCLAS_CODE = t2.PCKAGE_SCLAS_CODE and t1.PCKAGE_SLE_UNIT = t2.PCKAGE_SLE_UNIT\n' +
            'left join  tbranch_menu_small AS ms on t2.MRHST_SEQ = ms.MRHST_SEQ  and t2.SCLAS_CODE = ms.SCLAS_CODE\n' +
            'left join  tbranch_menu_small_price AS msp on msp.MRHST_SEQ = t2.MRHST_SEQ and msp.SCLAS_CODE = t2.SCLAS_CODE and msp.SLE_UNIT = t2.SLE_UNIT\n' +
            'left join  tbranch_choise_small AS cs on t2.MRHST_SEQ = cs.MRHST_SEQ and t2.SCLAS_CODE = cs.SCLAS_CODE\n' +
            'left join  tbranch_choise_small_price AS csp on csp.MRHST_SEQ = t2.MRHST_SEQ and csp.SCLAS_CODE = t2.SCLAS_CODE and csp.SLE_UNIT = t2.SLE_UNIT    \n' +
            'where t1.SCLAS_TYPE = "100001" \n' +
            'and t1.USE_AT = "Y" \n' +
            'and t1.MRHST_SEQ = :MRHST_SEQ \n' +
            'and t1.PCKAGE_SCLAS_CODE = :PCKAGE_SCLAS_CODE \n' +
            'and t1.PCKAGE_SLE_UNIT = :PCKAGE_SLE_UNIT \n' +
            'order by t2.SLE_SORT;'

        let pSelSmall = {MRHST_SEQ: MRHST_SEQ, PCKAGE_SCLAS_CODE: PCKAGE_SCLAS_CODE, PCKAGE_SLE_UNIT: PCKAGE_SLE_UNIT}

        sync.parallel(function () {
            ssoDB.excuteQuerySelect(qSelSmall, pSelSmall, true, sync.defers())
        })
        let retSmall = sync.await()
        let retSmall_err = [].concat.apply([], retSmall)[0]
        let retSmall_ret = [].concat.apply([], retSmall)[1]
        if (retSmall_err) {
            logger.error('selQuery - procFinalOrderSetMenu_new - insertLargeData - csid: ' + csid + ' - err: ' + retSmall)

            let sendData = {RESULT: 'ERROR'}
            let pubData = {data: sendData, workType: workType, csid: csid}
            redis.pubRedis(CONSTS_SYS.PUB_CH.RES_ORDER + CNAME, pubData)
            return
        }


        //////////////////////////////////////////////////
        // insert order small data....

        // sorting small menu
        retSmall_ret = retSmall_ret.sort(doSort_SAME_ORDER_GOODS_SEQ)
        sync.parallel(function () {
            sortSmallMenu(retSmall_ret, sync.defers())
        })
        let sortedArr = sync.await()
        sortedArr = [].concat.apply([], sortedArr)[0]

        // make small menu
        sync.parallel(function () {
            makeQuerySmallOrder(insertID, sortedArr, sync.defers())
        })
        let smallOrderInsertQuery = sync.await()
        let dlvrSAME_ORDER_GOODS_SEQ = [].concat.apply([], smallOrderInsertQuery)[0]
        let orderSmallInsertQuery = [].concat.apply([], smallOrderInsertQuery)[1]
        logger.info('selQuery - procFinalOrderSetMenu_new - orderSmall - query: ' + orderSmallInsertQuery)

        // insert small menu...
        sync.parallel(function () {
            ssoDB.excuteQueryInsert(orderSmallInsertQuery, '', true, sync.defers())
        })
        let retSmallOrder = sync.await()
        let retSmallOrder_err = [].concat.apply([], retSmallOrder)[0]
        let retSmallOrder_ret = [].concat.apply([], retSmallOrder)[1]
        if (retSmallOrder_err) {
            logger.error('selQuery - procFinalOrderSetMenu_new - insert order small - insertID: ' + insertID + ' - err: ' + retSmallOrder_err)
            let sendData = {RESULT: 'ERROR'}
            let pubData = {data: sendData, workType: workType, csid: csid}
            redis.pubRedis(CONSTS_SYS.PUB_CH.RES_ORDER + CNAME, pubData)
            return
        }

        logger.info('selQuery - procFinalOrderSetMenu_new - insert order small -  orderLarge insertID: ' + insertID + ' - retSmallOrder_ret: ' + JSON.stringify(retSmallOrder_ret))


        //////////////////////////////////////////////////
        // update order large URL param...
        let reORDER_URL = CONSTS.CONN_SETTING.OVS_ORDER_CHK_URL + '/' + insertID
        let qUrl = "UPDATE THISTORY_ORDER_LARGE SET ORDER_URL = :ORDER_URL WHERE SLE_HIST_SEQ = :SLE_HIST_SEQ"
        let pUrl = {
            ORDER_URL: reORDER_URL,
            SLE_HIST_SEQ: insertID
        }
        logger.info('selQuery - procFinalOrderSetMenu_new - UPDATE THISTORY_ORDER_LARGE - ORDER_URL: ' + reORDER_URL + ' , insertID: ' + insertID)
        sync.parallel(function () {
            ssoDB.excuteQueryUpDel(qUrl, pUrl, true, sync.defers())
        })
        let retUpUrl = sync.await()
        let retUpUrl_err = [].concat.apply([], retUpUrl)[0]
        //let retUpUrl_ret = [].concat.apply([], retUpUrl)[1]
        if (retUpUrl_err) {
            logger.error('selQuery - procFinalOrderSetMenu_new - update order URL insertID - insertID: ' + insertID + ' - err: ' + retUpUrl_err)
            let sendData = {RESULT: 'ERROR'}
            let pubData = {data: sendData, workType: workType, csid: csid}
            redis.pubRedis(CONSTS_SYS.PUB_CH.RES_ORDER + CNAME, pubData)
            return
        }


        ////////////////////////////////////
        // check address...
        logger.debug('selQuery - procFinalOrderSetMenu_new - ADDR_TYPE: ' + ADDR_TYPE + ' - MRHST_SEQ: ' + MRHST_SEQ + ' , CSTMR_TEL_NO: ' + CSTMR_TEL_NO + ' , DLVR_AREA: ' + DLVR_AREA + ' , DLVR_DETAIL_AREA: ' + DLVR_DETAIL_AREA)

        if (ADDR_TYPE === 'I')  // insert address info...
        {
            sync.parallel(function () {
                insertAddress_TTINFO_CUSTOMER(MRHST_SEQ, CSTMR_TEL_NO, DLVR_AREA, DLVR_DETAIL_AREA, MEMO, SI, GU, DONG, LI, BUNJI, ROAD_NM, BULD_NM, CRDNT_X, CRDNT_Y, BFE_X, BFE_Y, ADRES_TYPE, sync.defers())
                //insertAddress_TTINFO_CUSTOMER(MRHST_SEQ, CSTMR_TEL_NO, DLVR_AREA, DLVR_DETAIL_AREA, MEMO, SI, GU, DONG, LI, BUNJI, ROAD_NM, BULD_NM, CRDNT_X, CRDNT_Y, ADRES_TYPE, sync.defers())
            })
            let retAddrIn = sync.await()
            let retAddrIn_err = [].concat.apply([], retAddrIn)[0]
            let retAddrIn_ret = [].concat.apply([], retAddrIn)[1]
            if (retAddrIn_err) {
                logger.error('selQuery - procFinalOrderSetMenu_new - insertCustomerAddressInfo - MRHST_SEQ: ' + MRHST_SEQ + ' - err: ' + retAddrIn_err, CSTMR_TEL_NO)
            }
            // else {
            //     logger.info('selQuery - procFinalOrderSetMenu_new - insertCustomerAddressInfo - MRHST_SEQ: ' + MRHST_SEQ, CSTMR_TEL_NO)
            // }
            else {
                if(retAddrIn_ret === 'DUP'){
                    sync.parallel(function () {
                        updateAddress_TTINFO_CUSTOMER(MRHST_SEQ, CSTMR_TEL_NO, DLVR_AREA, DLVR_DETAIL_AREA, MEMO, SI, GU, DONG, LI, BUNJI, ROAD_NM, BULD_NM, CRDNT_X, CRDNT_Y, BFE_X, BFE_Y, ADRES_TYPE, sync.defers())
                        //updateAddress_TTINFO_CUSTOMER(MRHST_SEQ, CSTMR_TEL_NO, DLVR_AREA, DLVR_DETAIL_AREA, MEMO, SI, GU, DONG, LI, BUNJI, ROAD_NM, BULD_NM, CRDNT_X, CRDNT_Y, ADRES_TYPE, sync.defers())
                    })
                    let retAddrInUp = sync.await()
                    let retAddrInUp_err = [].concat.apply([], retAddrInUp)[0]
                    //let retAddrInUp_ret = [].concat.apply([], retAddrInUp)[1]
                    if (retAddrInUp_err) {
                        logger.error('selQuery - procFinalOrderSetMenu_new - insertCustomerAddressInfo - DUP - updateTINFO - MRHST_SEQ: ' + MRHST_SEQ + ' - err: ' + retAddrInUp_err, CSTMR_TEL_NO)
                    }
                    else {
                        logger.info('selQuery - procFinalOrderSetMenu_new - insertCustomerAddressInfo - DUP - updateTINFO - MRHST_SEQ: ' + MRHST_SEQ, CSTMR_TEL_NO)
                    }
                }
                else {
                    logger.info('selQuery - procFinalOrderSetMenu_new - insertCustomerAddressInfo - MRHST_SEQ: ' + MRHST_SEQ, CSTMR_TEL_NO)
                }
            }
        }
        else if (ADDR_TYPE === 'U')  // update address info...
        {
            sync.parallel(function () {
                updateAddress_TTINFO_CUSTOMER(MRHST_SEQ, CSTMR_TEL_NO, DLVR_AREA, DLVR_DETAIL_AREA, MEMO, SI, GU, DONG, LI, BUNJI, ROAD_NM, BULD_NM, CRDNT_X, CRDNT_Y, BFE_X, BFE_Y, ADRES_TYPE, sync.defers())
                //updateAddress_TTINFO_CUSTOMER(MRHST_SEQ, CSTMR_TEL_NO, DLVR_AREA, DLVR_DETAIL_AREA, MEMO, SI, GU, DONG, LI, BUNJI, ROAD_NM, BULD_NM, CRDNT_X, CRDNT_Y, ADRES_TYPE, sync.defers())
            })
            let retAddrUp = sync.await()
            let retAddrUp_err = [].concat.apply([], retAddrUp)[0]
            //let retAddrUp_ret = [].concat.apply([], retAddrUp)[1]
            if (retAddrUp_err) {
                logger.error('selQuery - procFinalOrderSetMenu_new - updateCustomerAddressInfo - MRHST_SEQ: ' + MRHST_SEQ + ' - err: ' + retAddrUp_err, CSTMR_TEL_NO)
            }
            else {
                logger.info('selQuery - procFinalOrderSetMenu_new - updateCustomerAddressInfo - MRHST_SEQ: ' + MRHST_SEQ, CSTMR_TEL_NO)
            }
        }
        else if (ADDR_TYPE === 'P')  // update address info... shop pick up...
        {
            logger.info('selQuery - procFinalOrderSetMenu_new - ADDR_TYPE: P - No action customer address info...')
        }  // end of (ADDR_TYPE === 'P')

        else if (ADDR_TYPE === 'N') {  // No action...
            logger.info('selQuery - procFinalOrderSetMenu_new - ADDR_TYPE: N - No action customer address info...')
        }


        /////////////////////////////////////////
        // select order cancel time...
        sync.parallel(function () {
            selectOrderCancelTime(MRHST_SEQ, sync.defers())
        })
        let retCncl = sync.await()
        let retCncl_err = [].concat.apply([], retCncl)[0]
        let retCncl_ret = [].concat.apply([], retCncl)[1]
        let retCancelTime = ''
        if (retCncl_err) {
            logger.error('selQuery - procFinalOrderSetMenu - order cancel time - err: ' + retCncl_err)
        }
        else {
            retCancelTime = retCncl_ret
        }

        logger.info('selQuery - procFinalOrderSetMenu - end order - MRHST_SEQ: ' + MRHST_SEQ + ' , insertID: ' + insertID + ' , retCancelTime: ' + retCancelTime, CSTMR_TEL_NO)


        ////////////////////////////
        // 배달료 insert...
        if (retDlvr_ret && retDlvr_ret.length > 0) {
            if (retDlvr_ret[0].DLVR_AMOUNT_INCLS_AT === 'A' && arrLargeParams.ADDR_TYPE !== 'P') {
                sync.parallel(function () {
                    insertDeliveryAmount(insertID, dlvrSAME_ORDER_GOODS_SEQ, retDlvr_ret, sync.defers())
                })
                let retDlvrAmnt = sync.await()
                let retDlvrAmnt_err = [].concat.apply([], retDlvrAmnt)[0]
                //let retDlvrAmnt_ret = [].concat.apply([], retDlvrAmnt)[1]
                if (retDlvrAmnt_err) {
                    logger.error('selQuery - procFinalOrderSetMenu - insert delevery amount - insertID: ' + insertID + ' - err: ' + retDlvrAmnt_err)
                }

                doFinalOrderToShop(MRHST_SEQ, posCode, shopNumber, insertID, csid, workType, retCancelTime, CNAME)
            }
            else {
                doFinalOrderToShop(MRHST_SEQ, posCode, shopNumber, insertID, csid, workType, retCancelTime, CNAME)
            }
        }
        else {
            doFinalOrderToShop(MRHST_SEQ, posCode, shopNumber, insertID, csid, workType, retCancelTime, CNAME)
        }
    })  // end of sync.fiber()
}


/// 0705
//////////////////////////////////////
// HISTORY 메뉴 인서트....
// HISTORY - FINAL ORDER Query.....
exports.procFinalOrderHistory_new = (oData) => {
    // { workType: 'WT_ORDER_HISTORY',
    //   option: 'SNT306_HISTORY',
    //   params:
    //           [ { ORDER_LARGE: [Object] },
    //             { ORDER_SEL_HISTORY: [Object] },
    //             { MRHST_SEQ: 5, POS_CODE: '100001', SHOP_NUMBER: '07052013279' , USER_MDN: userMdn} ],
    //   csid: 'test',
    //   isRes: true,
    //    CNAME: '296BF' }

    logger.info('selQuery - procFinalOrderHistory_new - oData: ' + JSON.stringify(oData))

    let CNAME = oData.CNAME
    let csid = oData.csid
    let workType = oData.workType
    let oBase = oData.params[2]

    let MRHST_SEQ = oBase.MRHST_SEQ
    let posCode = oBase.POS_CODE
    let shopNumber = oBase.SHOP_NUMBER
    let userMdn = oBase.USER_MDN

    let arrSmallParams = oData.params[1].ORDER_SEL_HISTORY
    // { SLE_HIST_SEQ: '22356',
    //     SCLAS_SLE_UNIT: '간바치 외 3건',
    //     ORDER_AMOUNT: '139,000' }

    let SLE_HIST_SEQ = arrSmallParams.SLE_HIST_SEQ

    let arrLargeParams = oData.params[0].ORDER_LARGE
    let oBFE_X = arrLargeParams.BFE_X
    let oBFE_Y = arrLargeParams.BFE_Y
    if(!oBFE_X){
        arrLargeParams.BFE_X = null
    }
    if(!oBFE_Y){
        arrLargeParams.BFE_Y = null
    }

    let ADDR_TYPE = arrLargeParams.ADDR_TYPE


    let qOrderLarge =
        'INSERT INTO THISTORY_ORDER_LARGE (ORDER_NO' +
        ', MRHST_SEQ' +
        ', CSTMR_TEL_NO' +
        ', RECPTN_TEL_NO' +
        ', DLVR_AREA' +
        ', DLVR_DETAIL_AREA' +
        ', MEMO' +
        ', SI' +
        ', GU' +
        ', DONG' +
        ', LI' +
        ', BUNJI' +
        ', ROAD_NM' +
        ', BULD_NM' +
        ', CRDNT_X' +
        ', CRDNT_Y' +
        ', BFE_X' +
        ', BFE_Y' +
        ', ADRES_TYPE' +
        ', ORDER_DE' +
        ', ORDER_HMS' +
        ', PRE_PYMNT_SE_CODE' +
        ', PYMNT_TY' +
        ', ORDER_AMOUNT' +
        ', DLVR_AMOUNT' +
        ', ORDER_STTUS_CODE' +
        ', ORDER_STTUS_DE_HMS' +
        ', DLVR_EXPECT_TIME' +
        ', CANCL_RESN_CODE' +
        ', CANCL_OPETR_CODE' +
        ', ORDER_URL' +
        ', CSID' +
        ', MRHST_BSN_DE' +
        ', RESVE_GNRL_SE' +
        ', ORDER_SE' +
        ', DLVR_SE) \n' +

        'VALUES ((select IFNULL(max(SLE_HIST_SEQ), 0)+1000000001 from (select sle_hist_seq from thistory_order_large) tmp)' +
        ', :MRHST_SEQ' +
        ', HEX(AES_ENCRYPT(:CSTMR_TEL_NO,"$DlShQpDlTus&)(*&^%$#&WnAnsDlFur"))' +
        ', HEX(AES_ENCRYPT(:RECPTN_TEL_NO,"$DlShQpDlTus&)(*&^%$#&WnAnsDlFur"))' +
        ', :DLVR_AREA' +
        ', :DLVR_DETAIL_AREA' +
        ', :MEMO' +
        ', :SI' +
        ', :GU' +
        ', :DONG' +
        ', :LI' +
        ', :BUNJI' +
        ', :ROAD_NM' +
        ', :BULD_NM' +
        ', :CRDNT_X' +
        ', :CRDNT_Y' +
        ', :BFE_X' +
        ', :BFE_Y' +
        ', :ADRES_TYPE' +
        ', REPLACE(substring(curdate(),1,10),"-","")' +
        ', REPLACE(curtime(),":","")' +
        ', :PRE_PYMNT_SE_CODE' +
        ', :PYMNT_TY' +
        ', :ORDER_AMOUNT' +
        ', :DLVR_AMOUNT' +
        ', :ORDER_STTUS_CODE' +
        ', concat(REPLACE(substring(curdate(),1,10),"-",""),REPLACE(curtime(),":",""))' +
        ', :DLVR_EXPECT_TIME' +
        ', :CANCL_RESN_CODE' +
        ', :CANCL_OPETR_CODE' +
        ', :ORDER_URL' +
        ', :CSID' +
        ', get_branch_bsn_de(:MRHST_SEQ)' +
        ', :RESVE_GNRL_SE' +
        ', :ORDER_SE' +
        ', :DLVR_SE) '


    sync.fiber(function () {
        /////////////////////////////////////////
        // 배달료 상품 정보 조회...
        let dlvrQuery = 'SELECT dl.LCLAS_CODE, dl.LCLAS_NM, ds.SCLAS_CODE, ds.SCLAS_NM, ds.SCLAS_TYPE, dsp.SLE_UNIT '
            + ' , dsp.SLE_AMOUNT, 901 + ifnull(dsp.SLE_UNIT_DC_SORT,0) AS SLE_SORT, BSS.DLVR_AMOUNT_INCLS_AT '
            + ' , IFNULL(dsp.SCLAS_SLE_UNIT, "기본배달료") AS SCLAS_SLE_UNIT '
            + ' , dsp.POS_SCLAS_CODE, dsp.POS_SCLAS_NM '
            + ' FROM tbranch_delivery_large dl '
            + ' JOIN tbranch_delivery_small ds ON dl.MRHST_SEQ = ds.MRHST_SEQ AND dl.LCLAS_CODE = ds.LCLAS_CODE '
            + ' JOIN tbranch_delivery_small_price dsp ON ds.MRHST_SEQ = dsp.MRHST_SEQ AND ds.SCLAS_CODE = dsp.SCLAS_CODE '
            + ' LEFT JOIN tinfo_branch_store_state AS BSS ON BSS.MRHST_SEQ = dl.MRHST_SEQ '
            + ' WHERE ds.MRHST_SEQ = :MRHST_SEQ '
            + ' AND ds.SCLAS_CODE = 9000000001 '
            + ' AND ds.SCLAS_TYPE = "100004" '
            + ' AND dsp.USE_AT = "Y" '

        sync.parallel(function () {
            ssoDB.excuteQuerySelect(dlvrQuery, {MRHST_SEQ: MRHST_SEQ}, true, sync.defers())
        })
        let retDlvr = sync.await()
        let retDlvr_err = [].concat.apply([], retDlvr)[0]
        let retDlvr_ret = [].concat.apply([], retDlvr)[1]
        if (retDlvr_err) {
            logger.error('selQuery - procFinalOrderHistory_new - dlvrQuery - csid: ' + csid + ' - err: ' + retDlvr_err)

            let sendData = {RESULT: 'ERROR'}
            let pubData = {data: sendData, workType: workType, csid: csid}
            redis.pubRedis(CONSTS_SYS.PUB_CH.RES_ORDER + CNAME, pubData)
            return
        }


        let sleAmount = 0

        arrLargeParams.DLVR_AMOUNT = 0

        if (retDlvr_ret && retDlvr_ret.length > 0) {
            let sInclsAt = retDlvr_ret[0].DLVR_AMOUNT_INCLS_AT || ''
            if (sInclsAt === 'A' && ADDR_TYPE !== 'P') {
                let oDlvrRes = retDlvr_ret[0]
                let iSleAmount = oDlvrRes.SLE_AMOUNT || 0
                sleAmount = parseInt(iSleAmount)
                arrLargeParams.DLVR_AMOUNT = sleAmount
            }
        }


        ////////////////////////////////////////////
        // insert order large data...
        sync.parallel(function () {
            ssoDB.excuteQueryInsert(qOrderLarge, arrLargeParams, true, sync.defers())
        })
        let retLarge = sync.await()
        let retLarge_err = [].concat.apply([], retLarge)[0]
        let retLarge_ret = [].concat.apply([], retLarge)[1]
        if (retLarge_err) {
            logger.error('selQuery - procFinalOrderHistory_new - insertLargeData - csid: ' + csid + ' - err: ' + retLarge_err)

            let sendData = {RESULT: 'ERROR'}
            let pubData = {data: sendData, workType: workType, csid: csid}
            redis.pubRedis(CONSTS_SYS.PUB_CH.RES_ORDER + CNAME, pubData)
            return
        }

        let insertID = retLarge_ret.insertId
        logger.info('selQuery - procFinalOrderHistory_new - insertOrderLarge - insertID :   ' + insertID + ' - aff: ' + JSON.stringify(retLarge_ret))


        let CSTMR_TEL_NO = arrLargeParams.CSTMR_TEL_NO
        let DLVR_AREA = arrLargeParams.DLVR_AREA
        let DLVR_DETAIL_AREA = arrLargeParams.DLVR_DETAIL_AREA
        let MEMO = arrLargeParams.MEMO
        let SI = arrLargeParams.SI
        let GU = arrLargeParams.GU
        let DONG = arrLargeParams.DONG
        let LI = arrLargeParams.LI
        let BUNJI = arrLargeParams.BUNJI
        let ROAD_NM = arrLargeParams.ROAD_NM
        let BULD_NM = arrLargeParams.BULD_NM
        let CRDNT_X = arrLargeParams.CRDNT_X
        let CRDNT_Y = arrLargeParams.CRDNT_Y
        let BFE_X = arrLargeParams.BFE_X
        let BFE_Y = arrLargeParams.BFE_Y
        let ADRES_TYPE = arrLargeParams.ADRES_TYPE


        /////////////////////////////////////////////////////
        // select small data...
        // let qSelSmall = 'SELECT OL.SLE_HIST_SEQ\n' +
        //     '\t, OSH.SAME_ORDER_GOODS_SEQ \n' +
        //     '\t, OL.ORDER_NO\n' +
        //     '\t, OL.MRHST_SEQ\n' +
        //     '\t, OSH.LCLAS_CODE\n' +
        //     '\t, get_lclas_name(OL.MRHST_SEQ, OSH.LCLAS_CODE) LCLAS_NM\n' +
        //     '\t, OSH.SCLAS_CODE \n' +
        //     '\t, ifnull(ms.SCLAS_NM, cs.SCLAS_NM) SCLAS_NM\n' +
        //     '\t, ifnull(MSP.SCLAS_SLE_UNIT , csp.SCLAS_SLE_UNIT) AS SCLAS_SLE_UNIT\n' +
        //     '\t, OL.ORDER_AMOUNT\n' +
        //     '\t, OSH.SLE_QY\n' +
        //     '\t, ifnull(MSP.SLE_UNIT, CSP.SLE_UNIT) AS SLE_UNIT \n' +
        //     '\t, OSH.SLE_QY * ifnull(MSP.SLE_AMOUNT, CSP.SLE_AMOUNT) AS SLE_AMOUNT \n' +
        //     '\t, OSH.MENU_GOODS_AT \n' +
        //     '\t, OSH.SLE_SORT \n' +
        //     '\t, MS.POS_MRHST_SEQ , MSP.POS_SCLAS_CODE, MSP.POS_SCLAS_NM \n' +
        //     'FROM (\n' +
        //     '\tSELECT ol.ORDER_STTUS_CODE\n' +
        //     '\t\t, ol.MRHST_SEQ\n' +
        //     '\t\t, ol.SLE_HIST_SEQ\n' +
        //     '\t\t, ol.ORDER_NO\n' +
        //     '\t\t, SUM( os.SLE_QY * ifnull(ms.SLE_AMOUNT, cs.SLE_AMOUNT)) ORDER_AMOUNT\n' +
        //     '\t\t, os.SAME_ORDER_GOODS_SEQ \n' +
        //     '\tFROM tanalysis_order_large_01 AS ol\n' +
        //     '\t\tJOIN tanalysis_order_small_history AS os ON os.SLE_HIST_SEQ = ol.SLE_HIST_SEQ \n' +
        //     '\t\tLEFT JOIN TBRANCH_MENU_SMALL_PRICE AS ms ON ms.MRHST_SEQ = os.MRHST_SEQ AND ms.SCLAS_CODE = os.SCLAS_CODE AND ms.SLE_UNIT = os.SLE_UNIT\n' +
        //     '\t\tLEFT JOIN TBRANCH_CHOISE_SMALL_PRICE AS cs ON cs.MRHST_SEQ = os.MRHST_SEQ AND cs.SCLAS_CODE = os.SCLAS_CODE AND cs.SLE_UNIT = os.SLE_UNIT\n' +
        //     '\tWHERE ol.MRHST_SEQ = :MRHST_SEQ\n' +
        //     '\t\tAND  \tNOT EXISTS(SELECT SLE_HIST_SEQ FROM tanalysis_sle_hist_seq_state sh \n' +
        //     '\tWHERE ol.MRHST_SEQ = :MRHST_SEQ \n' +
        //     '\t\tAND ol.SLE_HIST_SEQ = sh.SLE_HIST_SEQ)\n' +
        //     '\t\tAND   ol.CSTMR_TEL_NO = HEX(AES_ENCRYPT(:USER_MDN, "$DlShQpDlTus&)(*&^%$#&WnAnsDlFur") )\n' +
        //     '\t\tAND   ol.MRHST_BSN_DE > date_format(DATE_SUB(NOW(), INTERVAL 6 MONTH), "%Y%m%d") \n' +
        //     '\t\tAND   ol.ORDER_STTUS_CODE <> "100006" \n' +
        //     '\tGROUP by ol.MRHST_SEQ, ol.SLE_HIST_SEQ, ol.ORDER_NO\n' +
        //     '\tORDER BY ol.SLE_HIST_SEQ DESC\n' +
        //     '\tLIMIT 3    \n' +
        //     ') OL\n' +
        //     'JOIN tanalysis_order_small_history AS OSH ON OSH.SLE_HIST_SEQ = OL.SLE_HIST_SEQ\n' +
        //     'LEFT JOIN TBRANCH_MENU_SMALL AS MS ON  MS.MRHST_SEQ = OSH.MRHST_SEQ AND MS.SCLAS_CODE = OSH.SCLAS_CODE  \t\t\t\n' +
        //     'LEFT JOIN TBRANCH_CHOISE_SMALL AS CS ON CS.MRHST_SEQ = OSH.MRHST_SEQ AND CS.SCLAS_CODE = OSH.SCLAS_CODE\n' +
        //     'LEFT JOIN TBRANCH_MENU_SMALL_PRICE AS MSP ON  MSP.MRHST_SEQ = OSH.MRHST_SEQ AND MSP.SCLAS_CODE = OSH.SCLAS_CODE AND MSP.SLE_UNIT = OSH.SLE_UNIT\n' +
        //     'LEFT JOIN TBRANCH_CHOISE_SMALL_PRICE AS CSP ON CSP.MRHST_SEQ = OSH.MRHST_SEQ AND CSP.SCLAS_CODE = OSH.SCLAS_CODE AND CSP.SLE_UNIT = OSH.SLE_UNIT\n' +
        //     'WHERE OSH.MENU_GOODS_AT <> "D" AND OSH.SLE_HIST_SEQ = :SLE_HIST_SEQ AND MSP.USE_AT = "Y" \n' +
        //     'ORDER BY ol.SLE_HIST_SEQ DESC, OSH.SLE_SORT;'

        let qSelSmall = 'SELECT OL.SLE_HIST_SEQ\n' +
            '\t, OSH.SAME_ORDER_GOODS_SEQ \n' +
            '\t, OL.ORDER_NO\n' +
            '\t, OL.MRHST_SEQ\n' +
            '\t, OSH.LCLAS_CODE\n' +
            '\t, get_lclas_name(OL.MRHST_SEQ, OSH.LCLAS_CODE) LCLAS_NM\n' +
            '\t, OSH.SCLAS_CODE \n' +
            '\t, ifnull(ms.SCLAS_NM, cs.SCLAS_NM) SCLAS_NM\n' +
            '\t, ifnull(MSP.SCLAS_SLE_UNIT , csp.SCLAS_SLE_UNIT) AS SCLAS_SLE_UNIT\n' +
            '\t, OL.ORDER_AMOUNT\n' +
            '\t, OSH.SLE_QY\n' +
            '\t, ifnull(MSP.SLE_UNIT, CSP.SLE_UNIT) AS SLE_UNIT \n' +
            '\t, OSH.SLE_QY * ifnull(MSP.SLE_AMOUNT, CSP.SLE_AMOUNT) AS SLE_AMOUNT \n' +
            '\t, OSH.MENU_GOODS_AT \n' +
            '\t, OSH.SLE_SORT \n' +
            '\t, MS.POS_MRHST_SEQ , MSP.POS_SCLAS_CODE, MSP.POS_SCLAS_NM \n' +
            'FROM (\n' +
            '\tSELECT ol.ORDER_STTUS_CODE\n' +
            '\t\t, ol.MRHST_SEQ\n' +
            '\t\t, ol.SLE_HIST_SEQ\n' +
            '\t\t, ol.ORDER_NO\n' +
            '\t\t, SUM( os.SLE_QY * ifnull(ms.SLE_AMOUNT, cs.SLE_AMOUNT)) ORDER_AMOUNT\n' +
            '\t\t, os.SAME_ORDER_GOODS_SEQ \n' +
            '\tFROM tanalysis_order_large_01 AS ol\n' +
            '\t\tJOIN tanalysis_order_small_history AS os ON os.SLE_HIST_SEQ = ol.SLE_HIST_SEQ \n' +
            '\t\tLEFT JOIN TBRANCH_MENU_SMALL_PRICE AS ms ON ms.MRHST_SEQ = os.MRHST_SEQ AND ms.SCLAS_CODE = os.SCLAS_CODE AND ms.SLE_UNIT = os.SLE_UNIT\n' +
            '\t\tLEFT JOIN TBRANCH_CHOISE_SMALL_PRICE AS cs ON cs.MRHST_SEQ = os.MRHST_SEQ AND cs.SCLAS_CODE = os.SCLAS_CODE AND cs.SLE_UNIT = os.SLE_UNIT\n' +
            '\tWHERE ol.MRHST_SEQ = :MRHST_SEQ\n' +
            '\t\tAND  \tNOT EXISTS(SELECT SLE_HIST_SEQ FROM tanalysis_sle_hist_seq_state sh \n' +
            '\tWHERE ol.MRHST_SEQ = :MRHST_SEQ \n' +
            '\t\tAND ol.SLE_HIST_SEQ = sh.SLE_HIST_SEQ)\n' +
            '\t\tAND   ol.CSTMR_TEL_NO = HEX(AES_ENCRYPT(:USER_MDN, "$DlShQpDlTus&)(*&^%$#&WnAnsDlFur") )\n' +
            '\t\tAND   ol.MRHST_BSN_DE > date_format(DATE_SUB(NOW(), INTERVAL 6 MONTH), "%Y%m%d") \n' +
            '\t\tAND   ol.ORDER_STTUS_CODE <> "100006" \n' +
            '\tGROUP by ol.MRHST_SEQ, ol.SLE_HIST_SEQ, ol.ORDER_NO\n' +
            '\tORDER BY ol.SLE_HIST_SEQ DESC\n' +
            '\tLIMIT 3    \n' +
            ') OL\n' +
            'JOIN tanalysis_order_small_history AS OSH ON OSH.SLE_HIST_SEQ = OL.SLE_HIST_SEQ\n' +
            'LEFT JOIN TBRANCH_MENU_SMALL AS MS ON  MS.MRHST_SEQ = OSH.MRHST_SEQ AND MS.SCLAS_CODE = OSH.SCLAS_CODE  \t\t\t\n' +
            'LEFT JOIN TBRANCH_CHOISE_SMALL AS CS ON CS.MRHST_SEQ = OSH.MRHST_SEQ AND CS.SCLAS_CODE = OSH.SCLAS_CODE\n' +
            'LEFT JOIN TBRANCH_MENU_SMALL_PRICE AS MSP ON  MSP.MRHST_SEQ = OSH.MRHST_SEQ AND MSP.SCLAS_CODE = OSH.SCLAS_CODE AND MSP.SLE_UNIT = OSH.SLE_UNIT\n' +
            'LEFT JOIN TBRANCH_CHOISE_SMALL_PRICE AS CSP ON CSP.MRHST_SEQ = OSH.MRHST_SEQ AND CSP.SCLAS_CODE = OSH.SCLAS_CODE AND CSP.SLE_UNIT = OSH.SLE_UNIT\n' +
            'WHERE OSH.MENU_GOODS_AT <> "D" AND OSH.SLE_HIST_SEQ = :SLE_HIST_SEQ -- AND MSP.USE_AT = "Y" \n' +
            'ORDER BY ol.SLE_HIST_SEQ DESC, OSH.SLE_SORT;'

        let pSelSmall = {MRHST_SEQ: MRHST_SEQ, USER_MDN: userMdn, SLE_HIST_SEQ: SLE_HIST_SEQ}

        sync.parallel(function () {
            ssoDB.excuteQuerySelect(qSelSmall, pSelSmall, true, sync.defers())
        })
        let retSmall = sync.await()
        let retSmall_err = [].concat.apply([], retSmall)[0]
        let retSmall_ret = [].concat.apply([], retSmall)[1]
        if (retSmall_err) {
            logger.error('selQuery - procFinalOrderHistory_new - insertLargeData - csid: ' + csid + ' - err: ' + retSmall)

            let sendData = {RESULT: 'ERROR'}
            let pubData = {data: sendData, workType: workType, csid: csid}
            redis.pubRedis(CONSTS_SYS.PUB_CH.RES_ORDER + CNAME, pubData)
            return
        }

        //////////////////////////////////////////////////
        // insert order small data....
        // sorting small menu
        retSmall_ret = retSmall_ret.sort(doSort_SAME_ORDER_GOODS_SEQ)

        sync.parallel(function () {
            sortSmallMenu(retSmall_ret, sync.defers())
        })
        let sortedArr = sync.await()
        sortedArr = [].concat.apply([], sortedArr)[0]
        // make small menu
        sync.parallel(function () {
            makeQuerySmallOrder(insertID, sortedArr, sync.defers())
        })
        let smallOrderInsertQuery = sync.await()
        let dlvrSAME_ORDER_GOODS_SEQ = [].concat.apply([], smallOrderInsertQuery)[0]
        let orderSmallInsertQuery = [].concat.apply([], smallOrderInsertQuery)[1]
        logger.info('selQuery - procFinalOrderHistory_new - orderSmall - query: ' + orderSmallInsertQuery)

        // insert small menu...
        sync.parallel(function () {
            ssoDB.excuteQueryInsert(orderSmallInsertQuery, '', true, sync.defers())
        })
        let retSmallOrder = sync.await()
        let retSmallOrder_err = [].concat.apply([], retSmallOrder)[0]
        let retSmallOrder_ret = [].concat.apply([], retSmallOrder)[1]
        if (retSmallOrder_err) {
            logger.error('selQuery - procFinalOrderHistory_new - insert order small - insertID: ' + insertID + ' - err: ' + retSmallOrder_err)
            let sendData = {RESULT: 'ERROR'}
            let pubData = {data: sendData, workType: workType, csid: csid}
            redis.pubRedis(CONSTS_SYS.PUB_CH.RES_ORDER + CNAME, pubData)
            return
        }

        logger.info('selQuery - procFinalOrderHistory_new - insert order small -  orderLarge insertID: ' + insertID + ' - retSmallOrder_ret: ' + JSON.stringify(retSmallOrder_ret))


        //////////////////////////////////////////////////
        // update order large URL param...
        let reORDER_URL = CONSTS.CONN_SETTING.OVS_ORDER_CHK_URL + '/' + insertID
        let qUrl = "UPDATE THISTORY_ORDER_LARGE SET ORDER_URL = :ORDER_URL WHERE SLE_HIST_SEQ = :SLE_HIST_SEQ"
        let pUrl = {
            ORDER_URL: reORDER_URL,
            SLE_HIST_SEQ: insertID
        }
        logger.info('selQuery - procFinalOrderHistory_new - UPDATE THISTORY_ORDER_LARGE - ORDER_URL: ' + reORDER_URL + ' , insertID: ' + insertID)
        sync.parallel(function () {
            ssoDB.excuteQueryUpDel(qUrl, pUrl, true, sync.defers())
        })
        let retUpUrl = sync.await()
        let retUpUrl_err = [].concat.apply([], retUpUrl)[0]
        //let retUpUrl_ret = [].concat.apply([], retUpUrl)[1]
        if (retUpUrl_err) {
            logger.error('selQuery - procFinalOrderHistory_new - update order URL insertID - insertID: ' + insertID + ' - err: ' + retUpUrl_err)
            let sendData = {RESULT: 'ERROR'}
            let pubData = {data: sendData, workType: workType, csid: csid}
            redis.pubRedis(CONSTS_SYS.PUB_CH.RES_ORDER + CNAME, pubData)
            return
        }


        ////////////////////////////////////
        // check address...
        logger.debug('selQuery - procFinalOrderHistory_new - ADDR_TYPE: ' + ADDR_TYPE + ' - MRHST_SEQ: ' + MRHST_SEQ + ' , CSTMR_TEL_NO: ' + CSTMR_TEL_NO + ' , DLVR_AREA: ' + DLVR_AREA + ' , DLVR_DETAIL_AREA: ' + DLVR_DETAIL_AREA)

        if (ADDR_TYPE === 'I')  // insert address info...
        {
            sync.parallel(function () {
                insertAddress_TTINFO_CUSTOMER(MRHST_SEQ, CSTMR_TEL_NO, DLVR_AREA, DLVR_DETAIL_AREA, MEMO, SI, GU, DONG, LI, BUNJI, ROAD_NM, BULD_NM, CRDNT_X, CRDNT_Y, BFE_X, BFE_Y, ADRES_TYPE, sync.defers())
                //insertAddress_TTINFO_CUSTOMER(MRHST_SEQ, CSTMR_TEL_NO, DLVR_AREA, DLVR_DETAIL_AREA, MEMO, SI, GU, DONG, LI, BUNJI, ROAD_NM, BULD_NM, CRDNT_X, CRDNT_Y, ADRES_TYPE, sync.defers())
            })
            let retAddrIn = sync.await()
            let retAddrIn_err = [].concat.apply([], retAddrIn)[0]
            let retAddrIn_ret = [].concat.apply([], retAddrIn)[1]
            if (retAddrIn_err) {
                logger.error('selQuery - procFinalOrderHistory_new - insertCustomerAddressInfo - MRHST_SEQ: ' + MRHST_SEQ + ' - err: ' + retAddrIn_err, CSTMR_TEL_NO)
            }
            // else {
            //     logger.info('selQuery - procFinalOrderHistory_new - insertCustomerAddressInfo - MRHST_SEQ: ' + MRHST_SEQ, CSTMR_TEL_NO)
            // }
            else {
                if(retAddrIn_ret === 'DUP'){
                    sync.parallel(function () {
                        updateAddress_TTINFO_CUSTOMER(MRHST_SEQ, CSTMR_TEL_NO, DLVR_AREA, DLVR_DETAIL_AREA, MEMO, SI, GU, DONG, LI, BUNJI, ROAD_NM, BULD_NM, CRDNT_X, CRDNT_Y, BFE_X, BFE_Y, ADRES_TYPE, sync.defers())
                        //updateAddress_TTINFO_CUSTOMER(MRHST_SEQ, CSTMR_TEL_NO, DLVR_AREA, DLVR_DETAIL_AREA, MEMO, SI, GU, DONG, LI, BUNJI, ROAD_NM, BULD_NM, CRDNT_X, CRDNT_Y, ADRES_TYPE, sync.defers())
                    })
                    let retAddrInUp = sync.await()
                    let retAddrInUp_err = [].concat.apply([], retAddrInUp)[0]
                    //let retAddrInUp_ret = [].concat.apply([], retAddrInUp)[1]
                    if (retAddrInUp_err) {
                        logger.error('selQuery - procFinalOrderHistory_new - insertCustomerAddressInfo - DUP - updateTINFO - MRHST_SEQ: ' + MRHST_SEQ + ' - err: ' + retAddrInUp_err, CSTMR_TEL_NO)
                    }
                    else {
                        logger.info('selQuery - procFinalOrderHistory_new - insertCustomerAddressInfo - DUP - updateTINFO - MRHST_SEQ: ' + MRHST_SEQ, CSTMR_TEL_NO)
                    }
                }
                else {
                    logger.info('selQuery - procFinalOrderHistory_new - insertCustomerAddressInfo - MRHST_SEQ: ' + MRHST_SEQ, CSTMR_TEL_NO)
                }
            }
        }
        else if (ADDR_TYPE === 'U')  // update address info...
        {
            sync.parallel(function () {
                updateAddress_TTINFO_CUSTOMER(MRHST_SEQ, CSTMR_TEL_NO, DLVR_AREA, DLVR_DETAIL_AREA, MEMO, SI, GU, DONG, LI, BUNJI, ROAD_NM, BULD_NM, CRDNT_X, CRDNT_Y, BFE_X, BFE_Y, ADRES_TYPE, sync.defers())
                //updateAddress_TTINFO_CUSTOMER(MRHST_SEQ, CSTMR_TEL_NO, DLVR_AREA, DLVR_DETAIL_AREA, MEMO, SI, GU, DONG, LI, BUNJI, ROAD_NM, BULD_NM, CRDNT_X, CRDNT_Y, ADRES_TYPE, sync.defers())
            })
            let retAddrUp = sync.await()
            let retAddrUp_err = [].concat.apply([], retAddrUp)[0]
            //let retAddrUp_ret = [].concat.apply([], retAddrUp)[1]
            if (retAddrUp_err) {
                logger.error('selQuery - procFinalOrderHistory_new - updateCustomerAddressInfo - MRHST_SEQ: ' + MRHST_SEQ + ' - err: ' + retAddrUp_err, CSTMR_TEL_NO)
            }
            else {
                logger.info('selQuery - procFinalOrderHistory_new - updateCustomerAddressInfo - MRHST_SEQ: ' + MRHST_SEQ, CSTMR_TEL_NO)
            }
        }
        else if (ADDR_TYPE === 'P')  // update address info... shop pick up...
        {
            logger.info('selQuery - procFinalOrderHistory_new - ADDR_TYPE: P - No action customer address info...')
        }  // end of (ADDR_TYPE === 'P')

        else if (ADDR_TYPE === 'N') {  // No action...
            logger.info('selQuery - procFinalOrderHistory_new - ADDR_TYPE: N - No action customer address info...')
        }


        /////////////////////////////////////////
        // select order cancel time...
        sync.parallel(function () {
            selectOrderCancelTime(MRHST_SEQ, sync.defers())
        })
        let retCncl = sync.await()
        let retCncl_err = [].concat.apply([], retCncl)[0]
        let retCncl_ret = [].concat.apply([], retCncl)[1]
        let retCancelTime = ''
        if (retCncl_err) {
            logger.error('selQuery - procFinalOrderHistory_new - order cancel time - err: ' + retCncl_err)
        }
        else {
            retCancelTime = retCncl_ret
        }
        logger.info('selQuery - procFinalOrderHistory_new - end order - MRHST_SEQ: ' + MRHST_SEQ + ' , insertID: ' + insertID + ' , retCancelTime: ' + retCancelTime, CSTMR_TEL_NO)


        ////////////////////////////
        // 배달료 insert...
        if (retDlvr_ret && retDlvr_ret.length > 0) {
            if (retDlvr_ret[0].DLVR_AMOUNT_INCLS_AT === 'A' && arrLargeParams.ADDR_TYPE !== 'P') {
                sync.parallel(function () {
                    insertDeliveryAmount(insertID, dlvrSAME_ORDER_GOODS_SEQ, retDlvr_ret, sync.defers())
                })
                let retDlvrAmnt = sync.await()
                let retDlvrAmnt_err = [].concat.apply([], retDlvrAmnt)[0]
                //let retDlvrAmnt_ret = [].concat.apply([], retDlvrAmnt)[1]
                if (retDlvrAmnt_err) {
                    logger.error('selQuery - procFinalOrderHistory_new - insert delevery amount - insertID: ' + insertID + ' - err: ' + retDlvrAmnt_err)
                }
                doFinalOrderToShop(MRHST_SEQ, posCode, shopNumber, insertID, csid, workType, retCancelTime, CNAME)
            }
            else {
                doFinalOrderToShop(MRHST_SEQ, posCode, shopNumber, insertID, csid, workType, retCancelTime, CNAME)
            }
        }
        else{
            doFinalOrderToShop(MRHST_SEQ, posCode, shopNumber, insertID, csid, workType, retCancelTime, CNAME)
        }
    })  // end of sync.fiber()
}  // end of procFinalOrderHistory()



let doSort_MENU_GOODS_AT = (a, b) => {
    if (a.MENU_GOODS_AT === b.MENU_GOODS_AT) {
        return 0
    }
    return a.MENU_GOODS_AT < b.MENU_GOODS_AT ? 1 : -1
}
let doSort_SAME_ORDER_GOODS_SEQ = (a, b) => {
    if (a.SAME_ORDER_GOODS_SEQ === b.SAME_ORDER_GOODS_SEQ) {
        return 0
    }
    return a.SAME_ORDER_GOODS_SEQ > b.SAME_ORDER_GOODS_SEQ ? 1 : -1
}
////////////////////////////////////////////////


let doExcuteQuery = (query, params, cb) => {
    ssoDB.excuteQuerySelect(query, params, false, (err, rows) => {
        if (err) {
            logger.error('selQuery - doExcuteQuery - err: ' + err)
            cb(err)
            return
        }

        cb(null, rows)
    })
}


//////////////////////////////////////
// Insert Query.....
exports.procInsertExecuteQuery = (queryGCode, params, cb) => {
    logger.debug('selQuery - procInsertExecuteQuery - queryGCode: ' + queryGCode + ' - params: ' + JSON.stringify(params))

    let staticQuery = 'SELECT QUERY_CODE, QUERY_STR '
        + 'FROM twv_query_group AS QG '
        + 'LEFT JOIN twv_query_grouping AS QGP ON QGP.QUERY_GROUP_SEQ = QG.SEQ '
        + 'LEFT JOIN twv_query AS Q ON Q.SEQ = QGP.QUERY_SEQ '
        + 'WHERE QG.QUERY_GROUP_CODE = "' + queryGCode + '"'

    ssoDB.excuteQuerySelect(staticQuery, '', false, (err, resQuerys) => {
        if (err) {
            logger.error('selQuery - procInsertExecuteQuery - err: ' + err)
            cb(err)
            return
        }

        let insertQuery = resQuerys[0].QUERY_STR

        // insert...
        ssoDB.excuteQueryInsert(insertQuery, params, false, (err, result) => {
            if (err) {
                logger.error('selQuery - procInsertExecuteQuery - params: ' + JSON.stringify(params) + ' , err: ' + err)
                cb(err)
                return
            }

            let insertID = result.insertId
            logger.info('selQuery - procInsertExecuteQuery - params: ' + JSON.stringify(params) + ' , insertID :   ' + insertID + ' - affectedRows: ' + result.affectedRows)
            cb(null, insertID)
        })
    })
}


//////////////// succccccccccccc eeeeeeeeeeeee /////////////////


let selectGroupCodeStaticQuery = (queryGCode) => {
    let query
    switch (queryGCode) {
        case 'SEL_QGCODE' :
            query = 'SELECT QUERY_CODE, QUERY_STR '
                + 'FROM twv_query_group AS QG '
                + 'LEFT JOIN twv_query_grouping AS QGP ON QGP.QUERY_GROUP_SEQ = QG.SEQ '
                + 'LEFT JOIN twv_query AS Q ON Q.SEQ = QGP.QUERY_SEQ '
                + 'WHERE QG.QUERY_GROUP_CODE = :QUERY_GROUP_CODE '
            break

        default:
            query = ''
    }

    return query
}


let doSingleCodeExcute = (code, params, cb) => {
    findQuery(code, (err, query) => {
        if (err) {
            logger.error("selQuery - doSingleCodeExcute - err: " + err)
            cb(err)
            return
        }

        if (!query) {
            logger.error('selQuery - doSingleCodeExcute - no query...!!! code: ' + code)
            cb('no query')
            return
        }

        let p = {}

        ssoDB.excuteQuerySelect(query, p, false, (err, rows) => {
            if (err) {
                logger.error('selQuery - doSingleCodeExcute2 - err: ' + err)
                cb(err)
                return
            }

            cb(null, {[code]: rows})
        })
    })
}

///////////////////////////////////////////////////////
// query group code를 이용해서 쿼리 조회
// 그룹쿼리코드가 고정된 쿼리인지 동적 쿼리인지 확인...
let procFindQueryGCode = (queryGCode, cb) => {

    /*ovsDB.excuteQuery(selectQuery('SEL_GQUERYS'), p, function (err, rows) {
        if (err) {
            logger.error('selQuery - procFindQueryGCode - err: ' + err)
            cb(err)
            return
        }
        cb(null, rows)
    })*/
}


//////////////////////////////////////////////////////////////
// 쿼리 그룹 코드를 이용해서 쿼리를 실행한 후 결과 리턴...
let doExecuteQueryGroupCode = (qGCode, params, cb) => {
    // 쿼리 찾기
    let query = selectGroupCodeStaticQuery('SEL_GCODE')
    if (query) {
        this.procExecuteQuery(query, {QUERY_GROUP_CODE: qGCode}, (err, res) => {
            if (err) {
                logger.error('selQuery - doExecuteQueryGroupCode - err: ' + err)
                cb(err)
                return
            }

            cb(null, res)
            return
        })

        return
    }

    cb(null, 'no static')
}


///////////////////////////////////////////////////
// 쿼리 그룹 코드를 이용해서 쿼리를 실행...
exports.executeQueryUsingQueryGCode = (queryGCode, params, cb) => {
    doExecuteQueryGroupCode(queryGCode, params, (err, res) => {
        if (err) {
            logger.error('selQuery - executeQueryUsingQueryGCode - err: ' + err)
            cb(err)
            return
        }

        cb(null, res)
    })
}


exports.executeQueryUsingCodesARR = (queryCodes, params, cb) => {
    doMultiCodeExcute(queryCodes, params, (err, res) => {
        if (err) {
            logger.error("selQuery - executeQueryUsingCodes2 - err: " + err)
            cb(err)
            return
        }

        cb(null, res)
    })
}


exports.executeQueryUsingCodes = (queryCodes, params, cb) => {
    if (typeof (queryCodes) === 'string') {
        doSingleCodeExcute(queryCodes, params, (err, res) => {
            if (err) {
                logger.error("selQuery - executeQueryUsingCodes1 - err: " + err)
                cb(err)
                return
            }

            cb(null, res)
        })
    }
    else {
        doMultiCodeExcute(queryCodes, params, (err, res) => {
            if (err) {
                logger.error("selQuery - executeQueryUsingCodes2 - err: " + err)
                cb(err)
                return
            }

            cb(null, res)
        })
    }
}


//////////////////////////////////////////////////////////////////
// 여러 쿼리를 이용해서 멀티 조회
let procMultiQueryExcute = (arrQCode, arrQuery, params, cb) => {
    sync.fiber(function () {
        sync.parallel(function () {
            for (let i = 0; i < arrQuery.length; i++) {
                let p = {}
                ssoDB.excuteQuerySelect(arrQuery[i], p, true, sync.defers())
            }
        })
        let resData = sync.await()
        resData = [].concat.apply([], resData)

        let arrResults = {}
        for (let j = 0; j < arrQCode.length; j++) {
            arrResults[arrQCode[j]] = resData[j]
        }

        cb(null, arrResults)
    })
}


let doMultiCodeExcute = (codes, params, cb) => {
    sync.fiber(function () {
        sync.parallel(function () {
            for (let i = 0; i < codes.length; i++) {
                findQuery(codes[i], sync.defers())
            }
        })
        // returns [[1,2,3],[4,5,6]]
        let arrQuery = sync.await()
        // concat to get [1,2,3,4,5,6]
        arrQuery = [].concat.apply([], arrQuery)

        /////////////////////////////
        sync.fiber(function () {
            sync.parallel(function () {
                for (let i = 0; i < arrQuery.length; i++) {
                    let p = {}
                    ssoDB.excuteQuerySelect(arrQuery[i], p, true, sync.defers())
                }
            })
            let resData = sync.await()
            resData = [].concat.apply([], resData)

            let arrResults = {}
            for (let j = 0; j < codes.length; j++) {
                arrResults[codes[j]] = resData[j]
            }

            cb(null, arrResults)
        })
    })
}


let findQuery = (code, cb) => {
    // STORE_BASIC_DATA , STORE_MENT
    let query
    switch (code) {
        case 'STORE_BASIC_DATA':  //used... 시작할 때...
            query = 'SELECT BRCH.MRHST_SEQ, BRCH.FRNCHS_SEQ, BRCH.MRHST_NM, BRCH.BIZRNO ' +
                ', CAST(AES_DECRYPT(UNHEX(BRCH.REPRSNT_TEL_NO), "$DlShQpDlTus$RkAodWjaAoWkd&)(*&^") AS CHAR) AS REPRSNT_TEL_NO ' +
                ', CAST(AES_DECRYPT(UNHEX(BRCH.MOBLPHON_NO), "$DlShQpDlTus$RkAodWjaAoWkd&)(*&^") AS CHAR) AS MOBLPHON_NO ' +
                ', CAST(AES_DECRYPT(UNHEX(BRCH.ADRES), "$DlShQpDlTus$RkAodWjaAoWkd&)(*&^") AS CHAR) AS ADRES ' +
                ', CAST(AES_DECRYPT(UNHEX(BRCH.EMAIL), "$DlShQpDlTus$RkAodWjaAoWkd&)(*&^") AS CHAR) AS EMAIL ' +
                ', CAST(AES_DECRYPT(UNHEX(STT.RCV_TEL_NO), "$DlShQpDlTus$RkAodWjaAoWkd&)(*&^") AS CHAR) AS RCV_TEL_NO ' +
                ', STT.RESVE_RCEPT_USE_AT ' +
                ', STT.RESVE_RCEPT_POSBL_HM ' +
                ', STT.BSN_PRPARE_HM ' +
                ', get_mrhst_stus_search_type(BRCH.MRHST_SEQ) AS POS_CODE ' +
                'FROM tinfo_branch_store AS BRCH ' +
                'LEFT JOIN tinfo_branch_store_state AS STT ON STT.MRHST_SEQ = BRCH.MRHST_SEQ ' +
                'WHERE BRCH.SVC_STTUS_CODE = "100001" '
            break

        case 'STORE_MENT':  //used... 시작할 때...
            query = 'SELECT BRCH.MRHST_SEQ, BRCH.FRNCHS_SEQ, BRCH.MRHST_NM' +
                ', CAST(AES_DECRYPT(UNHEX(STT.RCV_TEL_NO), "$DlShQpDlTus$RkAodWjaAoWkd&)(*&^") AS CHAR) AS RCV_TEL_NO' +
                ', TM.BRANCH_STORE_NAME' +
                ', TM.MENT ' +
                ', STT.RESVE_RCEPT_USE_AT ' +
                ', STT.RESVE_RCEPT_POSBL_HM ' +
                ', STT.BSN_PRPARE_HM ' +
                'FROM tinfo_branch_store AS BRCH ' +
                'LEFT JOIN tinfo_branch_store_state AS STT ON STT.MRHST_SEQ = BRCH.MRHST_SEQ ' +
                'LEFT JOIN twv_ment AS TM ON TM.SEQ = BRCH.MRHST_SEQ ' +
                'WHERE BRCH.SVC_STTUS_CODE = "100001" '
            break

        case 'SEL_QGCODE' :
            query = 'SELECT QG.QUERY_GROUP_CODE, Q.QUERY_CODE, Q.QUERY_STR '
                + 'FROM twv_query_group AS QG '
                + 'LEFT JOIN twv_query_grouping AS QGPNG ON QGPNG.QUERY_GROUP_SEQ = QG.SEQ '
                + 'LEFT JOIN twv_query AS Q ON Q.SEQ = QGPNG.QUERY_SEQ '
                + 'WHERE QG.QUERY_GROUP_CODE = :QUERY_GROUP_CODE '
            break

        case 'BEST_MENU':
            query = 'select menuL.LCLAS_CODE, menuL.LCLAS_NM, menuL.LCLAS_DC ' +
                ' , menuSsle.SLE_AMOUNT, menuSsle.SLE_UNIT, menuSsle.SLE_UNIT_DC ' +
                '       , menuS.SCLAS_CODE, menuS.SCLAS_NM, menuS.SCLAS_DC, menuS.ADIT_GOODS_COMPOSITION_AT, menuS.SCLAS_SORT, menuS.INVNTRY_AT ' +
                ' , menuS.BEST_AT, menuS.RECOMMEND_AT, menuS.SINGL_CMPND_AT, menuS.INDPNDNC_ORDER_AT, menuS.REPRSNT_GOODS_AT, menuS.REPRSNT_GOODS_SORT ' +
                'FROM tbranch_menu_large as menuL ' +
                'JOIN tbranch_menu_small as menuS on menuL.MRHST_SEQ = menuS.MRHST_SEQ ' +
                'AND menuL.LCLAS_CODE = menuS.LCLAS_CODE ' +
                'AND menuL.MRHST_SEQ = 1 ' +
                'AND menuL.USE_AT = menuS.USE_AT and menuL.USE_AT = "Y" ' +
                'AND menuS.REPRSNT_GOODS_AT = "Y" ' +
                'LEFT JOIN tbranch_menu_small_price as menuSsle on menuSsle.SCLAS_CODE = menuS.SCLAS_CODE ' +
                'ORDER BY menuL.LCLAS_SORT, menuS.SCLAS_SORT'
            break

        case 'MENU_COMBO':
            query = 'SELECT menuL.LCLAS_CODE, menuL.LCLAS_NM, menuL.LCLAS_DC ' +
                ' , menuSsle.SLE_AMOUNT, menuSsle.SLE_UNIT, menuSsle.SLE_UNIT_DC ' +
                '       , menuS.SCLAS_CODE, menuS.SCLAS_NM, menuS.SCLAS_DC, menuS.ADIT_GOODS_COMPOSITION_AT, menuS.SCLAS_SORT, menuS.INVNTRY_AT ' +
                ' , menuS.BEST_AT, menuS.RECOMMEND_AT, menuS.SINGL_CMPND_AT, menuS.INDPNDNC_ORDER_AT, menuS.REPRSNT_GOODS_AT, menuS.REPRSNT_GOODS_SORT ' +
                'FROM tbranch_menu_large as menuL ' +
                'JOIN tbranch_menu_small as menuS on menuL.MRHST_SEQ = menuS.MRHST_SEQ ' +
                'AND menuL.LCLAS_CODE = menuS.LCLAS_CODE  ' +
                'AND menuL.MRHST_SEQ = 1 ' +
                'AND menuL.USE_AT = menuS.USE_AT and menuL.USE_AT = "Y"  ' +
                'AND menuS.REPRSNT_GOODS_AT = "Y" ' +
                'LEFT JOIN tbranch_menu_small_price as menuSsle on menuSsle.SCLAS_CODE = menuS.SCLAS_CODE ' +
                'ORDER BY menuL.LCLAS_SORT, menuS.SCLAS_SORT'
            break

        case 'FIRST_SCENARIO_CODE':
            query = 'SELECT scen.SCEN_CODE, temp.TEMPLATE_FILE_NAME, temp.TEMPLATE_NAME, scen.SCEN_NAME, brch.MRHST_SEQ, CAST(AES_DECRYPT(UNHEX(brch.REPRSNT_TEL_NO), \'$DlShQpDlTus$RkAodWjaAoWkd&)(*&^\') AS CHAR) AS REPRSNT_TEL_NO ' +
                'FROM twv_scenario_info AS scen ' +
                'LEFT JOIN tinfo_branch_store AS brch ON brch.MRHST_SEQ = scen.MRHST_SEQ ' +
                'LEFT JOIN twv_template_info AS temp ON temp.TEMPLATE_SEQ = scen.TEMPLATE_SEQ ' +
                'WHERE SCEN_FIRST = 1'
            break

        case 'CHOICE_MENU':
            query = 'SELECT a1.SCLAS_CODE, a2.SCLAS_NM ' +
                ' , a1.CHOICE_LCLAS_CODE ' +
                ' , b2.LCLAS_NM   CHOICE_LCLAS_NM ' +
                ' , a1.CHOICE_LCLAS_SORT ' +
                ' , b2.LCLAS_DC   CHOICE_LCLAS_DC ' +
                ' , b2.ESSNTL_AT  CHOICE_ESSNTL_AT ' +
                ' , b3.SCLAS_CODE CHOICE_SCLAS_CODE ' +
                ' , b3.SCLAS_NM   CHOICE_SCLAS_NM   ' +
                ' , b3.SCLAS_DC   CHOICE_SCLAS_DC   ' +
                ' , b3.SCLAS_SORT CHOICE_SCLAS_SORT ' +
                ' , b3.INVNTRY_AT CHOICE_INVNTRY_AT ' +
                'FROM tbranch_rel_menu_small_choise_large AS a1 ' +
                'JOIN tbranch_menu_small AS a2 ' +
                'ON  a1.MRHST_SEQ = a2.MRHST_SEQ ' +
                '  AND a1.SCLAS_CODE = a2.SCLAS_CODE ' +
                '  AND a2.USE_AT = "Y" ' +
                'JOIN tbranch_choise_large AS b2 ' +
                '  ON  a1.MRHST_SEQ = b2.MRHST_SEQ ' +
                '  AND a1.CHOICE_LCLAS_CODE = b2.LCLAS_CODE ' +
                '  AND b2.USE_AT = "Y" ' +
                'JOIN tbranch_choise_small AS b3 ' +
                '  ON  b2.MRHST_SEQ = b3.MRHST_SEQ ' +
                '  AND b2.LCLAS_CODE = b3.LCLAS_CODE ' +
                '  AND b3.USE_AT = "Y" ' +
                'WHERE a1.MRHST_SEQ  = 1 ' +
                'AND   a1.SCLAS_CODE = 2 ' +
                'ORDER BY a1.CHOICE_LCLAS_SORT, b3.SCLAS_SORT'
            break

        default:
            query = ''
    }

    cb(null, query)
}


/////////////////////////
/////////////////////////
/////////////////////////
/////////////////////////
//// new ////////////////
//// new ////////////////
//// new ////////////////
//// new ////////////////

let doMultiCodeExcute_new = (queryGroupCode, params, cb) => {
    let findQuery = 'SELECT QG.QUERY_GROUP_CODE , Q.QUERY_CODE, Q.QUERY_STR ' +
        ' FROM tws_query_group_code AS QG ' +
        ' LEFT JOIN tws_query_grouping AS QGPNG ON QGPNG.QUERY_GROUP_SEQ = QG.SEQ ' +
        ' LEFT JOIN tws_query AS Q ON Q.SEQ = QGPNG.QUERY_SEQ ' +
        'WHERE QG.QUERY_GROUP_CODE = "' + queryGroupCode + '"'

    ssoDB.excuteQuerySelect(findQuery, '', false, (err, resQuerys) => {
        if (err) {
            logger.error('selQuery - doMultiCodeExcute_new - queryGroupCode: ' + queryGroupCode + ' - err: ' + err)
            cb(err)
            return
        }

        if (!resQuerys || !resQuerys.length) {
            logger.error('selQuery - doMultiCodeExcute_new - no query group code data... - queryGroupCode: ' + queryGroupCode)
            cb('no query group code data')
            return
        }

        // 쿼리 그룹에 속한 쿼리가 하나일 때...
        if (typeof (resQuerys) === 'string') {
            console.log("쿼리 그룹에 속한 쿼리가 하나일 때...")
            console.log("쿼리 그룹에 속한 쿼리가 하나일 때...")
            console.log("쿼리 그룹에 속한 쿼리가 하나일 때...")
            doExcuteQuery(resQuerys, params, (err, qRes) => {
                if (err) {
                    logger.error("selQuery - doMultiCodeExcute_new - err: " + err)
                    cb(err)
                    return
                }

                let setData = {}
                let pName
                let arrPropName = []
                for (let i = 0; i < qRes.length; i++) {
                    Object.getOwnPropertyNames(qRes[i]).forEach((name) => {
                        switch (name) {
                            case 'QUERY_CODE':
                                pName = qRes[i][name]
                                setData[pName] = {}
                                arrPropName.push(pName)
                                break

                            case 'QUERY_STR':
                                setData[pName] = qRes[i][name]
                                break
                        }
                    })
                }

                cb(null, setData)
            })

            return
        }

        /////////////////////////////////////////////////////////////
        // 여러 쿼리를 이용해서 멀티 조회
        if (!params) {
            params = []
            for (let m = 0; m < resQuerys.length; m++) {
                params.push('')
            }
        }

        sync.fiber(function () {
            sync.parallel(function () {
                for (let i = 0; i < resQuerys.length; i++) {
                    //try {
                    let t = resQuerys[i].QUERY_STR
                    let tq = t.substr(0, 7).toLowerCase()
                    logger.info('selQuery - doMultiCodeExcute_new - queryGroupCode: ' + queryGroupCode + ' - [' + i + '] - query: ' + resQuerys[i].QUERY_STR + ' - params: ' + JSON.stringify(params[i]))
                    switch (tq) {
                        case 'select ':
                            ssoDB.excuteQuerySelect(resQuerys[i].QUERY_STR, params[i], true, sync.defers())
                            break
                        case 'insert ':
                            ssoDB.excuteQueryInsert(resQuerys[i].QUERY_STR, params[i], true, sync.defers())
                            break
                        case 'update ':
                            ssoDB.excuteQueryUpDel(resQuerys[i].QUERY_STR, params[i], true, sync.defers())
                            break
                        case 'delete ':
                            ssoDB.excuteQueryUpDel(resQuerys[i].QUERY_STR, params[i], true, sync.defers())
                            break
                        default:
                            ssoDB.excuteSProcedure(resQuerys[i].QUERY_STR, params[i], true, sync.defers())
                            break
                    }
                    /*}
                    catch (e) {
                        logger.error('selQuery - doMultiCodeExcute_new - sync for - catch e: ' + e)
                        logger.error('selQuery - doMultiCodeExcute_new - resQuerys[' + i + '].QUERY_STR: ' + resQuerys[i].QUERY_STR)
                        logger.error('selQuery - doMultiCodeExcute_new - params[' + i + ']: ' + JSON.stringify(params[i]))

                        cb(e)
                    }*/
                }
            })

            let resData = sync.await()
            //[ [ null, [ [Object] ] ], [ null, [ [Object] ] ] ]


            let oResData = []
            for (let r = 0; r < resData.length; r++) {
                oResData.push(resData[r][1])
            }

            // console.log('@@@@@@@@@@@@@@@@@@@@@@@@@@@@')
            // console.log('@@@@@@@@@@@@@@@@@@@@@@@@@@@@')
            // console.dir(oResData)
            // console.log('@@@@@@@@@@@@@@@@@@@@@@@@@@@@')
            // console.log('@@@@@@@@@@@@@@@@@@@@@@@@@@@@')

            //let resErr = [].concat.apply([], resData)[0]
            //if(resErr){
            //    logger.error('selQuery - doMultiCodeExcute_new - sync for - resErr : ' + resErr)
            //    cb(resErr)
            //    return
            //}

            //let oResData = [].concat.apply([], resData)[1]
            //let oResData = [].concat.apply([], resData)
            let arrResults = {}

            for (let j = 0; j < resQuerys.length; j++) {
                arrResults[resQuerys[j].QUERY_CODE] = oResData[j]

                if (j === (resQuerys.length - 1)) {
                    cb(null, arrResults)
                }
            }
        })
    })
}

exports.executeQueryGroupCode_new = (queryGroupCode, params, cb) => {
    doMultiCodeExcute_new(queryGroupCode, params, (err, res) => {
        if (err) {
            logger.error("selQuery - executeQueryGroupCode_new - err: " + err)
            cb(err)
            return
        }

        cb(null, res)
    })
}
