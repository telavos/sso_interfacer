/*
const mysql = require('mysql')
const CONSTS = require('../CONSTS')
const logger = require('../logger')

const OVSDB_1 = 'OVSDB1'

let clusterConfig, poolCluster


let conf1 = {
    host: CONSTS.DB_INFO.OVS_DB_HOST_1,
    port: CONSTS.DB_INFO.OVS_DB_PORT_1,
    user: CONSTS.DB_INFO.OVS_DB_USER_1,
    password: CONSTS.DB_INFO.OVS_DB_PASSWORD_1,
    database: CONSTS.DB_INFO.OVS_DB_NAME_1,
    connectTimeout: CONSTS.DB_INFO.OVS_DB_CONNECT_TIMEOUT,
    connectionLimit: CONSTS.DB_INFO.OVS_DB_CONNECT_LIMIT
}


let doAddDBinCluster = (clusterName) => {
    poolCluster.remove(clusterName)

    try {
        logger.info('db_single - ADD Cluster - ' + clusterName + '...')
        switch (clusterName) {
            case OVSDB_1:
                poolCluster.add(clusterName, conf1)
                break
        }
    }
    catch (e) {
        logger.error('db_single - doAddDBinCluster - catch - e: ' + e)
    }
}


clusterConfig = {
    removeNodeErrorCount: CONSTS.DB_INFO.OVS_DB_REMOVE_NODE_ERROR_COUNT,
    defaultSelector: 'ORDER'
}

poolCluster = mysql.createPoolCluster(clusterConfig)

doAddDBinCluster(OVSDB_1)

poolCluster.on('remove', function (nodeId) {
    logger.debug('db_single - on remove NODE : ' + nodeId)
})


let connClusterDB = function (cb) {
    poolCluster.getConnection(OVSDB_1, function (err, connection) {
        if (err) {
            cb(err)
            return
        }
        cb(null, connection)
    })
}

let connMariasql = function (q, p, cb) {
    // let encK = CONSTS.Q_ENC_K
    // p = Object.assign(p, encK)
    connClusterDB(function (err, connection) {
        if (err) {
            logger.error('db_single - connMariasql - connClusterDB() - ' + err)
            cb(err)
            return
        }
        connection.config.queryFormat = function (query, values) {
            if (!values) return query
            return query.replace(/\:(\w+)/g, function (txt, key) {
                if (values.hasOwnProperty(key)) {
                    return this.escape(values[key])
                }
                return txt
            }.bind(this))
        }
        connection.query(q, p, function (err, rows) {
            if (err) {
                connection.release()
                logger.error('db_single - connMariasql - query() - ' + err)
                cb(err)
                return
            }
            connection.release()
            cb(null, rows)
        })
    })
}

let connMariasqlInsert = (q, p, cb) => {
    // let encK = CONSTS.Q_ENC_K
    // p = Object.assign(p, encK)
    connClusterDB((err, connection) => {
        if (err) {
            logger.error('db_single - connMariasqlInsert - connClusterDB() - err: ' + err)
            cb(err)
            return
        }
        connection.config.queryFormat = function (query, values) {
            if (!values) return query
            return query.replace(/\:(\w+)/g, function (txt, key) {
                if (values.hasOwnProperty(key)) {
                    return this.escape(values[key])
                }
                return txt
            }.bind(this))
        }

        connection.beginTransaction((err) => {
            if (err) {
                logger.error('index_single - connMariasqlInsert - beginTransaction - err: ' + err)
                connection.rollback(() => {
                    connection.release()
                    cb(err)
                })
                return
            }

            connection.query(q, p, function (err, rows) {
                if (err) {
                    logger.error('index_single - connMariasqlInsert - query - err: ' + err)
                    connection.rollback(() => {
                        connection.release()
                        cb(err)
                    })
                    return
                }

                connection.commit(function (err) {
                    if (err) {
                        logger.error('index_single - connMariasqlInsert - commit - err: ' + err)
                        connection.rollback(() => {
                            connection.release()
                            cb(err)
                        })
                        return
                    }
                    cb(null, rows)
                })
            })
        })
    })
}

let connMariasqlUpDel = function (q, p, cb) {
    // let encK = CONSTS.Q_ENC_K
    // p = Object.assign(p, encK)
    connClusterDB((err, connection) => {
        if (err) {
            logger.error('db_single - connMariasqlUpDel - connClusterDB() - err: ' + err)
            cb(err)
            return
        }
        connection.config.queryFormat = function (query, values) {
            if (!values) return query
            return query.replace(/\:(\w+)/g, function (txt, key) {
                if (values.hasOwnProperty(key)) {
                    return this.escape(values[key])
                }
                return txt
            }.bind(this))
        }

        connection.beginTransaction((err) => {
            if (err) {
                logger.error('index_single - connMariasqlUpDel - beginTransaction - err: ' + err)
                connection.rollback(() => {
                    connection.release()
                    cb(err)
                })
                return
            }

            connection.query(q, p, function (err, rows) {
                if (err) {
                    logger.error('index_single - connMariasqlUpDel - query - err: ' + err)
                    connection.rollback(() => {
                        connection.release()
                        cb(err)
                    })
                    return
                }

                connection.commit(function (err) {
                    if (err) {
                        logger.error('index_single - connMariasqlUpDel - commit - err: ' + err)
                        connection.rollback(() => {
                            connection.release()
                            cb(err)
                        })
                        return
                    }

                    cb(null, rows.affectedRows)
                })
            })
        })
    })
}

let connProc = (sP, arrP, cb) => {
    connClusterDB(function (err, connection) {
        if (err) {
            logger.error('db_single - connProc - connClusterDB() - ' + err)
            cb(err)
            return
        }
        logger.info('db index - connProc - query: ' + sP)
        logger.info(arrP)

        connection.query(sP, arrP, (err, result) => {
            if (err) {
                logger.error('db_single - connProc - query() - ' + err)
                cb(err)
                return
            }

            cb(null, result)
        })
    })
}


let getQueryData = (arr) => {
    if (arr.length === undefined) {
        return []
    }
    else {
        let arrRet = arr.map(function (item) {
            return item
        })

        if (arrRet.length > 0) {
            return arrRet
        }
        else {
            return []
        }
    }
}

exports.excuteQuery = (q, p, cb) => {
    connMariasql(q, p, (err, result) => {
        if (err) {
            cb(err)
        }
        else {
            cb(null, JSON.parse(JSON.stringify(result)))
        }
    })
}

exports.excuteInsert = (q, p, cb) => {
    connMariasqlInsert(q, p, (err, result) => {
        if (err) {
            cb(err)
        }
        else {
            cb(null, result)
        }
    })
}

exports.excuteUpDel = (q, p, cb) => {
    connMariasqlUpDel(q, p, (err, result) => {
        if (err) {
            cb(err)
        }
        else {
            cb(null, result)
        }
    })
}

exports.excuteProc = (sProc, arrParams, cb) => {
    connProc(sProc, arrParams, (err, result) => {
        if (err) {
            cb(err)
        }
        else {
            cb(null, result)
        }
    })
}
*/