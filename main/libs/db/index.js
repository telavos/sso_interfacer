/*
const mysql = require('mysql')
const CONSTS = require('../CONSTS')
const logger = require('../logger')

const OVSDB_1 = 'OVSDB1'
const OVSDB_2 = 'OVSDB2'

let clusterConfig, poolCluster


let conf1 = {
    host: CONSTS.DB_INFO.OVS_DB_HOST_1,
    port: CONSTS.DB_INFO.OVS_DB_PORT_1,
    user: CONSTS.DB_INFO.OVS_DB_USER_1,
    password: CONSTS.DB_INFO.OVS_DB_PASSWORD_1,
    database: CONSTS.DB_INFO.OVS_DB_NAME_1,
    connectTimeout: CONSTS.DB_INFO.OVS_DB_CONNECT_TIMEOUT,
    connectionLimit: CONSTS.DB_INFO.OVS_DB_CONNECT_LIMIT
}
let conf2 = {
    host: CONSTS.DB_INFO.OVS_DB_HOST_2,
    port: CONSTS.DB_INFO.OVS_DB_PORT_2,
    user: CONSTS.DB_INFO.OVS_DB_USER_2,
    password: CONSTS.DB_INFO.OVS_DB_PASSWORD_2,
    database: CONSTS.DB_INFO.OVS_DB_NAME_2,
    connectTimeout: CONSTS.DB_INFO.OVS_DB_CONNECT_TIMEOUT,
    connectionLimit: CONSTS.DB_INFO.OVS_DB_CONNECT_LIMIT
}


let doAddDBinCluster = (clusterName) => {
    poolCluster.remove(clusterName)

    try {
        logger.info('db - ADD Cluster - ' + clusterName + '...')
        switch (clusterName) {
            case OVSDB_1:
                poolCluster.add(clusterName, conf1)
                break

            case OVSDB_2:
                poolCluster.add(clusterName, conf2)
                break
        }
    }
    catch (e) {
        logger.error('db - doAddDBinCluster - catch - e: ' + e)
    }
}


clusterConfig = {
    removeNodeErrorCount: CONSTS.DB_INFO.OVS_DB_REMOVE_NODE_ERROR_COUNT,
    defaultSelector: 'ORDER'
}

poolCluster = mysql.createPoolCluster(clusterConfig)

doAddDBinCluster(OVSDB_1)
doAddDBinCluster(OVSDB_2)

poolCluster.on('remove', function (nodeId) {
    logger.debug('db - on remove NODE : ' + nodeId)
})


let connClusterDB = function (cb) {
    poolCluster.getConnection(OVSDB_1, function (err, connection) {
        if (err) {
            connClusterSlaveDB(function (err2, connection2) {
                if (err2) {
                    cb(err2)
                    return
                }
                cb(null, connection2)
            })
            return
        }
        cb(null, connection)
    })
}
let connClusterSlaveDB = function (cb) {
    poolCluster.getConnection(OVSDB_2, function (err, connection) {
        if (err) {
            logger.error('db - connClusterSlaveDB err: ' + err)
            poolCluster = mysql.createPoolCluster(clusterConfig)
            doAddDBinCluster(OVSDB_1)
            doAddDBinCluster(OVSDB_2)
            cb(err)
            return
        }
        cb(null, connection)
        doAddDBinCluster(OVSDB_1)
    })
}

let connMariasql = function (q, p, cb) {
    // let encK = CONSTS.Q_ENC_K
    // p = Object.assign(p, encK)
    connClusterDB(function (err, connection) {
        if (err) {
            logger.error('db - connMariasql - connClusterDB() - ' + err)
            cb(err)
            return
        }
        connection.config.queryFormat = function (query, values) {
            if (!values) return query
            return query.replace(/\:(\w+)/g, function (txt, key) {
                if (values.hasOwnProperty(key)) {
                    return this.escape(values[key])
                }
                return txt
            }.bind(this))
        }
        connection.query(q, p, function (err, rows) {
            connection.release()
            if (err) {
                logger.error('db - connMariasql - query() - ' + err)
                cb(err)
                return
            }

            cb(null, rows)
        })
    })
}

let connMariasqlInsert = function (q, p, cb) {
    connClusterDB(function (err, connection) {
        if (err) {
            logger.error('db - connMariasqlInsert - connClusterDB() - ' + err)
            cb(err)
            return
        }
        connection.config.queryFormat = function (query, values) {
            if (!values) return query
            return query.replace(/\:(\w+)/g, function (txt, key) {
                if (values.hasOwnProperty(key)) {
                    return this.escape(values[key])
                }
                return txt
            }.bind(this))
        }

        connection.query(q, p, function (err, rows) {
            connection.release()
            if (err) {
                logger.error('db - connMariasqlInsert - query() - ' + err)
                cb(err)
                return
            }

            cb(null, rows.insertId)
        })
    })
}

let connMariasqlUpDel = function (q, p, cb) {
    connClusterDB(function (err, connection) {
        if (err) {
            logger.error('db - connMariasqlUpDel - connClusterDB() - ' + err)
            cb(err)
            return
        }
        connection.config.queryFormat = function (query, values) {
            if (!values) return query
            return query.replace(/\:(\w+)/g, function (txt, key) {
                if (values.hasOwnProperty(key)) {
                    return this.escape(values[key])
                }
                return txt
            }.bind(this))
        }

        connection.query(q, p, function (err, rows) {
            connection.release()
            if (err) {
                logger.error('db - connMariasqlUpDel - query() - ' + err)
                cb(err)
                return
            }

            cb(null, rows.affectedRows)
        })
    })
}

let connProc = (sP, arrP, cb) => {
    connClusterDB(function (err, connection) {
        if (err) {
            logger.error('db - connProc - connClusterDB() - ' + err)
            cb(err)
            return
        }
        logger.info('db index - connProc - query: ' + sP)
        logger.info(arrP)

        connection.query(sP, arrP, (err, result) => {
            if (err) {
                logger.error('db - connProc - query() - ' + err)
                cb(err)
                return
            }

            cb(null, result)
        })
    })
}


let getQueryData = (arr) => {
    if (arr.length === undefined) {
        return []
    }
    else {
        let arrRet = arr.map(function (item) {
            return item
        })

        if (arrRet.length > 0) {
            return arrRet
        }
        else {
            return []
        }
    }
}

exports.excuteQuery = (q, p, cb) => {
    connMariasql(q, p, (err, result) => {
        if (err) {
            cb(err)
        }
        else {
            cb(null, JSON.parse(JSON.stringify(result)))
        }
    })
}

exports.excuteInsert = (q, p, cb) => {
    connMariasqlInsert(q, p, (err, result) => {
        if (err) {
            cb(err)
        }
        else {
            cb(null, result)
        }
    })
}

exports.excuteUpDel = (q, p, cb) => {
    connMariasqlUpDel(q, p, (err, result) => {
        if (err) {
            cb(err)
        }
        else {
            cb(null, result)
        }
    })
}

exports.excuteProc = (sProc, arrParams, cb) => {
    connProc(sProc, arrParams, (err, result) => {
        if (err) {
            cb(err)
        }
        else {
            cb(null, result)
        }
    })
}
*/