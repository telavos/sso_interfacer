const mysql = require('mysql')
const CONSTS = require('../CONSTS')
const logger = require('../logger')


let pool = mysql.createPool({
    host: CONSTS.DB_INFO.OVS_DB_HOST_1,
    port: CONSTS.DB_INFO.OVS_DB_PORT_1,
    user: CONSTS.DB_INFO.OVS_DB_USER_1,
    password: CONSTS.DB_INFO.OVS_DB_PASSWORD_1,
    database: CONSTS.DB_INFO.OVS_DB_NAME_1,
    connectTimeout: CONSTS.DB_INFO.OVS_DB_CONNECT_TIMEOUT,
    connectionLimit: CONSTS.DB_INFO.OVS_DB_CONNECT_LIMIT,
    waitForConnections: false
})


let doSelect = function (q, p, cb) {
    // let encK = CONSTS.Q_ENC_K
    // p = Object.assign(p, encK)
    pool.getConnection(function(err, connection){
        if(err){
            connection.release()
            logger.error('ssoDB - doSelect - q: ' + q + ' - p: ' + JSON.stringify(p) + ' - err: ' + err)
            cb(err)
            return
        }

        connection.config.queryFormat = function (query, values) {
            if (!values) return query
            return query.replace(/\:(\w+)/g, function (txt, key) {
                if (values.hasOwnProperty(key)) {
                    return this.escape(values[key])
                }
                return txt
            }.bind(this))
        }
        connection.query(q, p, function (err, rows) {
            if (err) {
                connection.release()
                logger.error('ssoDB - connMariasql - query() - ' + err)
                cb(err)
                return
            }
            cb(null, rows)
            connection.release()
        })
    })
}


let doInsert = (q, p, cb) => {
    // let encK = CONSTS.Q_ENC_K
    // p = Object.assign(p, encK)
    pool.getConnection(function(err, connection) {
        if (err) {
            logger.error('ssoDB - doInsert - q: ' + q + ' - p: ' + JSON.stringify(p) + ' - err: ' + err)
            cb(err)
            return
        }

        connection.config.queryFormat = function (query, values) {
            if (!values) return query
            return query.replace(/\:(\w+)/g, function (txt, key) {
                if (values.hasOwnProperty(key)) {
                    return this.escape(values[key])
                }
                return txt
            }.bind(this))
        }

        connection.beginTransaction((err) => {
            if (err) {
                logger.error('ssoDB - doInsert - beginTransaction - err: ' + err)
                connection.rollback(() => {
                    connection.release()
                    cb(err)
                })
                return
            }

            connection.query(q, p, function (err, rows) {
                if (err) {
                    logger.error('ssoDB - doInsert - query - q: ' + q + ' \n- p: ' + JSON.stringify(p) + ' - err: ' + err)
                    connection.rollback(() => {
                        connection.release()
                        cb(err)
                    })
                    return
                }

                connection.commit(function (err) {
                    if (err) {
                        logger.error('ssoDB - doInsert - commit - err: ' + err)
                        connection.rollback(() => {
                            connection.release()
                            cb(err)
                        })
                        return
                    }

                    /*OkPacket {
                        fieldCount: 0,
                        affectedRows: 1,
                        insertId: 0,
                        serverStatus: 3,
                        warningCount: 0,
                        message: '',
                        protocol41: true,
                        changedRows: 0 }*/
                    cb(null, rows)
                    connection.release()
                })
            })
        })
    })
}


let doUpDel = function (q, p, cb) {
    // let encK = CONSTS.Q_ENC_K
    // p = Object.assign(p, encK)
    pool.getConnection(function(err, connection) {
        if (err) {
            logger.error('ssoDB - doUpDel - q: ' + q + ' - p: ' + JSON.stringify(p) + ' - err: ' + err)
            cb(err)
            return
        }

        connection.config.queryFormat = function (query, values) {
            if (!values) return query
            return query.replace(/\:(\w+)/g, function (txt, key) {
                if (values.hasOwnProperty(key)) {
                    return this.escape(values[key])
                }
                return txt
            }.bind(this))
        }

        connection.beginTransaction((err) => {
            if (err) {
                logger.error('ssoDB - doUpDel - beginTransaction - err: ' + err)
                connection.rollback(() => {
                    connection.release()
                    cb(err)
                })
                return
            }

            connection.query(q, p, function (err, rows) {
                if (err) {
                    logger.error('ssoDB - doUpDel - query - q: ' + q + ' - p: ' + JSON.stringify(p) + ' - err: ' + err)
                    connection.rollback(() => {
                        connection.release()
                        cb(err)
                    })
                    return
                }

                connection.commit(function (err) {
                    if (err) {
                        logger.error('ssoDB - doUpDel - commit - err: ' + err)
                        connection.rollback(() => {
                            connection.release()
                            cb(err)
                        })
                        return
                    }
                    /*OkPacket {
                        fieldCount: 0,
                        affectedRows: 1,
                        insertId: 0,
                        serverStatus: 3,
                        warningCount: 0,
                        message: '',
                        protocol41: true,
                        changedRows: 0 }*/
                    cb(null, rows)
                    connection.release()
                })
            })
        })
    })

}

let doStoredProcedure = (sP, arrP, cb) => {
    let q = ''

    switch (sP){
        case 'sp_cre_thistory_telephone' :
            q = 'CALL sp_cre_thistory_telephone('+arrP.MRHST_SEQ+', "'+arrP.SHOP_NUMBER+'"' +
                 '        , HEX(AES_ENCRYPT("'+arrP.USER_MDN+'", "^WjsGhkDlFur$DlShQpDlTus&)(*&^%$"))\n' +
                 '        , "100002", "100003", "'+arrP.CSID+'"\n' +
                 '        ,"$DlShQpDlTus!TjQlTmDnsDudWk&)(*&"\n' +
                 '        , "^WjsGhkDlFur$DlShQpDlTus&)(*&^%$"\n' +
                 '        , @OUT);'
            break;
    }

    if(!q){
        logger.warning('ssoDB - doStoredProcedure - no select query - sp: ' + sP)
        cb(null, [])
        return
    }

    pool.getConnection(function(err, connection) {
        if (err) {
            logger.error('ssoDB - doStoredProcedure - q: ' + q + ' - p: ' + JSON.stringify(arrP) + ' - err: ' + err)
            cb(err)
            return
        }

        connection.query(q, {}, (err, result) => {
            if (err) {
                connection.release()
                logger.error('ssoDB - doStoredProcedure - query() - sp: ' + sP + ' , arrP: ' + JSON.stringify(arrP) + ' - err: ' + err)
                cb(err)
                return
            }

            cb(null, result)
            connection.release()
        })
    })
}

//
// let getQueryData = (arr) => {
//     if (arr.length === undefined) {
//         return []
//     }
//     else {
//         let arrRet = arr.map(function (item) {
//             return item
//         })
//
//         if (arrRet.length > 0) {
//             return arrRet
//         }
//         else {
//             return []
//         }
//     }
// }

exports.excuteQuerySelect = (q, p, bSync, cb) => {
    doSelect(q, p, (err, result) => {
        if (err) {
            bSync ? cb(null, err) : cb(err)
            return
        }

        bSync ? cb(null, null, JSON.parse(JSON.stringify(result))) : cb(null, JSON.parse(JSON.stringify(result)))
    })
}

exports.excuteQueryInsert = (q, p, bSync, cb) => {
    doInsert(q, p, (err, result) => {
        if (err) {
            bSync ? cb(null, err) : cb(err)
            return
        }

        bSync ? cb(null, null, result) : cb(null, result)
    })
}

exports.excuteQueryUpDel = (q, p, bSync, cb) => {
    doUpDel(q, p, (err, result) => {
        if (err) {
            bSync ? cb(null, err) : cb(err)
            return
        }

        bSync ? cb(null, null, result) : cb(null, result)
    })
}

exports.excuteSProcedure = (sProc, arrParams, bSync, cb) => {
    doStoredProcedure(sProc, arrParams, (err, result) => {
        if (err) {
            bSync ? cb(null, err) : cb(err)
        }
        else {
            bSync ? cb(null, null, result) : cb(null, result)
        }
    })
}
