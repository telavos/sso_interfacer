require('dotenv').config()

let LOG_LEVEL = process.env.LOG_LEVEL || 'debug'
let LOG_FILE_NAME = process.env.LOG_FILE_NAME || 'ovs_IF'
let LOG_STORAGE_AGE = chk_age(process.env.LOG_STORAGE_AGE || 10)
let LOG_MAX_FILE = chkIntString(process.env.LOG_MAX_FILE || 200)
let LOG_MAX_SIZE = convert_mb(process.env.LOG_MAX_SIZE || 10)
let LOG_LOGGING_MDN = process.env.LOG_LOGGING_MDN || 'Y'

let OVS_DB_TIME = chkIntString(process.env.OVS_DB_TIME || 3000)


//////////////////////////////////////////////////////////////////
//// env...

let NODE_ENV = process.env.NODE_ENV || ''

let IF_NAME = process.env.IF_NAME || 'IF_A'

let IF_CONTENTS_PORT = chkIntString(process.env.IF_CONTENTS_PORT || 55000)
let IF_PING_PORT = chkIntString(process.env.IF_PING_PORT || 55100)

let OVS_DB_HOST_1 = process.env.OVS_DB_HOST_1 || ''
let OVS_DB_PORT_1 = chkIntString(process.env.OVS_DB_PORT_1 || 0)
let OVS_DB_NAME_1 = process.env.OVS_DB_NAME_1 || ''
let OVS_DB_USER_1 = process.env.OVS_DB_USER_1 || ''
let OVS_DB_PASSWORD_1 = process.env.OVS_DB_PASSWORD_1 || ''

let OVS_DB_HOST_2 = process.env.OVS_DB_HOST_2 || ''
let OVS_DB_PORT_2 = chkIntString(process.env.OVS_DB_PORT_2 || 0)
let OVS_DB_NAME_2 = process.env.OVS_DB_NAME_2 || ''
let OVS_DB_USER_2 = process.env.OVS_DB_USER_2 || ''
let OVS_DB_PASSWORD_2 = process.env.OVS_DB_PASSWORD_2 || ''

let OVS_DB_CONNECT_TIMEOUT = chkIntString(process.env.OVS_DB_CONNECT_TIMEOUT || 10000)
let OVS_DB_CONNECT_LIMIT = chkIntString(process.env.OVS_DB_CONNECT_LIMIT || 20)
let OVS_DB_REMOVE_NODE_ERROR_COUNT = chkIntString(process.env.OVS_DB_REMOVE_NODE_ERROR_COUNT || 3)


let R_IP_MASTER = process.env.R_IP_MASTER || ''
let R_PORT_MASTER = chkIntString(process.env.R_PORT_MASTER || 0)
let R_IP_MONITOR_A = process.env.R_IP_MONITOR_A || ''
let R_PORT_MONITOR_A = chkIntString(process.env.R_PORT_MONITOR_A || 0)
let R_P = process.env.R_P || ''

let OVS_SHOP_URL = process.env.OVS_SHOP_URL || ''
let OVS_SHOP_URL_ID = process.env.OVS_SHOP_URL_ID || ''
let OVS_SHOP_URL_PW = process.env.OVS_SHOP_URL_PW || ''
let OVS_ORDER_CHK_URL = process.env.OVS_ORDER_CHK_URL || ''

let FUSE_SHOP_STATUS_URL = process.env.FUSE_SHOP_STATUS_URL || ''
let FUSE_SHOP_STATUS_URL_ID = process.env.FUSE_SHOP_STATUS_URL_ID || ''
let FUSE_SHOP_STATUS_URL_PW = process.env.FUSE_SHOP_STATUS_URL_PW || ''
let FUSE_ORDER_URL = process.env.FUSE_ORDER_URL || ''
let FUSE_ORDER_URL_ID = process.env.FUSE_ORDER_URL_ID || ''
let FUSE_ORDER_URL_PW = process.env.FUSE_ORDER_URL_PW || ''

let CNTT_SHOP_STATUS_URL = process.env.CNTT_SHOP_STATUS_URL || ''
let CNTT_SHOP_STATUS_URL_ID = process.env.CNTT_SHOP_STATUS_URL_ID || ''
let CNTT_SHOP_STATUS_URL_PW = process.env.CNTT_SHOP_STATUS_URL_PW || ''
let CNTT_ORDER_URL = process.env.CNTT_ORDER_URL || ''
let CNTT_ORDER_URL_ID = process.env.CNTT_ORDER_URL_ID || ''
let CNTT_ORDER_URL_PW = process.env.CNTT_ORDER_URL_PW || ''


let K_TINFO_CUSTOMER = process.env.K_TINFO_CUSTOMER || ''
let K_TINFO_SERVICE_OPERATOR = process.env.K_TINFO_SERVICE_OPERATOR || ''
let K_TINFO_FRANCHISE = process.env.K_TINFO_FRANCHISE || ''
let K_TINFO_BRANCH_STORE = process.env.K_TINFO_BRANCH_STORE || ''
let K_TINFO_BRANCH_STORE_STATE = process.env.K_TINFO_BRANCH_STORE_STATE || ''
let K_TUSER_MANAGE = process.env.K_TUSER_MANAGE || ''
let K_THISTORY_TELAPHONE = process.env.K_THISTORY_TELAPHONE || ''
let K_THISTORY_ORDER_LARGE = process.env.K_THISTORY_ORDER_LARGE || ''
let K_TSMS_TRANSMITION_LARGE = process.env.K_TSMS_TRANSMITION_LARGE || ''
let K_TSMS_TRANSMITION_SMALL = process.env.K_TSMS_TRANSMITION_SMALL || ''

function define(name, value) {
    Object.defineProperty(exports, name, {
        value: value,
        enumerable: true
    })
}

function chkIntString(val) {
    let iData = parseInt(val, 10)
    if (isNaN(iData)) {
        return val
    }
    if (iData >= 0) {
        return iData
    }
    return false
}

function convert_mb(value) {
    value = parseInt(value) * 1024 * 1024
    return value
}

function chk_age(storage) {
    if (parseInt(storage) < 1) {
        storage = 1
    }
    else if (parseInt(storage) > 60) {
        storage = 60
    }
    return storage * 60 * 60 * 24
}


define('SVR_ENV', {
    NODE_ENV: NODE_ENV
    , IF_CONTENTS_PORT: IF_CONTENTS_PORT
    , IF_PING_PORT: IF_PING_PORT
    , IF_NAME: IF_NAME
})

define('LOGGER', {
    LOG_LEVEL: LOG_LEVEL
    , LOG_FILE_NAME: LOG_FILE_NAME
    , LOG_MAX_FILE: LOG_MAX_FILE
    , LOG_MAX_SIZE: LOG_MAX_SIZE
    , LOG_STORAGE_AGE: LOG_STORAGE_AGE
    , LOG_LOGGING_MDN: LOG_LOGGING_MDN
})

define('EXPIRE_TIME', {
    OVS_DB_TIME: OVS_DB_TIME
})

define("DB_INFO", {
    OVS_DB_HOST_1: OVS_DB_HOST_1
    , OVS_DB_PORT_1: OVS_DB_PORT_1
    , OVS_DB_USER_1: OVS_DB_USER_1
    , OVS_DB_PASSWORD_1: OVS_DB_PASSWORD_1
    , OVS_DB_NAME_1: OVS_DB_NAME_1
    , OVS_DB_HOST_2: OVS_DB_HOST_2
    , OVS_DB_PORT_2: OVS_DB_PORT_2
    , OVS_DB_USER_2: OVS_DB_USER_2
    , OVS_DB_PASSWORD_2: OVS_DB_PASSWORD_2
    , OVS_DB_NAME_2: OVS_DB_NAME_2
    , OVS_DB_CONNECT_TIMEOUT: OVS_DB_CONNECT_TIMEOUT
    , OVS_DB_CONNECT_LIMIT: OVS_DB_CONNECT_LIMIT
    , OVS_DB_REMOVE_NODE_ERROR_COUNT: OVS_DB_REMOVE_NODE_ERROR_COUNT
})

define('REDIS_INFO', {
    R_IP_MASTER: R_IP_MASTER
    , R_PORT_MASTER: R_PORT_MASTER
    , R_IP_MONITOR_A: R_IP_MONITOR_A
    , R_PORT_MONITOR_A: R_PORT_MONITOR_A
    , R_P: R_P
})


define('CONN_SETTING', {
    OVS_SHOP_URL: OVS_SHOP_URL
    , OVS_SHOP_URL_ID: OVS_SHOP_URL_ID
    , OVS_SHOP_URL_PW: OVS_SHOP_URL_PW
    , OVS_ORDER_CHK_URL: OVS_ORDER_CHK_URL

    , FUSE_SHOP_STATUS_URL: FUSE_SHOP_STATUS_URL
    , FUSE_SHOP_STATUS_URL_ID: FUSE_SHOP_STATUS_URL_ID
    , FUSE_SHOP_STATUS_URL_PW: FUSE_SHOP_STATUS_URL_PW
    , FUSE_ORDER_URL: FUSE_ORDER_URL
    , FUSE_ORDER_URL_ID: FUSE_ORDER_URL_ID
    , FUSE_ORDER_URL_PW: FUSE_ORDER_URL_PW

    , CNTT_SHOP_STATUS_URL: CNTT_SHOP_STATUS_URL
    , CNTT_SHOP_STATUS_URL_ID: CNTT_SHOP_STATUS_URL_ID
    , CNTT_SHOP_STATUS_URL_PW: CNTT_SHOP_STATUS_URL_PW
    , CNTT_ORDER_URL: CNTT_ORDER_URL
    , CNTT_ORDER_URL_ID: CNTT_ORDER_URL_ID
    , CNTT_ORDER_URL_PW: CNTT_ORDER_URL_PW
})


define('Q_ENC_K', {
    K_TINFO_CUSTOMER: K_TINFO_CUSTOMER
    , K_TINFO_SERVICE_OPERATOR: K_TINFO_SERVICE_OPERATOR
    , K_TINFO_FRANCHISE: K_TINFO_FRANCHISE
    , K_TINFO_BRANCH_STORE: K_TINFO_BRANCH_STORE
    , K_TINFO_BRANCH_STORE_STATE: K_TINFO_BRANCH_STORE_STATE
    , K_TUSER_MANAGE: K_TUSER_MANAGE
    , K_THISTORY_TELAPHONE: K_THISTORY_TELAPHONE
    , K_THISTORY_ORDER_LARGE: K_THISTORY_ORDER_LARGE
    , K_TSMS_TRANSMITION_LARGE: K_TSMS_TRANSMITION_LARGE
    , K_TSMS_TRANSMITION_SMALL: K_TSMS_TRANSMITION_SMALL
})