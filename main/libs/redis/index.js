const ioRedis = require('ioredis')
const Sentinel = require('simple-redis-sentinel')

let CONSTS = require('../CONSTS')
const CONSTS_SYS = require('../CONSTS_SYS')
const logger = require('../logger')
const netUtils = require('../utils/netUtils')
const initBasicData = require('../../processApp/initBasicData')
const selectRequestProc = require('../../processApp/selectRequestProc')
//const ovsShop =require('../../exInterface/IF_ovsShop')

let m_oRedisInit, m_oRedisOrder
let m_oSub, m_oPub
let redisManager
let redisMasterConnInfo = connInfo(CONSTS.REDIS_INFO.R_IP_MASTER, CONSTS.REDIS_INFO.R_PORT_MASTER)


//////////////////////
// SUB CH
function getSublishMessage(ch, data) {
    logger.debug('redis - getSublishMessage - ch: ' + ch + ' - data: ' + JSON.stringify(data))

    switch (ch.toString()) {
        // 컨텐츠 처음 시작할 때 기본 데이터
        case CONSTS_SYS.SUB_CH.SHOP_BASIC_DATA:
            initBasicData.contentsShopBasicData(data)
            break



        // 쿼리 그룹 코드를 실행
        case CONSTS_SYS.SUB_CH.REQ_QGCODE:
            selectRequestProc.exeQueryGroupCode(data)
            break


        // ORDER 실행
        case CONSTS_SYS.SUB_CH.REQ_ORDER:
            selectRequestProc.exeOrder(data)
            break



        // POS 연동 실행
        case CONSTS_SYS.SUB_CH.REQ_POS:
            selectRequestProc.exePOSInterface(data)
            break

        // POS 연동 - 페이 결제 후 주문 알림
        case CONSTS_SYS.SUB_CH.REQ_POS_AFTER_PAY:
            selectRequestProc.exePOSInterfaceAfterPay(data)
            break


        // 통계 인서트...
        case CONSTS_SYS.SUB_CH.STAT_DATA:
            selectRequestProc.exeQueryGroupCode(data)
            break


        // SKM&S card 선결제...
        case CONSTS_SYS.SUB_CH.REQ_PC:
            selectRequestProc.payCard(data)
            break


        // SKM&S card 선결제 리턴 메시지
        case CONSTS_SYS.SUB_CH.REQ_PC_PAY_RES:
            selectRequestProc.payCardResponse(data)
            break


        // SKM&S card 결제 취소 리턴 메시지
        case CONSTS_SYS.SUB_CH.REQ_PC_PAY_CANCEL_RES:
            selectRequestProc.payCardCancelResponse(data)
            break


        // case CONSTS_SYS.SUB_CH.SHOP_DATA_CHANGE:
        //     console.log('SUB!!!!! - ' + CONSTS_SYS.SUB_CH.SHOP_BASIC_DATA)
        //     initBasicData.setInitBasicData()
        //     break
    }
}


startNoSentinel()


if (CONSTS.REDIS_INFO.R_IP_MONITOR_A !== '') {
    setTimeout(function () {
        startSentinelConnecting()
    }, 5000)
}


function startNoSentinel() {
    newInitRedis()
    newInitPub()
    newInitSub()
    orderData()
}

function newInitRedis() {
    m_oRedisInit = new ioRedis({
        host: redisMasterConnInfo.getIP()
        , port: redisMasterConnInfo.getPort()
        , password: CONSTS.REDIS_INFO.R_P
        , db: 0
    })

    m_oRedisInit.on("wait", function (err) {
        if (err) {
            logger.error("redis - Init - on wait err: " + err)
        }
        else {
            logger.info('redis - Init - on wait...')
        }
    })
    m_oRedisInit.on("close", function (err) {
        if (err) {
            logger.error("redis - Init - on close err: " + err)
        }
        else {
            logger.info('redis - Init - on close...')
        }
    })
    m_oRedisInit.on("reconnecting", function (err) {
        if (err) {
            logger.error("redis - Init - on reconnecting err: " + err)
        }
        else {
            logger.info('redis - Init - on reconnecting...')
        }
    })
    m_oRedisInit.on("connect", function (err) {
        if (err) {
            logger.error("redis - Init - on connect err: " + err)
        }
        else {
            logger.info('redis - Init - on connect...')
        }
    })
    m_oRedisInit.on("ready", function (err) {
        if (err) {
            logger.error("redis - Init - on ready err: " + err)
        }
        else {
            logger.info('redis - Init - on ready...')
        }
    })
    m_oRedisInit.on("error", function (err) {
        if (err) {
            logger.error("redis - Init - on error - err: " + err)
        }
    })
    m_oRedisInit.on("end", function (err) {
        if (err) {
            logger.error("redis - Init - on end err: " + err)
        }
        else {
            logger.info('redis - Init - on end...')
        }
    })
    m_oRedisInit.on("drain", function (err) {
        if (err) {
            logger.error("redis - Init - on drain err: " + err)
        }
        else {
            logger.info('redis - Init - on drain...')
        }
    })
    m_oRedisInit.on("idle", function (err) {
        if (err) {
            logger.error("redis - Init - on idle err: " + err)
        }
        else {
            logger.info('redis - Init - on idle...')
        }
    })
}

function orderData() {
    m_oRedisOrder = new ioRedis({
        host: redisMasterConnInfo.getIP()
        , port: redisMasterConnInfo.getPort()
        , password: CONSTS.REDIS_INFO.R_P
        , db: 4
    })

    m_oRedisOrder.on("wait", function (err) {
        if (err) {
            logger.error("redis - orderData - on wait err: " + err)
        }
        else {
            logger.info('redis - orderData - on wait...')
        }
    })
    m_oRedisOrder.on("close", function (err) {
        if (err) {
            logger.error("redis - orderData - on close err: " + err)
        }
        else {
            logger.info('redis - orderData - on close...')
        }
    })
    m_oRedisOrder.on("reconnecting", function (err) {
        if (err) {
            logger.error("redis - orderData - on reconnecting err: " + err)
        }
        else {
            logger.info('redis - orderData - on reconnecting...')
        }
    })
    m_oRedisOrder.on("connect", function (err) {
        if (err) {
            logger.error("redis - orderData - on connect err: " + err)
        }
        else {
            logger.info('redis - orderData - on connect...')
        }
    })
    m_oRedisOrder.on("ready", function (err) {
        if (err) {
            logger.error("redis - orderData - on ready err: " + err)
        }
        else {
            logger.info('redis - orderData - on ready...')
        }
    })
    m_oRedisOrder.on("error", function (err) {
        if (err) {
            logger.error("redis - orderData - on error - err: " + err)
        }
    })

    m_oRedisOrder.on("end", function (err) {
        if (err) {
            logger.error("redis - orderData - on end err: " + err)
        }
        else {
            logger.info('redis - orderData - on end...')
        }
    })
    m_oRedisOrder.on("drain", function (err) {
        if (err) {
            logger.error("redis - orderData - on drain err: " + err)
        }
        else {
            logger.info('redis - orderData - on drain...')
        }
    })
    m_oRedisOrder.on("idle", function (err) {
        if (err) {
            logger.error("redis - orderData - on idle err: " + err)
        }
        else {
            logger.info('redis - orderData - on idle...')
        }
    })
    /*m_oRedisOrder.monitor(function (err, monitor) {
        monitor.on('monitor', function (time, args, source, database) {
            logger.debug('redis - orderData - on monitor - \n' +
                ' # time     : ' + time + '\n' +
                ' # args     : ' + args + '\n' +
                ' # source   : ' + source + '\n' +
                ' # database : ' + database);
        });
    });*/
}


function newInitPub() {
    m_oPub = new ioRedis({
        host: redisMasterConnInfo.getIP()
        , port: redisMasterConnInfo.getPort()
        , password: CONSTS.REDIS_INFO.R_P
        , db: 14
    })

    m_oPub.on('close', function (err) {
        if (err) {
            logger.error('pub - on close err: ' + err)
        }
        else {
            logger.info('pub - on close...')
        }
    })
    m_oPub.on('reconnecting', function (err) {
        if (err) {
            logger.error('pub - on reconnecting err: ' + err)
        }
        else {
            logger.info('pub - on reconnecting...')
        }
    })
    m_oPub.on('connect', function (err) {
        if (err) {
            logger.error('pub - on connect err: ' + err)
        }
        else {
            logger.info('pub - on connect...')
        }
    })
    m_oPub.on('ready', function (err) {
        if (err) {
            logger.error('pub - on ready err: ' + err)
        }
        else {
            logger.info('pub - on ready...')
        }
    })
    m_oPub.on('error', function (err) {
        if (err) {
            logger.error('pub - on error - err: ' + err)
        }
    })

    m_oPub.on('end', function (err) {
        if (err) {
            logger.error('pub - on end err: ' + err)
        }
        else {
            logger.info('pub - on end...')
        }
    })
    m_oPub.on('drain', function (err) {
        if (err) {
            logger.error('pub - on drain err: ' + err)
        }
        else {
            logger.info('pub - on drain...')
        }
    })
    m_oPub.on('idle', function (err) {
        if (err) {
            logger.error('pub - on idle err: ' + err)
        }
        else {
            logger.info('pub - on idle...')
        }
    })
    /*m_oPub.monitor(function (err, monitor) {
        monitor.on('monitor', function (time, args, source, database) {
            logger.debug('redis - sub on monitor - \n' +
                ' # time     : ' + time + '\n' +
                ' # args     : ' + args + '\n' +
                ' # source   : ' + source + '\n' +
                ' # database : ' + database);
        });
    });*/
}


function newInitSub() {
    m_oSub = new ioRedis({
        host: redisMasterConnInfo.getIP()
        , port: redisMasterConnInfo.getPort()
        , password: CONSTS.REDIS_INFO.R_P
        , db: 15
    })

    m_oSub.on("close", function (err) {
        if (err) {
            logger.error("sub - on close err: " + err)
        }
        else {
            logger.info('sub - on close...')
        }
    })
    m_oSub.on("reconnecting", function (err) {
        if (err) {
            logger.error("sub - on reconnecting err: " + err)
        }
        else {
            logger.info('sub - on reconnecting...')
        }
    })
    m_oSub.on("connect", function (err) {
        if (err) {
            logger.error("sub - on connect err: " + err)
        }
        else {
            logger.info('sub - on connect...')
        }
    })
    m_oSub.on("ready", function (err) {
        if (err) {
            logger.error("sub - on ready err: " + err)
        }
        else {
            logger.info('sub - on ready...')
        }
    })
    m_oSub.on("error", function (err) {
        if (err) {
            logger.error("sub - on error - err: " + err)
        }
    })

    m_oSub.on("end", function (err) {
        if (err) {
            logger.error("sub - on end err: " + err)
        }
        else {
            logger.info('sub - on end...')
        }
    })
    m_oSub.on("drain", function (err) {
        if (err) {
            logger.error("sub - on drain err: " + err)
        }
        else {
            logger.info('sub - on drain...')
        }
    })
    m_oSub.on("idle", function (err) {
        if (err) {
            logger.error("sub - on idle err: " + err)
        }
        else {
            logger.info('sub - on idle...')
        }
    })
    m_oSub.on('message', function (channel, message) {
        logger.debug('redis - subscribe from channel: ' + channel)
        getSublishMessage(channel, message)
    })

    m_oSub.subscribe(
        CONSTS_SYS.SUB_CH.SHOP_BASIC_DATA
        , CONSTS_SYS.SUB_CH.REQ_QGCODE
        , CONSTS_SYS.SUB_CH.REQ_ORDER
        , CONSTS_SYS.SUB_CH.REQ_POS
        , CONSTS_SYS.SUB_CH.REQ_POS_AFTER_PAY
        , CONSTS_SYS.SUB_CH.STAT_DATA
        //, CONSTS_SYS.SUB_CH.SHOP_DATA_CHANGE
        , CONSTS_SYS.SUB_CH.REQ_PC
        , CONSTS_SYS.SUB_CH.REQ_PC_PAY_RES
        , CONSTS_SYS.SUB_CH.REQ_PC_PAY_CANCEL_RES

        , function (err, count) {
            if (err) {
                logger.error('redis - subscribe - err: ' + err)
            }
            else {
                logger.info('redis - subscribe - ch count: ' + count)
            }
        })
}


let sentinelEndpoints
let clusterName = 'redis_ma'
let redisOptions = {
    useSlave: true,
    redisConnectOptions: {
        retry_max_delay: 2000
    }
}


//////////////////////////////////
// start Sentinel...
function startSentinelConnecting() {
    logger.debug('redis - startSentinelConnecting...')

    netUtils.isConnectedSocket(CONSTS.REDIS_INFO.R_IP_MONITOR_A, CONSTS.REDIS_INFO.R_PORT_MONITOR_A, (isConn) => {
        if (!isConn) {
            logger.warning('redis - Not connected Redis sentinel !!! Cannot redis monitoring !!!')
            return
        }

        logger.info('redis - sentinel connected....')

        sentinelEndpoints = [{host: CONSTS.REDIS_INFO.R_IP_MONITOR_A, port: CONSTS.REDIS_INFO.R_PORT_MONITOR_A}]

        initNewSentinel()
    })
}


function initNewSentinel() {
    redisManager = new Sentinel(clusterName, sentinelEndpoints, redisOptions)

    redisManager.init(function (err, res) {
        if (err) {
            logger.error('redis - initNewSentinel Error !!! err: ' + err)
            //redisMasterConnInfo.resetConnInfo(CONSTS.REDIS_INFO.R_IP_MASTER, CONSTS.REDIS_INFO.R_PORT_MASTER);
        }
        else {
            if (res) {
                let resHost = ''
                let resPort = 0

                for (let i in res) {
                    if (res[i] !== undefined) {
                        resHost = res[i].host
                        resPort = parseInt(res[i].port)
                    }
                }

                logger.info('redis - initNewSentinel - master conn info - ' + resHost + ' : ' + resPort)
                ////redisMasterConnInfo.resetConnInfo(resHost, resPort);

                let masterConnection = redisManager.getMasterClientById('redis_ma')
                //let masterForSubscription = redisManager.getMasterClientById('masterForSub');
                let slaveConnection = redisManager.getSlaveClientById('redis_sv')

                redisManager.onSlaveChange(function () {
                    logger.info('new slave host ---- ' + slaveConnection.address)
                })

                redisManager.onMasterChange(function () {
                    logger.info('new master host: ' + masterConnection.address)

                    let arrT = masterConnection.address.split(':')
                    if (arrT && arrT.length > 1) {
                        let sIP = arrT[0]
                        let iPort = parseInt(arrT[1])

                        logger.info('new Redis master host: ' + masterConnection.address)

                        redisMasterConnInfo.resetConnInfo(sIP, parseInt(iPort))
                    }
                })

                redisMasterConnInfo.resetConnInfo(resHost, resPort)
            }
            else {
                logger.error('redis - initSentinel - no res...')
            }
        }
    })
}


function connInfo(ip, port) {
    return {
        getIP: function () {
            return ip
        }
        , getPort: function () {
            return port
        }
        , resetConnInfo: function (sIp, iPort) {
            logger.info('redis - connInfo - org host: ' + ip + ' : ' + port + ' --- chg host: ' + sIp + ' : ' + iPort)
            if (port !== iPort) {
                ip = sIp
                port = iPort

                redisDisconnect()

                setTimeout(function () {
                    redisNewInit()
                }, 1000)
            }
        }
    }
}

function redisDisconnect() {
    logger.info('redis - redisDisconnect.....')
    m_oRedisInit.end()
    m_oPub.end()
    m_oSub.end()
    m_oRedisOrder.end()
}

function redisNewInit() {
    logger.info('redis - redisNewInit.....')

    m_oRedisInit = null
    m_oPub = null
    m_oSub = null
    m_oRedisOrder = null

    newInitRedis()
    newInitPub()
    newInitSub()
    orderData()
}


exports.existsRedisH = function (key, field, cb) {
    m_oRedisInit.hexists(key, field, function (err, result) {
        if (err) {
            cb('redis - existsRedisH - err: ' + err)
            return
        }
        cb(null, result)
    })
}

exports.setRedisH = function (key, data, cb) {
    m_oRedisInit.hmset(key, data, function (err, result) {
        if (err) {
            cb('redis - setRedisH - err: ' + err)
            return
        }
        cb(null, result === 'OK' ? true : result)
    })
}

exports.getAllRedisH = (key, cb) => {
    m_oRedisInit.hgetall(key, (err, result) => {
        if (err) {
            logger.error('redis - getAllRedisH - err: ' + err)
            cb(err)
            return
        }
        cb(null, result)
    })
}

exports.getRedisH = function (key, field, cb) {
    m_oRedisInit.hget(key, field, function (err, result) {
        if (err) {
            cb('redis - getRedisH - err: ' + err)
            return
        }

        cb(null, result)
    })
}

exports.getFieldDataH = (key, field, cb) => {
    m_oRedisInit.hget(key, field, function (err, result) {
        if (err) {
            logger.error('redis - getFieldDataH - err: ' + err)
            cb(err)
            return
        }
        cb(null, result)
    })
}

exports.getPattenDataH = (patternKey, cb) => {
    m_oRedisInit.keys(patternKey + '*', function (err, result) {
        if (err) {
            logger.error('redis - getPattenDataH - err: ' + err)
            cb(err)
            return
        }
        cb(null, result)
    })
}

exports.delRedisH = function (key, cb) {
    m_oRedisInit.del(key, function (err, res) {
        if (err) {
            cb('redis - delRedisH - key: ' + key + ' , err: ' + err)
            cb(err)
            return
        }

        cb(null, res)
    })
}

exports.incrementRedisH = function (key, field, increment, cb) {
    m_oRedisInit.hincrby(key, field, increment, function (err, res) {
        if (err) {
            cb(err)
            return
        }

        cb(null, res)
    })
}


////////////////////
//// Redis data
exports.hExistsRedis = function (key, field, cb) {
    m_oRedisOrder.hexists(key, field, function (err, result) {
        if (err) {
            cb('redis - hExistsRedis - err: ' + err)
            return
        }
        cb(null, result)
    })
}
exports.hSetRedis = function (key, f, data, cb) {
    m_oRedisOrder.hmset(key, f, data, function (err, result) {
        if (err) {
            cb('redis - hSetRedis - err: ' + err)
            return
        }
        m_oRedisOrder.expire(key, 200)
        cb(null, result === 'OK' ? true : result)
    })
}
exports.hmSetRedis = function (key, arrF_Data, cb) {
    m_oRedisOrder.hmset(key, arrF_Data, function (err, result) {
        if (err) {
            cb('redis - hSetRedis - err: ' + err)
            return
        }
        m_oRedisOrder.expire(key, 200)
        cb(null, result === 'OK' ? true : result)
    })
}
exports.hmGetRedis = function (key, arrF, cb) {
    m_oRedisOrder.hmget(key, arrF, function (err, result) {
        if (err) {
            cb('redis - hmGetRedis - err: ' + err)
            return
        }
        cb(null, result)  //[null, 'abc']
    })
}
exports.hGetallRedis = function (key, cb) {
    m_oRedisOrder.hgetall(key, function (err, result) {
        if (err) {
            logger.error('redis - hGetallRedis - err: ' + err)
            cb(err)
            return
        }
        cb(null, result)
    })
}
exports.hGetRedis = function (key, field, cb) {
    m_oRedisOrder.hget(key, field, function (err, result) {
        if (err) {
            cb('redis - hGetRedis - err: ' + err)
            return
        }

        cb(null, result)
    })
}
/////////////////////////////////////////////////
// key에 속한 모든 field name을 조회
exports.hKeys = function (key, cb) {
    m_oRedisOrder.hkeys(key, function (err, result) {
        if (err) {
            logger.error('redis - hKeys - err: ' + err)
            cb(err)
            return
        }

        cb(null, result)
    })
}
exports.hDelRedis = function (key, field, cb) {
    m_oRedisOrder.hdel(key, field, function (err, res) {
        if (err) {
            cb('redis - hDelRedis - key: ' + key + ' , err: ' + err)
            cb(err)
            return
        }

        cb(null, res)
    })
}
exports.delRedisOrder = (key, cb) => {
    m_oRedisOrder.del(key, (err, result) => {
        if (err) {
            cb('redis - delRedisOrder - key: ' + key + ' , err: ' + err)
        }
        else {// result == 1
            cb(null)
        }
    })
}
exports.hIncrement = function (key, field, increment, cb) {
    m_oRedisOrder.hincrby(key, field, increment, function (err, res) {
        if (err) {
            cb(err)
            return
        }

        cb(null, res)
    })
}


exports.pubRedis = function (ch, msg) {
    //logger.debug('redis - pubH - ch: ' + ch + ' , msg: ' + JSON.stringify(msg))
    m_oPub.publish(ch, JSON.stringify(msg))
}

