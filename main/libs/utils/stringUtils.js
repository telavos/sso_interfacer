const customUnique = require('custom-unique');


exports.getSSstr = (length) => {
    return customUnique.compile({a: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'}, '((a)' + length + ')')()
}

exports.getRandomMyName = () => {
    let mix = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
    return mix.charAt(Math.floor(Math.random() * mix.length))
        + mix.charAt(Math.floor(Math.random() * mix.length))
        + mix.charAt(Math.floor(Math.random() * mix.length))
        + mix.charAt(Math.floor(Math.random() * mix.length))
        + mix.charAt(Math.floor(Math.random() * mix.length))
}

exports.isNumeric = (s) => {
    return !isNaN(Number(s))
}

exports.parsingString = (data, idx, len) => {
    return data.toString().substr(idx, len)
}

exports.parsingStringRemoveSpace = (data, idx, len) => {
    return this.parsingString(data, idx, len).replace(/ /g, '')
}