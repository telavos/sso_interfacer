const zlib = require('zlib')
const logger = require('../logger')

exports.isConnectedSocket = (ip, port, callback) => {
    let socket = new require('net').Socket()

    let onError = function () {
        socket.destroy()
        callback(false)
    }

    socket.setTimeout(500)
    socket.on('error', onError)
    socket.on('timeout', onError)

    socket.connect(port, ip, function (err) {
        if (err) {
            socket.destroy()
            callback(false)
            return
        }
        socket.end()
        callback(true)
    })
}


exports.zlibZip = (data, cb) => {
    zlib.deflate(data, function (err, buffer) {
        if (err) {
            logger.error('netUtils - zlibZip - err: ' + err)
            logger.error('netUtils - zlibZip - data --- sssss')
            logger.error(JSON.stringify(data))
            logger.error('netUtils - zlibZip - data --- eeeeeeeee')
            cb(err)
        }
        else {
            cb(null, buffer)
        }
    })
}

exports.zlibUnzip = (data, cb) => {
    zlib.unzip(new Buffer(data, 'binary'), function (err, buffer) {
        if (err) {
            logger.error('netUtils - zlibUnzip - err: ' + err)
            cb(err)
        }
        else {
            cb(null, buffer.toString())
        }
    })
}
