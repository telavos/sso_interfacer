const moment = require('moment');

exports.getDateFormat = (type) => {
    // YYYYMMDD , HHmmss
    return moment().format(type)
}
