const stringUtil = require('./utils/stringUtils')
const rootPath = process.cwd()
let CONSTS = require('./CONSTS')


function define(name, value) {
    Object.defineProperty(exports, name, {
        value: value,
        enumerable: true
    })
}

define('MY_INFO', {
    IF_NAME: stringUtil.getRandomMyName()
})

define('DIR_PATH', {
    LOG: rootPath + '/logs/'
})

define('PUB_CH', {
    RES_SHOP_BASIC_DATA: 'RES_SHOP_BASIC_DATA_'
    , RES_QGCODE: 'RES_QGCODE_'
    , RES_POS: 'RES_POS_'
    , RES_ORDER: 'RES_ORDER_'
})

define('SUB_CH', {
    SHOP_BASIC_DATA: 'SHOP_BASIC_DATA_' + CONSTS.SVR_ENV.IF_NAME
    , REQ_QGCODE: 'REQ_QGCODE_' + CONSTS.SVR_ENV.IF_NAME
    , REQ_ORDER: 'REQ_ORDER_' + CONSTS.SVR_ENV.IF_NAME
    , REQ_POS: 'REQ_POS_' + CONSTS.SVR_ENV.IF_NAME
    , REQ_POS_AFTER_PAY: 'REQ_POS_AFTER_PAY_' + CONSTS.SVR_ENV.IF_NAME
    , STAT_DATA: 'STAT_DATA_' + CONSTS.SVR_ENV.IF_NAME
    //, SHOP_DATA_CHANGE: 'SHOP_DATA_CHANGE'
    , REQ_PC: 'REQ_PC_' + CONSTS.SVR_ENV.IF_NAME
    , REQ_PC_PAY_RES: 'REQ_PC_PAY_RES_' + CONSTS.SVR_ENV.IF_NAME
    , REQ_PC_PAY_CANCEL_RES: 'REQ_PC_PAY_CANCEL_RES_' + CONSTS.SVR_ENV.IF_NAME
})

define('MESSAGE_SIZE', {
    HEADER_FULL_SIZE: 20
    , HEADER_MESSAGE_SIZE: 4
    , HEADER_MESSAGE_NO: 10
    , HEADER_SERVICE_CODE: 6
    , BODY_CLIENT_NAME: 5
    , BODY_FUNCTION_CODE: 17
    , BODY_FUNCTION_DATA_SIZE: 4
    , BODY_EXPAND_DATA_SIZE: 4
})

define("ENCRYPTION", {
    ENC_TYPE: 'aes-256-ecb'
    , BLOCK_SIZE: 16
})

define('POS_TYPE', {
    SSO_DB: '100001'
    , FUSE_PP: '200101'
    , FUSE_XXX: '200102'
    , CNTT_BQ: '200201'
});