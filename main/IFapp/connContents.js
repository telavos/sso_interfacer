const net = require('net')
const HashMap = require('hashmap')

const CONSTS = require('../libs/CONSTS')
const logger = require('../libs/logger')
const selQuery = require('../libs/db/selQuery')
const redis = require('../libs/redis')
const stringUtils = require('../libs/utils/stringUtils')
const ovsShop = require('../exInterface/IF_ovsShop')
const posFUSE = require('../exInterface/IF_FUSE')
const posCNTT = require('../exInterface/IF_CNTT')
const netUtils = require('../libs/utils/netUtils')
//const ovsDB = require('../libs/db/index_single')
const ssoDB = require('../libs/db/ssoDB')
const exInterface = require('../exInterface/index')
//let commonMapData = require('../../main/processApp/commonMapData')

let m_mapContents = new HashMap()


////////////////////////////////////
//from Contents...
let oConnContents = net.createServer(function (oContents) {
    let messageNo = stringUtils.getSSstr(10)

    oContents.on('data', function (oContentsData) {

        if (!oContentsData) {
            logger.error('connContents - on data - !oContentsData')
            oContentsData.destroy()
            return
        }

        let sResData = oContentsData.toString()
        let receiveData = JSON.parse(sResData)
        let messageType = receiveData.messageType

        //console.log('====ssssss==================')
        //console.dir(receiveData)
        //console.log('==========eeeeeeeeee============')

        oContents.messageNo = messageNo
        oContents.messageType = messageType

        ///////////////////////////////////////////////
        // OVS START....
        if (messageType === 'OVS_START') {
            logger.debug('connContents - oConnContents - OVS_START...')

            return
        }  // end of OVS_START


        getContentsConnectionCount('create', (err, connCnt) => {
            if (err) {
                logger.error('connContents - getContentsConnectionCount err: ' + err)
                let rErr = {}
                rErr.ERROR = err
                doSocketWriteData(m_mapContents.get(messageNo), JSON.stringify(rErr))
                return
            }

            oContents.setTimeout(5000)

            setMapContents(messageNo, messageType, oContents)

            logger.info('connContents - [' + connCnt + '] - messageType: ' + messageType + ' - messageNo: ' + messageNo)


            // EX_QGCODE_FIRST_SCEN
            if (messageType === 'EX_QGCODE_FIRST_SCEN') {
                doMsgType_EX_QGCODE_FIRST_SCEN(receiveData, messageNo)
                return
            }


            // EX_QGCODE , EX_QGCODE_SCEN , ORDER_CHECK , CART_MAIN_ADD , EX_QGCODE_OVS
            if (messageType === 'EX_QGCODE'
                || messageType === 'EX_QGCODE_SCEN'
                || messageType === 'ORDER_CHECK'
                || messageType === 'CART_MAIN_ADD'
                || messageType === 'EX_QGCODE_OVS') {
                doMsgType_EX_QGCODE_ALL(receiveData, messageNo)
                return
            }

            // SHOP_ORDER
            if (messageType === 'SHOP_ORDER') {
                doMsgType_SHOP_ORDER(receiveData, messageNo)
                return
            }

            // EX_QGCODE_FINAL_ORDER
            // 최종 주문 및 Shop으로 보내고 DB에 주문 데이터 인서트...
            if (messageType === 'EX_QGCODE_FINAL_ORDER') {
                doMsgType_EX_QGCODE_FINAL_ORDER(receiveData, messageNo)
                return
            }

            // CC_FIRST를 실행한다...
            if (messageType === 'CC_FIRST') {
                doMsgType_CC_FIRST(receiveData, messageNo)
                return
            }


            ////////////////////////////////////
            // CC_FIRST_POS 실행한다...
            if (messageType === 'CC_FIRST_POS') {
                let posType = receiveData.posType
                doMsgType_CC_FIRST_POS(messageType, posType, receiveData, messageNo)
                return
            }


            ////////////////////////////////////////
            // Contents 기본 데이터일 경우
            if (messageType === 'STORE_BASIC_DATA'
                || messageType === 'FIRST_SCENARIO_CODE') {
                //|| messageType === 'OVS_INIT') {
                doMsgType_BASIC_DATA(receiveData, messageNo)
                return
            }

            // if (messageType === 'initDataReloding') {
            //     doMsgType_initDataReloding(messageNo)
            //     return
            // }

            ///////////////////////////////////////
            // OVS_CLICK
            if (messageType === 'OVS_CLICK') {
                doMsgType_OVS_CLICK(receiveData, messageNo)
                return
            }


            ////////////////////////////////////////////////////
            // SP - history....
            if (messageType === 'EX_STORED_PROCEDURE') {
                doMsgType_EX_STORED_PROCEDURE(receiveData, messageNo)
                return
            }


            ////////////////////////////////////////////////////////////////////////
            // SET_MENU - 선택한 셋트 메뉴에 대한 메뉴를 가져온 후 주문 데이터 인서트...
            if (messageType === 'SETMENU_SELECT_ORDER') {
                doMsgType_SETMENU_SELECT_ORDER(receiveData, messageNo)
                return
            }


            ////////////////////////////////////////////////////////////////////////
            // HISTORY - 선택한 히스토리 메뉴의 세부 항목을 가져온 후 주문 데이터 인서트...
            if (messageType === 'HISTORY_SELECT_ORDER') {
                doMsgType_HISTORY_SELECT_ORDER(receiveData, messageNo)
                return
            }


            ////////////////////////////////////////////////////
            // insert stat view...
            if (messageType === 'EX_STAT_VIEW') {
                doMsgType_EX_STAT_VIEW(receiveData, messageNo)
            }

        })  // end of getContentsConnectionCount
    })

    oContents.on('close', function () {
        logger.info('connContents - oContents close... messageType: ' + oContents.messageType + ' , messageNo: ' + oContents.messageNo)
    })

    oContents.on('error', function (err) {
        if (err.code !== 'ECONNRESET') {
            logger.error('connContents - oContents ERROR -  err: ' + JSON.stringify(err))
            this.destroy()
        }
        else {
            logger.debug('connContents - oContents on error - ECONNRESET !!!')

            this.destroy()
        }
    })

    oContents.on('end', function () {
        logger.debug('connContents - oContents on end...')

        removeClientContents(messageNo)
    })

    oContents.on('timeout', function () {
        logger.warning('connContents - oContents on timeout...')
        this.destroy()
    })
})


oConnContents.on('error', function (e) {
    if (e.errno === 'EADDRINUSE') {
        logger.error('oConnContents - Address in use, retrying... ' + CONSTS.SVR_ENV.IF_CONTENTS_PORT)
        setTimeout(function () {
            oConnContents.close()
            oConnContents.listen(CONSTS.SVR_ENV.IF_CONTENTS_PORT)
        }, 1000)
    }
    else {
        logger.error('oConnContents - on error -  err: ' + e)
        oConnContents.destroy()
    }
})
oConnContents.listen(CONSTS.SVR_ENV.IF_CONTENTS_PORT, function () {
    logger.info('oConnContents - Server listening on ' + CONSTS.SVR_ENV.IF_CONTENTS_PORT)
    logger.info('address: ' + oConnContents.address().address + ' , Port: ' + oConnContents.address().port)
})


///////////////////////////////////////
// EX_QGCODE_FIRST_SCEN
function doMsgType_EX_QGCODE_FIRST_SCEN(receiveData, messageNo) {
    logger.debug('connContents - doMsgType_EX_QGCODE_FIRST_SCEN - EX_QGCODE_FIRST_SCEN... messageNo: ' + messageNo + ' - data start -----')
    logger.debug(receiveData)
    logger.debug('connContents - doMsgType_EX_QGCODE_FIRST_SCEN - EX_QGCODE_FIRST_SCEN... messageNo: ' + messageNo + ' - data end -----')

    let queryGroupCode = receiveData.sendData.queryGroupCode
    let params = receiveData.sendData.params

    logger.debug('connContents - doMsgType_EX_QGCODE_FIRST_SCEN - queryGroupCode: ' + queryGroupCode)

    // 최초 진입일 경우 history menu count를 조회한다.
    let bHistoryMenuExists = false
    let p = []
    let tMdn = params[2].USER_MDN
    let tMrSeq = params[2].MRHST_SEQ
    p.push({MRHST_SEQ: tMrSeq, USER_MDN: tMdn})
    selQuery.procExecuteQuery('QG_HISTORYMENU_COUNT', p, (err, oCnt) => {
        if (err) {
            logger.error('connContents - doMsgType_EX_QGCODE_FIRST_SCEN - EX_QGCODE_FIRST_SCEN - get history count - err: ' + err)
            sendErrorToContents(messageNo, err)
            return
        }

        if (oCnt.CHK_HISTORY_COUNT.length) {
            bHistoryMenuExists = true
        }

        // EX_QGCODE_FIRST_SCEN 쿼리 시작....
        if (!receiveData.sendData) {
            logger.warning('connContents - doMsgType_EX_QGCODE_FIRST_SCEN - No receiveData.sendData !!!')
            sendErrorToContents(messageNo, 'No receiveData.sendData...')
            return
        }

        /////////////////////////////////////////
        // 전달받은 쿼리 그룹 코드를 실행한다.
        if (params[0] && !Object.keys(params[0]).length) {
            logger.error('connContents - doMsgType_EX_QGCODE_FIRST_SCEN - QGCODE_FIRST_SCEN - err1: ' + err)
            sendErrorToContents(messageNo, err)
            return
        }

        selQuery.procExecuteQuery(queryGroupCode, params, (err, rows) => {
            if (err) {
                logger.error('connContents - doMsgType_EX_QGCODE_FIRST_SCEN - QGCODE_FIRST_SCEN - err2: ' + err)
                sendErrorToContents(messageNo, err)
                return
            }

            rows.HISTORY_MENU_EXISTS = [{HISTORY_MENU_EXISTS: bHistoryMenuExists}]

            doSocketWriteData(m_mapContents.get(messageNo), JSON.stringify(rows))
        })
    })
}


////////////////////////////////////////////////////////////////////////////////////////
// EX_QGCODE , EX_QGCODE_SCEN , ORDER_CHECK , CART_MAIN_ADD , EX_QGCODE_OVS
function doMsgType_EX_QGCODE_ALL(receiveData, messageNo) {
    logger.debug('connContents - doMsgType_EX_QGCODE_ALL - messageNo: ' + messageNo + ' - data start -----')
    logger.debug(receiveData)
    /*
    messageType=ORDER_CHECK, messageNo=1d8XpMBLBc, queryGroupCode=QGSNT91CHK
     , params=[SLE_HIST_SEQ=22403, SLE_HIST_SEQ=22403]
     */
    logger.debug('connContents - doMsgType_EX_QGCODE_ALL - messageNo: ' + messageNo + ' - data end -----')

    if (!receiveData.sendData) {
        logger.warning('connContents - QGCODE/QGCODE_SCEN/ORDER_CHECK/CART_MAIN_ADD/QGCODE_OVS - No receiveData.sendData !!!')
        sendErrorToContents(messageNo, 'No receiveData.sendData...')
        return
    }

    console.log('sss recccccc')
    console.dir(receiveData.sendData)
    console.log('eeeeeee recccccc')
    /*  receiveData.sendData
    91화면으로 들어갈 때...
    { queryGroupCode: 'QGSNT91', params: [ { SLE_HIST_SEQ: 22403 }, { SLE_HIST_SEQ: 22403 } ] }


       order check... 91화면에서 계속 현재 주문 상태 날릴 때....
    { queryGroupCode: 'QGSNT91CHK', params: [ { SLE_HIST_SEQ: 22403 }, { SLE_HIST_SEQ: 22403 } ] }

      order cancel... 94화면
      { queryGroupCode: 'QGSNT94', params: [ { ORDER_STTUS_DE_HMS: '20190613172147', SLE_HIST_SEQ: 22403 , POS_CODE: 100003 } ] }
     */

    let queryGroupCode = receiveData.sendData.queryGroupCode
    let params = receiveData.sendData.params

    logger.debug('connContents - doMsgType_EX_QGCODE_ALL - queryGroupCode: ' + queryGroupCode)

    let posCode = ''
    if(queryGroupCode === 'QGSNT94'){
        posCode = receiveData.sendData.params[0].POS_CODE
    }
    console.log('### POS_CODE: ' + posCode)

    if(posCode === '100001'){
        exInterface.sendData('ORDER_CANCEL', 'FUSE', params[0], (err, exRet)=>{
            if(err){
                logger.error('connContents - doMsgType_EX_QGCODE_ALL - sendData err: ' + err)
                sendErrorToContents(messageNo, err)
                return
            }

            selQuery.procExecuteQuery(queryGroupCode, params, (err, rows) => {
                if (err) {
                    logger.error('connContents - doMsgType_EX_QGCODE_ALL - err2: ' + err)
                    sendErrorToContents(messageNo, err)
                    return
                }

                doSocketWriteData(m_mapContents.get(messageNo), JSON.stringify(rows))
            })
        })
    }
    else {
        /////////////////////////////////////////
        // 전달받은 쿼리 그룹 코드를 실행한다.
        if (params[0] && !Object.keys(params[0]).length) {
            logger.error('connContents - doMsgType_EX_QGCODE_ALL - err1: no params data...')
            sendErrorToContents(messageNo, 'no params data...')
            return
        }

        selQuery.procExecuteQuery(queryGroupCode, params, (err, rows) => {
            if (err) {
                logger.error('connContents - doMsgType_EX_QGCODE_ALL - err2: ' + err)
                sendErrorToContents(messageNo, err)
                return
            }

            doSocketWriteData(m_mapContents.get(messageNo), JSON.stringify(rows))
        })
    }



}


/////////////////////////////////////////////
// SHOP_ORDER
function doMsgType_SHOP_ORDER(receiveData, messageNo) {
    logger.debug('connContents - doMsgType_SHOP_ORDER - messageNo: ' + messageNo + ' - data start -----')
    logger.debug(JSON.stringify(receiveData.sendData))
    //{"rqesterSe":"100001","sleHistSeq":"23012","requstTy":"100006","mrhstSeq":5
    logger.debug('--------------------------------------------------------------')
    /*
        sessionId=RZuyurmXYCtxmKtNlljy , messageNo=VzTAbr0PP8
        , messageType=SHOP_ORDER
        , rqesterSe=100001, sleHistSeq=22405, requstTy=100001, mrhstSeq=74

        {"rqesterSe":"100001","sleHistSeq":"23014","requstTy":"100006","mrhstSeq":80}
     */
    logger.debug('connContents - doMsgType_SHOP_ORDER - messageNo: ' + messageNo + ' - data end -----')

    let sleHistSeq = receiveData.sendData.sleHistSeq
    let requstTy = receiveData.sendData.requstTy
    let rqesterSe = receiveData.sendData.rqesterSe
    let mrhstSeq = receiveData.sendData.mrhstSeq
    let posCode = receiveData.sendData.posCode || ''
    let shopNumber = receiveData.sendData.shopNumber || ''

    if (!sleHistSeq || !requstTy || !rqesterSe || !mrhstSeq) {
        logger.error('connContents - doMsgType_SHOP_ORDER - invalid data...')
        sendErrorToContents(messageNo, 'invalid data...')
        return
    }

    sleHistSeq = parseInt(sleHistSeq)
    requstTy = requstTy.toString()
    rqesterSe = rqesterSe.toString()
    mrhstSeq = mrhstSeq.toString()
    posCode = posCode
    shopNumber = shopNumber


    /*
        sleHistSeq - 22405
        requstTy - 100001
        rqesterSe - 100001
        mrhstSeq - 74
        posCode: 100003,
        shopNumber: 07052013279
     */

    if(posCode === '100001' || posCode === '') { // SSO
        ovsShop.sendDataToOvsShop(sleHistSeq, requstTy, rqesterSe, mrhstSeq, (err, res) => {
            if (err) {
                logger.error('connContents - doMsgType_SHOP_ORDER - SSO - sendDataToOvsShop -- ' + err)
                sendErrorToContents(messageNo, err)
                return
            }

            doSocketWriteData(m_mapContents.get(messageNo), JSON.stringify(res))
        })
    }
    else if(posCode === '200101'){ // FUSE_PP:
        let sendData = {sleHistSeq: sleHistSeq
            , requstTy: requstTy
            , rqesterSe: rqesterSe
            , mrhstSeq: mrhstSeq
            , shopNumber: shopNumber}

        posFUSE.sendDataFUSE('ORDER_SEND', sendData,  (err, res) => {
            if (err) {
                logger.error('connContents - doMsgType_SHOP_ORDER - FUSE - sendDataFUSE - ORDER_SEND -- ' + err)
                sendErrorToContents(messageNo, err)
                return
            }

            doSocketWriteData(m_mapContents.get(messageNo), JSON.stringify(res))
        })
    }
    else if(posCode === '200201'){ // CNTT_BQ
        let sendData = {sleHistSeq: sleHistSeq
            , requstTy: requstTy
            , rqesterSe: rqesterSe
            , mrhstSeq: mrhstSeq
            , shopNumber: shopNumber}

        posCNTT.sendDataCNTT('ORDER_SEND', sendData,  (err, res) => {
            if (err) {
                logger.error('connContents - doMsgType_SHOP_ORDER - CNTT - sendDataCNTT - ORDER_SEND -- ' + err)
                sendErrorToContents(messageNo, err)
                return
            }

            doSocketWriteData(m_mapContents.get(messageNo), JSON.stringify(res))
        })
    }
}


///////////////////////////////////////////////////////////////////////
// EX_QGCODE_FINAL_ORDER
// 최종 주문 및 Shop으로 보내고 DB에 주문 데이터 인서트...
function doMsgType_EX_QGCODE_FINAL_ORDER(receiveData, messageNo) {
    logger.debug('connContents - doMsgType_EX_QGCODE_FINAL_ORDER - messageNo: ' + messageNo + ' - data start -----')
    logger.debug(receiveData)
    /*
        messageType=EX_QGCODE_FINAL_ORDER
        , messageNo=2KQ66ESCoj
        , queryGroupCode=QGSPP306
        , params=[MRHST_SEQ=74, CSTMR_TEL_NO=01089501450, RECPTN_TEL_NO=, DLVR_AREA=테이크아웃(매장방문수령)
            , DLVR_DETAIL_AREA=, MEMO=, PRE_PYMNT_SE_CODE=100002, PYMNT_TY=100003, ORDER_AMOUNT=17900
            , ORDER_STTUS_CODE=100000, DLVR_EXPECT_TIME=0, CANCL_RESN_CODE=, CANCL_OPETR_CODE=, ORDER_URL=, CSID=test
            , ADDR_TYPE=P, RESVE_GNRL_SE=100002, ORDER_SE=100001, DLVR_SE=100002
            , ORDER_SMALL=[SAME_ORDER_GOODS_SEQ=1, LCLAS_CODE=270, SCLAS_CODE=1077, SLE_UNIT=, LCLAS_NM=양념치킨(배)
                , SCLAS_NM=블랙베이크, SCLAS_SLE_UNIT=블랙베이크, MENU_GOODS_AT=Y, SLE_QY=1, SLE_AMOUNT=17900
                , POS_MRHST_SEQ=55889999998, POS_SCLAS_CODE=0044, POS_SCLAS_NM=블랙베이크], MRHST_SEQ=74
                , POS_CODE=, RECPTN_TEL_NO=]
     */
    logger.debug('connContents - doMsgType_EX_QGCODE_FINAL_ORDER - messageNo: ' + messageNo + ' - data end -----')

    if (!receiveData.sendData) {
        logger.warning('connContents - doMsgType_EX_QGCODE_FINAL_ORDER - No receiveData.sendData !!!')
        sendErrorToContents(messageNo, 'doMsgType_EX_QGCODE_FINAL_ORDER - No receiveData.sendData')
        return
    }

    let queryGroupCode = receiveData.sendData.queryGroupCode
    let params = receiveData.sendData.params

    logger.debug('connContents - doMsgType_EX_QGCODE_FINAL_ORDER - queryGroupCode: ' + queryGroupCode)

    selQuery.procFinalOrderExecuteQuery(queryGroupCode, params, (err, rows) => {
        if (err) {
            logger.error('connContents - doMsgType_EX_QGCODE_FINAL_ORDER - oConnContents - err: ' + err)
            sendErrorToContents(messageNo, err)
            return
        }

        doSocketWriteData(m_mapContents.get(messageNo), JSON.stringify(rows))
    })
}


////////////////////////////////////
// doMsgType_CC_FIRST
// CC_FIRST를 실행한다...
function doMsgType_CC_FIRST(receiveData, messageNo) {
    logger.debug('connContents - doMsgType_CC_FIRST - messageNo: ' + messageNo + ' - data start -----')
    logger.debug(receiveData)
    logger.debug('connContents - doMsgType_CC_FIRST - messageNo: ' + messageNo + ' - data end -----')

    if (!receiveData.sendData) {
        logger.warning('connContents - doMsgType_CC_FIRST - No receiveData.sendData !!!')
        sendErrorToContents(messageNo, 'doMsgType_CC_FIRST - No receiveData.sendData')
        return
    }

    let queryGroupCode = receiveData.sendData.queryGroupCode
    let params = receiveData.sendData.params

    logger.debug('connContents - doMsgType_CC_FIRST - queryGroupCode: ' + queryGroupCode)

    /////////////////////////////////////////
    // 전달받은 쿼리 그룹 코드를 실행한다.
    selQuery.procExecuteQuery(queryGroupCode, params, (err, rows) => {
        if (err) {
            logger.error('connContents - doMsgType_CC_FIRST - oConnContents - err: ' + err)
            sendErrorToContents(messageNo, err)
            return
        }

        doSocketWriteData(m_mapContents.get(messageNo), JSON.stringify(rows))
    })
}


////////////////////////////////////
// CC_FIRST_POS
// POS의 현재 상태를 받아옴...
function doMsgType_CC_FIRST_POS(messageType, posType, receiveData, messageNo) {
    logger.debug('connContents - doMsgType_CC_FIRST_POS - posType: ' + posType + ' - messageNo: ' + messageNo + ' - data start -----')
    logger.debug(receiveData)
    logger.debug('connContents - doMsgType_CC_FIRST_POS - messageNo: ' + messageNo + ' - data end -----')

    let mrhstSeq = receiveData.sendData[0].mrhstSeq
    let shopNumber = receiveData.sendData[0].shopNumber

    console.log("#### mrhstSeq: " + mrhstSeq)
    console.log("#### shopNumber: " + shopNumber)

    //if (!receiveData.sendData) {
    //    logger.warning('connContents - doMsgType_CC_FIRST_POS - No receiveData.sendData !!!')
    //   sendErrorToContents(messageNo, 'doMsgType_CC_FIRST_POS - No receiveData.sendData')
    //    return
    //}

    //let queryGroupCode = receiveData.sendData.queryGroupCode
    //let params = receiveData.sendData.params

    //logger.debug('connContents - doMsgType_CC_FIRST_POS - queryGroupCode: ' + queryGroupCode)

    exInterface.sendData(messageType, posType, receiveData, (err, sendRes)=>{
        if(err){
            logger.error('connContents - doMsgType_CC_FIRST_POS - sendData - err: ' + err)
            sendErrorToContents(messageNo, err)
            return
        }

        //sendRes <-- shop status

        /////
        selQuery.procExecuteQuery('CCFIRST', [{RCV_TEL_NO: shopNumber}
            , {MRHST_SEQ: mrhstSeq}
            , {MRHST_SEQ: mrhstSeq}
            , {MRHST_SEQ: mrhstSeq}], (err, rows) => {
            if (err) {
                logger.error('connContents - doMsgType_CC_FIRST - oConnContents - err: ' + err)
                sendErrorToContents(messageNo, err)
                return
            }

            let retData = {shopStatus: sendRes, DBData: rows}

            console.log('++++++++++++++++++++++')
            console.dir(sendRes)
            console.log('-----------------------')
            console.dir(rows)
            console.log('++++++++++++++++++++++++++++++++++')

            doSocketWriteData(m_mapContents.get(messageNo), JSON.stringify(retData))
        })
    })

    /*//dellll///////////////////////////////////////
    // 전달받은 쿼리 그룹 코드를 실행한다.
    selQuery.procExecuteQuery(queryGroupCode, params, (err, rows) => {
        if (err) {
            logger.error('connContents - doMsgType_CC_FIRST_POS - oConnContents - err: ' + err)
            sendErrorToContents(messageNo, err)
            return
        }

        doSocketWriteData(m_mapContents.get(messageNo), JSON.stringify(rows))
    })*/
}


////////////////////////////////////////
// doMsgType_BASIC_DATA
// STORE_BASIC_DATA , FIRST_SCENARIO_CODE , OVS_INIT
function doMsgType_BASIC_DATA(receiveData, messageNo) {

    logger.debug('connContents - doMsgType_BASIC_DATA - messageNo: ' + messageNo + ' - data start -----')
    logger.debug(receiveData)
    logger.debug('connContents - doMsgType_BASIC_DATA - messageNo: ' + messageNo + ' - data end -----')

    selQuery.executeQueryUsingQueryGCode('OVSINIT', '', (err, res) => {
        if (err) {
            logger.error('connContents - OVSINIT - err: ' + err)
            sendErrorToContents(messageNo, err)
            return
        }

        logger.info('connContents - exQuery - OVSINIT - res: ' + res)

        doSocketWriteData(m_mapContents.get(messageNo), res)
    })
}


// function doMsgType_initDataReloding(messageNo) {
//     selQuery.executeQueryUsingCodes('STORE_BASIC_DATA', '', (err, rows) => {
//         if (err) {
//             logger.error('connContents - doMsgType_initDataReloding - err: ' + err)
//             return
//         }
//
//         if (!rows) {
//             logger.warning('connContents - doMsgType_initDataReloding - no store data.....')
//             return
//         }
//
//         let arrData = rows.STORE_BASIC_DATA
//
//         for (let i = 0; i < arrData.length; i++) {
//             commonMapData.setStoreInfo(arrData[i].RCV_TEL_NO, arrData[i])
//         }
//
//         logger.info('connContents - doMsgType_initDataReloding - setting basic data...')
//
//         //initStoreMent
//         // selQuery.executeQueryUsingCodes('STORE_MENT', '', (err, rows) => {
//         //     if (err) {
//         //         logger.error('connContents - doMsgType_initDataReloding - err: ' + err)
//         //         return
//         //     }
//         //
//         //     if (!rows) {
//         //         logger.warning('connContents - doMsgType_initDataReloding - no ShopMent data.....')
//         //         return
//         //     }
//         //
//         //     let arrData = rows.STORE_MENT
//         //
//         //     for (let i = 0; i < arrData.length; i++) {
//         //         commonMapData.setStoreMent(arrData[i].RCV_TEL_NO, arrData[i])
//         //     }
//         //
//         //     logger.info('connContents - doMsgType_initDataReloding - setting basic data...')
//         //     doSocketWriteData(m_mapContents.get(messageNo), JSON.stringify(''))
//         // })
//     })
// }


///////////////////////////////////////
// OVS_CLICK
function doMsgType_OVS_CLICK(receiveData, messageNo) {
    logger.debug('connContents - doMsgType_OVS_CLICK - messageNo: ' + messageNo + ' - data start -----')
    logger.debug(receiveData)
    logger.debug('connContents - doMsgType_OVS_CLICK - messageNo: ' + messageNo + ' - data end -----')

    let userMdn = receiveData.userMdn
    let sessionId = receiveData.sessionId
    let scenCode = receiveData.scenCode
    let scenType = receiveData.scenType

    logger.debug('connContents - doMsgType_OVS_CLICK - scenType: ' + scenType + ' , sessionId: ' + sessionId + ' , scenCode: ' + scenCode, userMdn)
    m_mapContents.get(messageNo).sessionId = sessionId

    ////////////////////////////////////////////////
    // scenCode 데이터 가져오기
    getNextScenarioData(scenCode, (err, scenData) => {
        if (err) {
            logger.error('connContents - doMsgType_OVS_CLICK - err: ' + err)
            sendErrorToContents(messageNo, err)
            return
        }

        doSocketWriteData(m_mapContents.get(messageNo), JSON.stringify(scenData))
    })
}


////////////////////////////////////////////////////
// SP - history....
function doMsgType_EX_STORED_PROCEDURE(receiveData, messageNo) {
    logger.debug('connContents - doMsgType_EX_STORED_PROCEDURE - messageNo: ' + messageNo + ' - data start -----')
    logger.debug(receiveData)
    logger.debug('connContents - doMsgType_EX_STORED_PROCEDURE - messageNo: ' + messageNo + ' - data end -----')

    let params = receiveData.sendData.params
    try {
        //{MRHST_SEQ: mrhstSeq, SHOP_NUMBER: shopNumber, USER_MDN: userMdn, CSID: csid}
        let q = 'CALL sp_cre_thistory_telephone(' + params[0]
            + ', "' + params[1] + '"'
            //+ ', ' + params[1] +
            + ', HEX(AES_ENCRYPT("' + params[2] + '", "^WjsGhkDlFur$DlShQpDlTus&)(*&^%$")) '
            //+ ', HEX(AES_ENCRYPT(' + params[2] + ', "^WjsGhkDlFur$DlShQpDlTus&)(*&^%$")) '
            + ', "100002" '
            + ', "100003" '
            + ', "' + params[3] + '"'
            //+ ', "' + params[3] + '"'
            + ', "$DlShQpDlTus!TjQlTmDnsDudWk&)(*&" '
            + ', "^WjsGhkDlFur$DlShQpDlTus&)(*&^%$" '
            + ', @OUT);'
        ssoDB.excuteSProcedure(q
            , {}
            , function (err, result) {
                if (err) {
                    logger.error('connContents - doMsgType_EX_STORED_PROCEDURE - err: ' + err)
                    sendErrorToContents(messageNo, err)

                    return
                }

                logger.info('connContents - doMsgType_EX_STORED_PROCEDURE - OK - csid: ' + params[3] + ' - result: ' + JSON.stringify(result))

                doSocketWriteData(m_mapContents.get(messageNo), 'OK')
            })
    }
    catch (e) {
        logger.error('connContents - doMsgType_EX_STORED_PROCEDURE - catch: ' + e)
        let oCon = m_mapContents.get(messageNo)
        doSocketWriteData(oCon, e)
    }
}


////////////////////////////////////////////////////////////////////////
// SET_MENU - 선택한 셋트 메뉴에 대한 메뉴를 가져온 후 주문 데이터 인서트...
function doMsgType_SETMENU_SELECT_ORDER(receiveData, messageNo) {
    logger.debug('connContents - SETMENU_SELECT_ORDER - messageNo: ' + messageNo + ' - data start -----')
    logger.debug(JSON.stringify(receiveData.sendData))
    logger.debug('connContents - SETMENU_SELECT_ORDER - messageNo: ' + messageNo + ' - data end -----')

    if (!receiveData.sendData) {
        logger.warning('connContents - SETMENU_SELECT_ORDER - No receiveData.sendData !!!')
        sendErrorToContents(messageNo, 'SETMENU_SELECT_ORDER - No receiveData.sendData')
        return
    }

    let largeParam = receiveData.sendData.params[1]
    let oParams = receiveData.sendData.params

    /* largeParam
    { MRHST_SEQ: 74,
        CSTMR_TEL_NO: '01089501450',
        RECPTN_TEL_NO: '',
        DLVR_AREA: '테이크아웃(매장방문수령)',
        DLVR_DETAIL_AREA: '',
        MEMO: '',
        PRE_PYMNT_SE_CODE: '100002',
        PYMNT_TY: '100003',
        ORDER_AMOUNT: 19900,
        ORDER_STTUS_CODE: '100000',
        DLVR_EXPECT_TIME: 0,
        CANCL_RESN_CODE: '',
        CANCL_OPETR_CODE: '',
        ORDER_URL: '',
        CSID: 'test',
        ADDR_TYPE: 'P',
        RESVE_GNRL_SE: '100002',
        ORDER_SE: '100002',
        DLVR_SE: '100002',
        POS_CODE: '100003'}
       */
    /* oParams
        [ { MRHST_SEQ: 74, PCKAGE_SCLAS_CODE: '1078', PCKAGE_SLE_UNIT: '' },
            { MRHST_SEQ: 74,
                CSTMR_TEL_NO: '01089501450',
                RECPTN_TEL_NO: '',
                DLVR_AREA: '테이크아웃(매장방문수령)',
                DLVR_DETAIL_AREA: '',
                MEMO: '',
                PRE_PYMNT_SE_CODE: '100002',
                PYMNT_TY: '100003',
                ORDER_AMOUNT: 19900,
                ORDER_STTUS_CODE: '100000',
                DLVR_EXPECT_TIME: 0,
                CANCL_RESN_CODE: '',
                CANCL_OPETR_CODE: '',
                ORDER_URL: '',
                CSID: 'test',
                ADDR_TYPE: 'P',
                RESVE_GNRL_SE: '100002',
                ORDER_SE: '100002',
                DLVR_SE: '100002' } ]
    */

    //let posCode = largeParam.POS_CODE

    selQuery.procExecuteQuery('QGSETMENU_SEL', oParams, (err, oMenus) => {
        if (err) {
            logger.error('connContents - SETMENU_SELECT_ORDER - QGSETMENU_SEL - err: ' + err)
            sendErrorToContents(messageNo, err)
            return
        }

        selQuery.procFinalOrderSetMenu(largeParam, oMenus, (err, rows) => {
            if (err) {
                logger.error('connContents - SETMENU_SELECT_ORDER - procFinalOrderSetMenu - err: ' + err)
                sendErrorToContents(messageNo, err)
                return
            }

            doSocketWriteData(m_mapContents.get(messageNo), JSON.stringify(rows))
        })
    })

    /*if(posCode === '100003') { // SSO
        selQuery.procExecuteQuery('QGSETMENU_SEL', oParams, (err, oMenus) => {
            if (err) {
                logger.error('connContents - SETMENU_SELECT_ORDER - QGSETMENU_SEL - err: ' + err)
                sendErrorToContents(messageNo, err)
                return
            }

            selQuery.procFinalOrderSetMenu(largeParam, oMenus, (err, rows) => {
                if (err) {
                    logger.error('connContents - SETMENU_SELECT_ORDER - procFinalOrderSetMenu - err: ' + err)
                    sendErrorToContents(messageNo, err)
                    return
                }

                doSocketWriteData(m_mapContents.get(messageNo), JSON.stringify(rows))
            })
        })
    }
    else if(posCode === '100001'){ // FUSE
        let sendData = {sleHistSeq: sleHistSeq
            , requstTy: requstTy
            , rqesterSe: rqesterSe
            , mrhstSeq: mrhstSeq
            , shopNumber: shopNumber}

        posFUSE.sendDataFUSE('ORDER_SEND', sendData,  (err, res) => {
            if (err) {
                logger.error('connContents - doMsgType_SHOP_ORDER - FUSE - sendDataFUSE - ORDER_SEND -- ' + err)
                sendErrorToContents(messageNo, err)
                return
            }

            doSocketWriteData(m_mapContents.get(messageNo), JSON.stringify(res))
        })
    }*/


    /////////////////////////////////////////
    // 셋트 메뉴에 대한 메뉴 데이터

}


////////////////////////////////////////////////////////////////////////
// HISTORY - 선택한 히스토리 메뉴의 세부 항목을 가져온 후 주문 데이터 인서트...
function doMsgType_HISTORY_SELECT_ORDER(receiveData, messageNo) {
    logger.debug('connContents - doMsgType_HISTORY_SELECT_ORDER - messageNo: ' + messageNo + ' - data start -----')
    logger.debug(receiveData)
    logger.debug('connContents - doMsgType_HISTORY_SELECT_ORDER - messageNo: ' + messageNo + ' - data end -----')

    if (!receiveData.sendData) {
        logger.warning('connContents - doMsgType_HISTORY_SELECT_ORDER - No receiveData.sendData !!!')
        sendErrorToContents(messageNo, 'doMsgType_HISTORY_SELECT_ORDER - No receiveData.sendData')
        return
    }

    let largeParam = receiveData.sendData.params[1]

    /////////////////////////////////////////
    // HISTORY 메뉴에 대한 메뉴 데이터
    let sMRHST_SEQ = largeParam.MRHST_SEQ
    let sUSER_MDN = largeParam.USER_MDN
    let sleHistSeq = receiveData.sendData.params[0].SLE_HIST_SEQ

    selQuery.procExecuteQuery('QGSHISTORY_SEL_MENU', [{
        MRHST_SEQ: sMRHST_SEQ,
        USER_MDN: sUSER_MDN,
        SLE_HIST_SEQ: sleHistSeq
    }], (err, oMenus) => {
        if (err) {
            logger.error('connContents - doMsgType_HISTORY_SELECT_ORDER - QGHISTORY_SEL_MENU - err: ' + err)
            sendErrorToContents(messageNo, err)
            return
        }

        if (largeParam.DLVR_AREA) {
            let sleAmount = 0
            for (let i = 0; i < oMenus.ORDER_SEL_HISTORY.length; i++) {
                sleAmount += parseInt(oMenus.ORDER_SEL_HISTORY[i].SLE_AMOUNT)
            }
            largeParam.ORDER_AMOUNT = sleAmount

            selQuery.procFinalOrderHistory(largeParam, oMenus, (err, rows) => {
                if (err) {
                    logger.error('connContents - doMsgType_HISTORY_SELECT_ORDER - procFinalOrderSetMenu - err1: ' + err)
                    sendErrorToContents(messageNo, err)
                    return
                }

                doSocketWriteData(m_mapContents.get(messageNo), JSON.stringify(rows))
            })
        }
        else {
            // have juso...
            let csid = largeParam.CSID
            redis.hGetRedis(csid + '_CART', 'HISTORY_ORDER', (err, retCart) => {
                if (err) {
                    logger.error('connContents - doMsgType_HISTORY_SELECT_ORDER - get redis err: ' + err)
                    sendErrorToContents(messageNo, err)
                    return
                }

                let addr1 = JSON.parse(retCart)[0].ADRES
                let addr2 = JSON.parse(retCart)[0].ADRES2

                redis.hGetRedis(csid, 'CC', (err, oCC) => {
                    if (err) {
                        logger.error('connContents - doMsgType_HISTORY_SELECT_ORDER - get redis err: ' + err)
                        sendErrorToContents(messageNo, err)
                        return
                    }

                    let userMdn = JSON.parse(oCC).user

                    largeParam.DLVR_AREA = addr1
                    largeParam.DLVR_DETAIL_AREA = addr2
                    largeParam.CSTMR_TEL_NO = userMdn
                    largeParam.PYMNT_TY = '100003'

                    let sleAmount = 0
                    for (let i = 0; i < oMenus.ORDER_SEL_HISTORY.length; i++) {
                        sleAmount += parseInt(oMenus.ORDER_SEL_HISTORY[i].SLE_AMOUNT)
                    }
                    largeParam.ORDER_AMOUNT = sleAmount

                    selQuery.procFinalOrderHistory(largeParam, oMenus, (err, rows) => {
                        if (err) {
                            logger.error('connContents - doMsgType_HISTORY_SELECT_ORDER - procFinalOrderSetMenu - err2: ' + err)
                            sendErrorToContents(messageNo, err)
                            return
                        }

                        doSocketWriteData(m_mapContents.get(messageNo), JSON.stringify(rows))
                    })
                })
            })
        }
    })
}


////////////////////////////////////////////////////
// EX_STAT_VIEW
// insert stat view...
function doMsgType_EX_STAT_VIEW(receiveData, messageNo) {
    logger.debug('connContents - doMsgType_EX_STAT_VIEW - messageNo: ' + messageNo + ' - data start -----')
    logger.debug(receiveData)
    logger.debug('connContents - doMsgType_EX_STAT_VIEW - messageNo: ' + messageNo + ' - data end -----')

    if (!receiveData.sendData) {
        logger.warning('connContents - doMsgType_EX_STAT_VIEW - No receiveData.sendData !!!')
        sendErrorToContents(messageNo, 'doMsgType_EX_STAT_VIEW - No receiveData.sendData')
        return
    }

    let queryGroupCode = receiveData.sendData.queryGroupCode
    let params = receiveData.sendData.params

    /////////////////////////////////////////
    // 전달받은 쿼리 그룹 코드를 실행한다.
    if (params[0] && !Object.keys(params[0]).length) {
        logger.error('connContents - doMsgType_EX_STAT_VIEW - err: ' + err)
        sendErrorToContents(messageNo, err)
        return
    }

    selQuery.procInsertExecuteQuery(queryGroupCode, params, (err, rows) => {
        if (err) {
            logger.error('connContents - doMsgType_EX_STAT_VIEW 2 - err: ' + err)
            sendErrorToContents(messageNo, err)
            return
        }

        doSocketWriteData(m_mapContents.get(messageNo), JSON.stringify(rows))
    })
}


/////////////////////////////
// 연결 갯수 카운트...
let getContentsConnectionCount = (str, cb) => {
    oConnContents.getConnections((err, cnt) => {
        if (err) {
            logger.error('connContents - getContentsConnectionCount - state - ' + str + ' - err: ' + err)
            cb(err)
            return
        }

        logger.info('connContents - getContentsConnectionCount - state - ' + str + ' - count: ' + cnt)
        cb(null, cnt)
    })
}
//////////////////////////////////////
// set m_mapContents
let setMapContents = (messageNo, messageType, oContents) => {
    m_mapContents.set(messageNo, oContents)
    logger.debug('connContents - setMapContents - m_mapContents.count: ' + m_mapContents.count() + ' - messageNo: ' + messageNo + ' , messageType: ' + messageType)
}


///////////////////////////////
// 소켓 데이터 전송
let doSocketWriteData = (socket, data) => {

    if (socket) {
        netUtils.zlibZip(data, (err, oData) => {
            if (err) {
                logger.error('connContents - doSocketWriteData - zip err: ' + err)
                let sData = data.toString()
                logger.error('connContents - doSocketWriteData - zip err - sData: ' + sData)
                try {
                    let success = !socket.write(err)
                    if (!success) {
                        socket.once('drain', doSocketWriteData.bind(this, socket, err))
                    }
                }
                catch (e) {
                    logger.error('connContents - doSocketWriteData err - catch - e: ' + e)
                }

                return
            }

            try {
                let success = !socket.write(oData)
                if (!success) {
                    socket.once('drain', doSocketWriteData.bind(this, socket, oData))
                }
            }
            catch (e) {
                logger.error('connContents - doSocketWriteData - catch - e: ' + e)
            }
        })
    }
}


///////////////////////////////////////////
// remove client object...
let removeClientContents = (messageNo) => {
    if (!messageNo) {
        logger.error('connContents - removeClientContents - invalid messageNo: ' + messageNo + ' , m_mapContents.cnt: ' + m_mapContents.count())
        return
    }

    if (!m_mapContents.has(messageNo)) {
        logger.warning('connContents - removeClientContents - No has m_mapContents!!! messageNo: ' + messageNo + ' , m_mapContents.cnt: ' + m_mapContents.count())
        return
    }

    m_mapContents.remove(messageNo)
    logger.info('connContents - removeClientContents - messageNo: ' + messageNo + ' , m_mapContents.cnt: ' + m_mapContents.count())
}


let findQueryGroup = (code) => {
    let ret

    switch (code) {
        case 'TEST':
            ret = ['BEST_MENU', 'MENU_COMBO']
            break

        case 'OVS_START':
            ret = ['BEST_MENU', 'MENU_COMBO']
            break

        case 'SNT001':
            ret = ['BEST_MENU', 'MENU_COMBO']
            break

        case 'SNT002':
            ret = 'CHOICE_MENU'
            break

        default:
            ret = ''
    }

    return ret
}


//////////////////////////////////////////////
// next scenario data
let getNextScenarioData = (scenCode, cb) => {
    // redis 에 있는지 체크...

    selQuery.executeQueryUsingCodes(findQueryGroup(scenCode), '', (err, rows) => {
        if (err) {
            logger.error('connContents - getNextScenarioData - err: ' + err)
            cb(err)
            return
        }

        /////// redis 저장

        cb(null, rows)
    })
}


///////////////////////////////////////
// send ERROR
let sendErrorToContents = (messageNo, errMsg) => {
    if (m_mapContents.has(messageNo)) {
        let rErr = {}
        rErr.ERROR = errMsg
        doSocketWriteData(m_mapContents.get(messageNo), JSON.stringify(rErr))
    }
    else {
        logger.warning('connContents - sendErrorToContents - No messageNo key... messageNo:' + messageNo + ' - send err message: ' + errMsg)
    }
}
