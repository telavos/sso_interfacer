const net = require('net')
const CONSTS = require('../libs/CONSTS')
const logger = require('../libs/logger')

////////////////////////////////////
//from Contents...
let oConnPing = net.createServer(function (oConn) {
    oConn.on('data', function (oConnData) {
        let sResData = oConnData.toString()
        logger.warning('connPing - on data - ++|' + sResData + '|++')
        this.destroy()
    })

    oConn.on('close', function () {
        //console.log('connPing - close...')
    })

    oConn.on('error', function (err) {
        logger.error('connPing - oConn ERROR -  err: ' + JSON.stringify(err))
        this.destroy()
    })

    oConn.on('end', function () {
        //console.log('connPing - oConn on end...')
    })

    oConn.on('timeout', function () {
        logger.warning('connPing - oConn on timeout...')
        this.destroy()
    })
})


oConnPing.on('error', function (e) {
    logger.error('connPing - oConnPing error -  err: ' + e)
    oConnPing.destroy()
})
oConnPing.listen(CONSTS.SVR_ENV.IF_PING_PORT, function () {
    logger.info('connPing - Server listening on ' + CONSTS.SVR_ENV.IF_PING_PORT)
})
