const request = require('request')
const logger = require('../libs/logger')
const CONSTS = require('../libs/CONSTS')
const CONSTS_SYS = require('../libs/CONSTS_SYS')
const redis = require('../libs/redis')
const selQuery = require('../libs/db/selQuery')


module.exports = {
    sendDataCNTT: function(workType, data, cb) {
        switch(workType){
            case 'WT_POS_SHOP_STATUS':
                doShopStatusCheck_new(data)
                break

            case 'ORDER_SEND':
                doSendOrderData_new(data, cb)
                break

            case 'CC_FIRST_POS':
                doShopStatusCheck(data, cb)
                break

            case 'ORDER_CANCEL':
                doOrderCancel(data, cb)
                break
        }
    }
    , sendDataCNTTforPay: function(workType, data) {
        switch(workType){
            case 'WT_POS_SHOP_ORDER_AFTER_PAY_CARD':  // 카드 결제 후 SHOP으로...
                doSendOrderAfterPayCard(data)
                break
        }
    }
}


function doSendOrderData(sendData, cb) {
    /*
        {sleHistSeq: sleHistSeq
        , requstTy: requstTy
        , rqesterSe: rqesterSe
        , mrhstSeq: mrhstSeq
        , shopNumber: shopNumber}
     */

    logger.info('IF_CNTT - doSendOrderData - sendData: ' + JSON.stringify(sendData))

    let sleHistSeq = sendData.sleHistSeq
    let requstTy = sendData.requstTy
    let rqesterSe = sendData.rqesterSe
    let mrhstSeq = sendData.mrhstSeq
    let shopNumber = sendData.shopNumber

    let username = CONSTS.CONN_SETTING.CNTT_ORDER_URL_ID
    let password = CONSTS.CONN_SETTING.CNTT_ORDER_URL_PW
    let auth = 'Basic ' + Buffer.from(username + ':' + password).toString('base64')
    let hostId = CONSTS_SYS.MY_INFO.IF_NAME
    let serviceNm = 'ovs.interfacer'

    let orderData = {"sleHistSeq": sleHistSeq
        , "rqesterSe": rqesterSe
        , "requstTy": requstTy
        , "mrhstSeq": mrhstSeq}

    let reqData = {
        uri: CONSTS.CONN_SETTING.CNTT_ORDER_URL,
        method: 'POST',
        headers: {'Content-Type': 'application/json', 'Authorization': auth},
        json: {
            "hostId": hostId
            , 'serviceNm': serviceNm
            , 'ovsOrderDomains': [orderData]
        }
        , timeout: 5000
    }


    logger.info('IF_CNTT - doSendOrderData - reqData - shopNumber: ' + shopNumber + ' - sendData: ' + JSON.stringify(reqData))

    request(reqData, (error, response, body)=>{
        if (error) {
            logger.error('IF_CNTT - request - err: ' + error)
            cb(error, error)
            return
        }

        logger.info('IF_CNTT - doSendOrderData - response - sleHistSeq: ' + sleHistSeq + ' , requstTy: ' + requstTy + ' , rqesterSe: ' + rqesterSe + ' , mrhstSeq: ' + mrhstSeq + ' , shopNumber: ' + shopNumber + ' - callback data: ' + JSON.stringify(body))

        if (!body) {
            logger.error('IF_CNTT - response - no body data - sleHistSeq: ' + sleHistSeq + ' , requstTy: ' + requstTy + ' , rqesterSe: ' + rqesterSe + ' , mrhstSeq: ' + mrhstSeq + ' , shopNumber: ' + shopNumber)
            cb('shop error - no body data')
            return
        }

        if (!body.statusCode || body.statusCode !== '200') {
            logger.error('IF_CNTT - doSendOrderData - response - !200 - sleHistSeq: ' + sleHistSeq + ' , requstTy: ' + requstTy + ' , rqesterSe: ' + rqesterSe + ' , mrhstSeq: ' + mrhstSeq + ' - statusCode: ' + body.statusCode)
            cb('shop error!!!')
            return
        }

        logger.info('IF_CNTT - doSendOrderData - success... sleHistSeq: ' + sleHistSeq + ' , rqesterSe: ' + rqesterSe + ' , requstTy: ' + requstTy)
        cb(null, body.statusCode)
    })
}

///////////////////////////////////////
// Shop 로그인 상태 체크
function doShopStatusCheck(sendData, cb) {
    let username = CONSTS.CONN_SETTING.CNTT_SHOP_STATUS_URL_ID
    let password = CONSTS.CONN_SETTING.CNTT_SHOP_STATUS_URL_PW
    let auth = 'Basic ' + Buffer.from(username + ':' + password).toString('base64')
    let hostId = CONSTS_SYS.MY_INFO.IF_NAME
    let serviceNm = 'ovs.interfacer'
    let mrhstSeq = sendData.sendData[0].mrhstSeq

    let reqData = {
        uri: CONSTS.CONN_SETTING.CNTT_SHOP_STATUS_URL,
        method: 'POST',
        headers: {'Content-Type': 'application/json', 'Authorization': auth},
        json: {
            "hostId": hostId
            , "serviceNm": serviceNm
            , "mrhstSeq": mrhstSeq
        }
        , timeout: 5000
    }

    logger.info('IF_CNTT - doShopStatusCheck - mrhstSeq: ' + mrhstSeq + ' - reqData: ' + JSON.stringify(reqData))

    request(reqData, (error, response, body)=>{
        if (error) {
            logger.error('IF_CNTT - doShopStatusCheck - request - err: ' + error)
            cb(error, error)
            return
        }

        if (!body) {
            logger.error('IF_CNTT - doShopStatusCheck - request - resultcode no Data !!! - mrhstSeq: ' + mrhstSeq)
            cb('shop error - no body data', 'No response Shop data... - mrhstSeq: ' + mrhstSeq)
            return
        }

        //console.log("body.mrhstSeq - " + body.mrhstSeq)
        //console.log("body.status - " + body.status)

        //if (!body.statusCode || body.statusCode !== '200') {
        //    logger.error('IF_CNTT - doShopStatusCheck - request - mrhstSeq: ' + mrhstSeq + ' - resultcode: ' + body.statusCode)
        //    cb('shop error!!!', '')
        //    return
        //}

        logger.info('IF_CNTT - doShopStatusCheck - response - mrhstSeq: ' + mrhstSeq + ' - body: ' + JSON.stringify(body))

        cb(null, body)
    })
}


///////////////////////////////////////
// 주문 취소
function doOrderCancel(sendData, cb) {
    /*
        { ORDER_STTUS_DE_HMS: '20190613172147', SLE_HIST_SEQ: 22403 , POS_CODE: 100003 , MRHST_SEQ: 5
     */
    let username = CONSTS.CONN_SETTING.CNTT_ORDER_URL_ID
    let password = CONSTS.CONN_SETTING.CNTT_ORDER_URL_PW
    let auth = 'Basic ' + Buffer.from(username + ':' + password).toString('base64')
    let hostId = CONSTS_SYS.MY_INFO.IF_NAME
    let serviceNm = 'ovs.interfacer'

    let mrhstSeq = sendData.MRHST_SEQ
    let sleHistSeq = sendData.SLE_HIST_SEQ

    let reqData = {
        uri: CONSTS.CONN_SETTING.CNTT_ORDER_URL,
        method: 'POST',
        headers: {'Content-Type': 'application/json', 'Authorization': auth},
        json: {
            "hostId": hostId
            , "serviceNm": serviceNm
            , "ovsOrderDomains": [{"sleHistSeq": sleHistSeq
                , "rqesterSe":'100001'
                , "requstTy": '100006'
                , "mrhstSeq": mrhstSeq}]
        }
        , timeout: 5000
    }

    logger.debug('IF_CNTT - doOrderCancel - mrhstSeq: ' + mrhstSeq + ' - reqData: ' + JSON.stringify(reqData))

    request(reqData, (error, response, body)=>{
        if (error) {
            logger.error('IF_CNTT - doOrderCancel - request - err: ' + error)
            cb(error, error)
            return
        }

        if (!body) {
            logger.error('IF_CNTT - doOrderCancel - request - resultcode no Data !!! - mrhstSeq: ' + mrhstSeq)
            cb('shop error - no body data', 'No response Shop data... - mrhstSeq: ' + mrhstSeq)
            return
        }

        logger.debug('IF_CNTT - doOrderCancel - response - mrhstSeq: ' + mrhstSeq + ' , body: ' + JSON.stringify(body))

        //if (!body.statusCode || body.statusCode !== '200') {
        //    logger.error('IF_CNTT - doOrderCancel - request - mrhstSeq: ' + mrhstSeq + ' - resultcode: ' + body.statusCode)
        //    cb('shop error!!!', '')
        //    return
        //}

        cb(null, body)
    })
}





///////////////////////////////////////////////
///////////////////////////////////////////////
///////////////////////////////////////////////
///////////////////////////////////////////////
///// new /////////////////////////////////////
///// new /////////////////////////////////////
///// new /////////////////////////////////////

///////////////////////////////////////
// Shop 로그인 상태 체크
function doShopStatusCheck_new(data) {
    // data: {"workType":"WT_POS_SHOP_STATUS"
    //          ,"option":"200101"
    //          ,"params":[{"mrhstSeq":81,"shopNumber":"07052013279"}]
    //          ,"csid":"1560387300.115468"
    //          , isRes: true
    //          ,"CNAME":"mb4H7"}
    let oData = JSON.parse(data)
    let workType = oData.workType
    let posCode = oData.option
    let params = oData.params
    let csid = oData.csid
    let isRes = oData.isRes
    let CNAME = oData.CNAME

    logger.info('IF_CNTT - doShopStatusCheck_new - sendData: ' + JSON.stringify(data))
    let username = CONSTS.CONN_SETTING.CNTT_SHOP_STATUS_URL_ID
    let password = CONSTS.CONN_SETTING.CNTT_SHOP_STATUS_URL_PW
    let auth = 'Basic ' + Buffer.from(username + ':' + password).toString('base64')
    let hostId = CONSTS_SYS.MY_INFO.IF_NAME
    let serviceNm = 'ovs.interfacer'
    let mrhstSeq = params[0].mrhstSeq
    let shopNumber = params[0].shopNumber

    let reqData = {
        uri: CONSTS.CONN_SETTING.CNTT_SHOP_STATUS_URL,
        method: 'POST',
        headers: {'Content-Type': 'application/json', 'Authorization': auth},
        json: {
            "hostId": hostId
            , "serviceNm": serviceNm
            , "mrhstSeq": mrhstSeq
        }
        , timeout: 5000
    }

    logger.debug('IF_CNTT - doShopStatusCheck_new - mrhstSeq: ' + mrhstSeq + ' - reqData: ' + JSON.stringify(reqData))

    request(reqData, (error, response, body)=>{
        if (error) {
            logger.error('IF_CNTT - doShopStatusCheck_new - request - err: ' + error)
            if(isRes){
                let pubData = {data: '', workType: workType, csid: csid}
                redis.pubRedis(CONSTS_SYS.PUB_CH.RES_POS + CNAME, pubData)
            }
            return
        }

        if (!body) {
            logger.error('IF_CNTT - doShopStatusCheck_new - result no Data !!! - mrhstSeq: ' + mrhstSeq)
            if(isRes){
                let pubData = {data: '', workType: workType, csid: csid}
                redis.pubRedis(CONSTS_SYS.PUB_CH.RES_POS + CNAME, pubData)
            }
            return
        }

        logger.info('IF_CNTT - doShopStatusCheck_new - callback body - mrhstSeq: ' + mrhstSeq + ' - body: ' + JSON.stringify(body))

        let shopStatus = body.status

        if (!shopStatus) {
            logger.error('IF_CNTT - doShopStatusCheck_new - request - mrhstSeq: ' + mrhstSeq + ' - resultcode: ' + shopStatus)
            if(isRes){
                let pubData = {data: '', workType: workType, csid: csid}
                redis.pubRedis(CONSTS_SYS.PUB_CH.RES_POS + CNAME, pubData)
            }
            return
        }



        ////// DB와 연결해서 SHOP 정보를 가져온다...
        let params = [{MRHST_SEQ: mrhstSeq, SHOP_NUMBER: shopNumber}, {MRHST_SEQ: mrhstSeq}]
        selQuery.executeQueryGroupCode_new('QG_CURR_SHOP_STATUS', params, (err, rows) => {
            if (err) {
                logger.error('IF_CNTT - doShopStatusCheck_new - executeQueryGroupCode_new - csid: ' + csid + ' - err: ' + err)
                if(isRes){
                    let pubData = {data: '', workType: workType, csid: csid}
                    redis.pubRedis(CONSTS_SYS.PUB_CH.RES_POS + CNAME, pubData)
                }
                return
            }

            if (!rows) {
                logger.warning('IF_CNTT - doShopStatusCheck_new - executeQueryGroupCode_new - csid: ' + csid + ' - no data')
                if(isRes){
                    let pubData = {data: '', workType: workType, csid: csid}
                    redis.pubRedis(CONSTS_SYS.PUB_CH.RES_POS + CNAME, pubData)
                }
                return
            }

            if(isRes){
                let sendData = {POS_DATA: shopStatus, DB_DATA: rows}

                let pubData = {data: sendData, workType: workType, csid: csid}
                redis.pubRedis(CONSTS_SYS.PUB_CH.RES_POS + CNAME, pubData)
            }
        })
    })
}




function doSendOrderData_new(sendData, cb) {
    /*
        { sleHistSeq: 22673,
          requstTy: '100001',
          rqesterSe: '100001',
          mrhstSeq: '73',
          shopNumber: '07052013290' }
     */

    logger.info('IF_CNTT - doSendOrderData_new - sendData: ' + JSON.stringify(sendData))

    let sleHistSeq = sendData.sleHistSeq
    let requstTy = sendData.requstTy
    let rqesterSe = sendData.rqesterSe
    let mrhstSeq = sendData.mrhstSeq
    let shopNumber = sendData.shopNumber

    let username = CONSTS.CONN_SETTING.CNTT_ORDER_URL_ID
    let password = CONSTS.CONN_SETTING.CNTT_ORDER_URL_PW
    let auth = 'Basic ' + Buffer.from(username + ':' + password).toString('base64')
    let hostId = CONSTS_SYS.MY_INFO.IF_NAME
    let serviceNm = 'ovs.interfacer'

    let orderData = {"sleHistSeq": sleHistSeq
        , "rqesterSe": rqesterSe
        , "requstTy": requstTy
        , "mrhstSeq": mrhstSeq}

    let reqData = {
        uri: CONSTS.CONN_SETTING.CNTT_ORDER_URL,
        method: 'POST',
        headers: {'Content-Type': 'application/json', 'Authorization': auth},
        json: {
            "hostId": hostId
            , 'serviceNm': serviceNm
            , 'ovsOrderDomains': [orderData]
        }
        , timeout: 5000
    }


    logger.info('IF_CNTT - doSendOrderData_new - reqData - shopNumber: ' + shopNumber + ' - sendData: ' + JSON.stringify(reqData))

    request(reqData, (error, response, body)=>{
        if (error) {
            logger.error('IF_CNTT - request - err: ' + error)
            cb(error, error)
            return
        }

        logger.info('IF_CNTT - doSendOrderData_new - response - sleHistSeq: ' + sleHistSeq + ' , requstTy: ' + requstTy + ' , rqesterSe: ' + rqesterSe + ' , mrhstSeq: ' + mrhstSeq + ' , shopNumber: ' + shopNumber + ' - callback data: ' + JSON.stringify(body))

        if (!body) {
            logger.error('IF_CNTT - response - no body data - sleHistSeq: ' + sleHistSeq + ' , requstTy: ' + requstTy + ' , rqesterSe: ' + rqesterSe + ' , mrhstSeq: ' + mrhstSeq + ' , shopNumber: ' + shopNumber)
            cb('shop error - no body data')
            return
        }

        if (!body.statusCode || body.statusCode !== '200') {
            logger.error('IF_CNTT - doSendOrderData_new - response - sleHistSeq: ' + sleHistSeq + ' , requstTy: ' + requstTy + ' , rqesterSe: ' + rqesterSe + ' , mrhstSeq: ' + mrhstSeq + ' - statusCode: ' + body.statusCode)
            cb('shop error!!!')
            return
        }

        logger.info('IF_CNTT - doSendOrderData_new - success... sleHistSeq: ' + sleHistSeq + ' , rqesterSe: ' + rqesterSe + ' , requstTy: ' + requstTy)
        cb(null, body.statusCode)
    })
}


//////////////////////////////////////
// 페이 결제 후 POS 전달...
function doSendOrderAfterPayCard (sendData) {
    console.log('IF_CNTT - doSendOrderAfterPayCard --------- sssssssss')
    console.dir(sendData)
    console.log('IF_CNTT - doSendOrderAfterPayCard --------- eeeeeeeeeeeeeee')
    logger.info('IF_CNTT - doSendOrderAfterPayCard - sendData: ' + JSON.stringify(sendData))

    let requstTy = '100001'
    let rqesterSe = '100001'

    let sleHistSeq = sendData.sleHistSeq
    //let requstTy = sendData.requstTy
    //let rqesterSe = sendData.rqesterSe
    let mrhstSeq = sendData.mrhstSeq
    let shopNumber = sendData.shopNumber

    let username = CONSTS.CONN_SETTING.CNTT_ORDER_URL_ID
    let password = CONSTS.CONN_SETTING.CNTT_ORDER_URL_PW
    let auth = 'Basic ' + Buffer.from(username + ':' + password).toString('base64')
    let hostId = CONSTS_SYS.MY_INFO.IF_NAME
    let serviceNm = 'ovs.interfacer'

    let orderData = {"sleHistSeq": sleHistSeq
        , "rqesterSe": rqesterSe
        , "requstTy": requstTy
        , "mrhstSeq": mrhstSeq}

    let reqData = {
        uri: CONSTS.CONN_SETTING.CNTT_ORDER_URL,
        method: 'POST',
        headers: {'Content-Type': 'application/json', 'Authorization': auth},
        json: {
            "hostId": hostId
            , 'serviceNm': serviceNm
            , 'ovsOrderDomains': [orderData]
        }
        , timeout: 5000
    }


    logger.info('IF_CNTT - doSendOrderAfterPayCard - reqData - shopNumber: ' + shopNumber + ' - sendData: ' + JSON.stringify(reqData))

    request(reqData, (error, response, body)=>{
        if (error) {
            logger.error('IF_CNTT - request - err: ' + error)
            //cb(error, error)
            return
        }

        logger.info('IF_CNTT - doSendOrderAfterPayCard - response - sleHistSeq: ' + sleHistSeq + ' , requstTy: ' + requstTy + ' , rqesterSe: ' + rqesterSe + ' , mrhstSeq: ' + mrhstSeq + ' , shopNumber: ' + shopNumber + ' - callback data: ' + JSON.stringify(body))

        if (!body) {
            logger.error('IF_CNTT - response - no body data - sleHistSeq: ' + sleHistSeq + ' , requstTy: ' + requstTy + ' , rqesterSe: ' + rqesterSe + ' , mrhstSeq: ' + mrhstSeq + ' , shopNumber: ' + shopNumber)
            //cb('shop error - no body data')
            return
        }

        if (!body.statusCode || body.statusCode !== '200') {
            logger.error('IF_CNTT - doSendOrderAfterPayCard - response - sleHistSeq: ' + sleHistSeq + ' , requstTy: ' + requstTy + ' , rqesterSe: ' + rqesterSe + ' , mrhstSeq: ' + mrhstSeq + ' - statusCode: ' + body.statusCode)
            //cb('shop error!!!')
            return
        }

        logger.info('IF_CNTT - doSendOrderAfterPayCard - success... sleHistSeq: ' + sleHistSeq + ' , rqesterSe: ' + rqesterSe + ' , requstTy: ' + requstTy)
        //cb(null, body.statusCode)
    })
}




/*
module.exports = function (type) {
    function ovsShop(type) {
        let OPTIONS = {
            headers: {'Content-Type': 'application/json'},
            url: null,
            body: null
        };
        const PORT = '3030';
        const BASE_PATH = '/api';
        let HOST = null;
        (function () {
            switch (type) {
                case 'dev':
                    HOST = 'http://106.250.166.99';
                    break;
                case 'prod':
                    HOST = 'https://www.ovs.com';
                    break;
                default:
                    HOST = 'http://localhost';
            }
        })(type);

        return {
            // sleHistSeq - 판매이력순번
            // orderTy - 요청구분코드 ["order", "cancel"]
            sendData : function (data, sleHistSeq, orderTy, callback) {
                OPTIONS.url = HOST + ':' + PORT + BASE_PATH + '/contacts/' + data;
                OPTIONS.body = JSON.stringify({
                    "sleHistSeq": sleHistSeq,
                    "orderTy": orderTy
                });

                request.post(OPTIONS, function (err, res, result) {
                    statusCodeErrorHandler(res.statusCode, callback, result);
                });
            }
        };
    }
    function statusCodeErrorHandler(statusCode, callback , data) {
        switch (statusCode) {
            case 200:
                callback(null, JSON.parse(data));
                break;
            default:
                callback('error', JSON.parse(data));
                break;
        }
    }
    let INSTANCE;
    if (INSTANCE === undefined) {
        INSTANCE = new ovsShop(type);
    }
    return INSTANCE;
};*/