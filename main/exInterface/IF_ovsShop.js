const request = require('request')
const logger = require('../libs/logger')
const CONSTS = require('../libs/CONSTS')
const CONSTS_SYS = require('../libs/CONSTS_SYS')

module.exports = {
    sendDataToOvsShop: function (sleHistSeq, requstTy, rqesterSe, mrhstSeq, cb) {
        let username = CONSTS.CONN_SETTING.OVS_SHOP_URL_ID
        let password = CONSTS.CONN_SETTING.OVS_SHOP_URL_PW
        let auth = 'Basic ' + Buffer.from(username + ':' + password).toString('base64')
        let hostId = CONSTS_SYS.MY_INFO.IF_NAME
        let serviceNm = 'ovs.interfacer'

        let orderData = {
            "sleHistSeq": sleHistSeq
            , "rqesterSe": rqesterSe
            , "requstTy": requstTy
            , "mrhstSeq": mrhstSeq
        }

        let reqData = {
            uri: CONSTS.CONN_SETTING.OVS_SHOP_URL,
            method: 'POST',
            headers: {'Content-Type': 'application/json', 'Authorization': auth},
            json: {
                "hostId": hostId
                , 'serviceNm': serviceNm
                , 'ovsOrderDomains': [orderData]
            }
            , timeout: 5000
        }


        logger.debug('IF_ovsShop --- reqData sss ---')
        logger.debug(JSON.stringify(reqData))
        logger.debug('IF_ovsShop --- reqData eee ---')


        request(reqData, (error, response, body) => {
            if (error) {
                logger.error('IF_ovsSHop - request - err: ' + error)
                cb(error, error)
                return
            }

            logger.debug('IF_ovsShop --- callback body sss --- sleHistSeq: ' + sleHistSeq + ' , requstTy: ' + requstTy + ' , rqesterSe: ' + rqesterSe + ' , mrhstSeq: ' + mrhstSeq)
            logger.debug(body)
            logger.debug('IF_ovsShop --- callback body eee ---')

            if (!body) {
                logger.error('IF_ovsSHop - request - resultcode no Data !!! --- sleHistSeq: ' + sleHistSeq + ' , requstTy: ' + requstTy + ' , rqesterSe: ' + rqesterSe + ' , mrhstSeq: ' + mrhstSeq)
                cb('shop error - no body data', 'No response Shop data... --- sleHistSeq: ' + sleHistSeq + ' , requstTy: ' + requstTy + ' , rqesterSe: ' + rqesterSe + ' , mrhstSeq: ' + mrhstSeq)
                return
            }

            if (!body.statusCode || body.statusCode !== '200') {
                logger.error('IF_ovsSHop - request - sleHistSeq: ' + sleHistSeq + ' , requstTy: ' + requstTy + ' , rqesterSe: ' + rqesterSe + ' , mrhstSeq: ' + mrhstSeq + ' - resultcode: ' + body.statusCode)
                cb('shop error!!!', '')
                return
            }

            logger.info('IF_ovsSHop - sendDataToOvsShop - success... sleHistSeq: ' + sleHistSeq + ' , rqesterSe: ' + rqesterSe + ' , requstTy: ' + requstTy)
            cb(null, body.statusCode)
        })
    }
}
